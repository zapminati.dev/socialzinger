import Link from "next/link";
import { useState } from "react";
import Layouts from "../src/layouts/Layouts";

const Youtube = () => {
    const [video, setVideo] = useState(false);


    return (
        <Layouts pageName="Youtube ">


            <>
                <section className="pricing-section p-t-50 p-b-50">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-8">
                                <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                                    <h2 className="title">
                                        All SERVICES {" "}
                                        <span>
                                            YOUTUBE {" "}
                                            {/* <img src="/assets/img/particle/title-line.png" alt="Line" /> */}
                                        </span>{" "}

                                    </h2>
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

                                </div>

                            </div>
                        </div>

                        {/* <!-- Pricing Table --> */}
                        <div className="row justify-content-center">
                            <div className="col-lg-4 col-md-6 col-sm-8">
                                <div className="pricing-table pricing-secondary-4 " style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                    <div className="plan-title-area">
                                        <img
                                            src="/assets/img/icon/11.png"
                                            alt="Plan icon"
                                            className="plan-icon"
                                        />

                                    </div>
                                    <div className="plan-cost text-center">
                                        <h4 className="title">Subscribers</h4>
                                        <p className="plan-feature m-t-10">Channel Subscribers</p>
                                        <a href="/buy-youtube-subscribers" className="template-btn hero-btns  justify-content-center">
                                            Buy Subscribers
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-8">
                                <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                    <div className="plan-title-area">
                                        <img
                                            src="/assets/img/icon/11.png"
                                            alt="Plan icon"
                                            className="plan-icon"
                                        />

                                    </div>
                                    <div className="plan-cost text-center">
                                        <h4 className="title">LIKES</h4>
                                        <p className="plan-feature m-t-10"> Post Likes</p>
                                        <a href="/buy-youtube-likes" className="template-btn hero-btns  justify-content-center">
                                            Buy Likes
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6 col-sm-8">
                                <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                    <div className="plan-title-area">
                                        <img
                                            src="/assets/img/icon/11.png"
                                            alt="Plan icon"
                                            className="plan-icon"
                                        />

                                    </div>
                                    <div className="plan-cost text-center">
                                        <h4 className="title">VIEWS</h4>
                                        <p className="plan-feature m-t-10">Video Views</p>
                                        <a href="buy-youtube-views" className="template-btn hero-btns  justify-content-center">
                                            Buy Views
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

                <section className="pricing-section p-t-80 p-b-130" style={{ background: "#f3f3fa" }}>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-8">
                                <div className="common-heading title-line-bottom text-center m-b-20">
                                    <span className="tagline">Buy Youtube Subscribers </span>
                                    <h2 className="title">
                                        Benefits
                                    </h2>

                                    <img
                                        src="/assets/img/particle/title-line-2.png"
                                        alt="Image"
                                        className="Line"
                                    />
                                </div>
                            </div>
                        </div>

                        {/* <!-- Pricing Table --> */}
                        <div className="row justify-content-center fancy-icon-boxes-v2">

                            <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                                <div className="fancy-icon-box color-3 m-t-30" style={{ background: "rgb(223, 242, 255)", boxShadow: "rgb(5 24 43 / 10%) 0px 10px 20px 0px" }}>

                                    <div className="">
                                        <h4 className="title">
                                            <a href="#">Organic subscribers</a>
                                        </h4>
                                        <p>
                                            Unlike other shady, unreliable websites that claim to sell YouTube engagement, our subscribers
                                            are real, organic and 100% compliant with YouTube’s Terms and Conditions so your engagement
                                            will get picked up by the algorithm in no time. Organic subscribers are frequently active on
                                            your videos and ensure long-term promotion and stability for your channel.

                                        </p>

                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                                <div className="fancy-icon-box color-1 m-t-30" style={{ background: "rgb(228, 255, 223)", boxShadow: " rgb(5 24 43 / 10%) 0px 10px 20px 0px" }}>

                                    <div className="">
                                        <h4 className="title">
                                            <a href="#">Recognition within your niche</a>
                                        </h4>
                                        <p>
                                            Once you begin to gain engagement on your YouTube channel, your videos are automatically recommended to more people by the YouTube algorithm, and you can begin to garner an audience within your specific niche of content.
                                            Gaining recognition within your niche can make or break the future popularity of your YouTube channel.
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                                <div className="fancy-icon-box color-2 m-t-30" style={{ background: "rgb(232, 222, 255)", boxShadow: " rgb(5 24 43 / 10%) 0px 10px 20px 0px" }}>

                                    <div className="">
                                        <h4 className="title">
                                            <a href="#">Multiplies organic engagement</a>
                                        </h4>
                                        <p>
                                            Gaining interactions on social media is a domino effect. Once you start gaining subscribers,
                                            YouTube will automatically recommend your videos to more people, fetching you organic engagement
                                            and multiplying your views, likes and shares in no time.

                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-xl-12 col-lg-12">
                            <div className="row fancy-icon-boxes-v2">
                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-2 m-t-30" style={{ background: "#D7C6FF", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="">
                                            <h4 className="title">
                                                <a href="#">Get discovered in recommended videos</a>
                                            </h4>
                                            <p>
                                                Videos from channels with more subscribers show up in recommended videos more, giving your
                                                content the chance to get discovered by more people.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-5 m-t-30" style={{ background: "#C6FFD7", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="">
                                            <h4 className="title">
                                                <a href="#">Channel gets more likes, comments and shares</a>
                                            </h4>
                                            <p>
                                                More exposure means more likes, comments, views and shares. All these multiply your engagement and exponentially grow your channel.
                                                They also boost your representation in the recommended videos of YouTube users as well as your search results rankings so more and more
                                                people find your videos and interact with them.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                                    <div className="fancy-icon-box color-3 m-t-30" style={{ background: "#FFD7C6", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="">
                                            <h4 className="title">
                                                <a href="#">Permanent subscribers that will not be removed</a>
                                            </h4>
                                            <p>
                                                Other untrustworthy services generate subscribers with shady tricks like codes and bots, which
                                                are eventually recognized and removed by YouTube, wasting your time and money. With us,
                                                you will never have to worry about that, as we offer organic, permanent subscribers that will never be removed.

                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </section>

                <section className="about-section p-t-130 p-b-30">
                    <div className="container">
                        <div className="row justify-content-lg-between justify-content-center align-items-center">
                            <div className="col-xl-6 col-lg-6 col-md-10">
                                <div className="preview-blob-image with-floating-icon m-b-md-100">
                                    <img src="/assets/img/about-img-1.png" alt="Image" />

                                    <div className="floating-icons">
                                        <img
                                            src="/assets/img/particle/thumbs-up.png"
                                            alt="Icon"
                                            className="icon-1 animate-float-bob-y"
                                        />
                                        <img
                                            src="/assets/img/particle/announcement-mic.png"
                                            alt="Icon"
                                            className="icon-2 animate-float-bob-x"
                                        />
                                        <img
                                            src="/assets/img/particle/paper-plane.png"
                                            alt="Icon"
                                            className="icon-3 animate-float-bob-x"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-5 col-lg-6 col-md-10">
                                <div className="about-text">
                                    <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-40">
                                        {/* <span className="tagline">About Landio</span> */}
                                        <h2 className="title">
                                            Buy Youtube Subscribers And Grow Your
                                            {" "}
                                            <span>
                                                Channel {" "}
                                                <img
                                                    src="/assets/img/particle/title-line.png"
                                                    alt="Line"
                                                />
                                            </span>{" "}
                                            Organically
                                        </h2>
                                    </div>
                                    <p className="text-pullquote pullquote-secondary-color m-b-35">
                                        If you’re a small channel on YouTube trying to grow your audience, you are likely all too
                                        familiar with the incredibly difficult task of trying to grow your audience. In the initial stage,
                                        it is even harder to reach people and find subscribers, and excellent videos can get lost in the
                                        sea of content due to lack of engagement. But don’t worry; we’ve got you covered.
                                    </p>
                                    <p> Buy YouTube subscribers and our service can give you the boost in engagement you need to get
                                        your new channel up and going. Once your videos receive enough interaction, they will get picked
                                        up by the algorithm and shown in recommended videos until you begin to build an audience in no time.
                                    </p>



                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        </Layouts >
    );
};

export default Youtube;
