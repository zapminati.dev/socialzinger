import { Fragment, useEffect, useState } from "react";
import PreLoader from "../src/layouts/PreLoader";
import "../styles/globals.css";
import { store } from "../store";
import { Provider } from "react-redux";
import cron from 'node-cron';
import axios from 'axios';

export async function getServerSideProps() {
  console.log("hello");

  const cronSchedule = '0/1 * * * *'; // Runs every hour
  const apiUrl = 'http://localhost:3002/api/hello';

  const callApi = async () => {
    try {
      const response = await axios.get(apiUrl);
      console.log('API called successfully:', response.data);
    } catch (error) {
      console.error('Error calling API:', error.message);
    }
  };

  // Create the cron job
  cron.schedule(cronSchedule, () => {
    console.log('Running cron job...');
    callApi();
  });

  return {
    props: {
      data: 'test'
    },
  };
}

function MyApp({ Component, pageProps }) {
  const [loader, setLoader] = useState(true);


  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 300);
  }, []);
  return (
    <Provider store={store}>
    <Fragment>
      {loader && <PreLoader />}
      <Component {...pageProps} />
    </Fragment>
    </Provider>
  );
}

export default MyApp;
