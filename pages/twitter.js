import Link from "next/link";
import { useState } from "react";
import Layouts from "../src/layouts/Layouts";

const Twitter = () => {
    const [video, setVideo] = useState(false);


    return (
        <Layouts pageName="Twitter">
            
            <section className="pricing-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                                <h2 className="title">
                                    All SERVICES {" "}
                                    <span>
                                        TWITTER {" "}
                                        {/* <img src="/assets/img/particle/title-line.png" alt="Line" /> */}
                                    </span>{" "}

                                </h2>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

                            </div>

                        </div>
                    </div>

                    {/* <!-- Pricing Table --> */}
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 " style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/9.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">COMMENTS</h4>
                                    <p className="plan-feature m-t-10">Twitter Comments</p>
                                    <a href="/buy-twitter-comments" className="template-btn hero-btns  justify-content-center">
                                        Buy Now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/9.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">VIEWS</h4>
                                    <p className="plan-feature m-t-10"> Post Views</p>
                                    <a href="/buy-twitter-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/9.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">LIKES</h4>
                                    <p className="plan-feature m-t-10"> Post Likes</p>
                                    <a href="/buy-twitter-likes" className="template-btn hero-btns  justify-content-center">
                                        Buy Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            {/*====== End Scroll To Top ======*/}

        </Layouts >
    );
};

export default Twitter;
