import axios from "axios";
import connectMongo from "../../../../../utils/db";
const Signup = require("../../../../../src/models/freemiumModel");
// const sendEmail = require("../../../../../utils/sendEmail");
import sendEmail from "../../../../../utils/mailerSend";

export default async function handler(req, res) {
     await connectMongo();
     try {
          // console.log(req.query.email);
          const signup = await Signup.findOne({ email: req.query.email });
          if (!signup) {
               return res
                    .status(404)
                    .json({ success: false, message: "User not found." });
          }
          //Get Verification Token
          const verificationToken = signup.getEmailVerificationToken();

          await signup.save({ validateBeforeSave: false });

          const host = process.env.HOST;

          const emailVerificationUrl = `${host}/promo/email/${verificationToken}`;
          // const emailVerificationUrl = `${process.env.FRONTEND_URL}/promo/email/${verificationToken}`
          const message = `<div
          style="
            padding: 0;
            margin: 0;
            background-color: #f6f6f6;
            background-image: none;
            background-size: cover;
          "
        >
          <table width="100%" border="0" cellspacing="0" cellpadding="0" dir="ltr">
            <tbody>
              <tr>
                <td align="center">
                  <div>
                    <table
                      cellpadding="0"
                      cellspacing="0"
                      border="0"
                      align="center"
                      width="640"
                      style="width: 640px; min-width: 640px"
                      class="m_-6923522299734663521mobileHide"
                    >
                      <tbody>
                        <tr>
                          <td colspan="2" height="20"></td>
                        </tr>
                      </tbody>
                    </table>
                    <table
                      align="center"
                      border="0"
                      cellpadding="0"
                      cellspacing="0"
                      class="m_-6923522299734663521mlContentTable"
                      width="640"
                      style="width: 640px; min-width: 640px"
                    >
                      <tbody>
                        <tr>
                          <td align="center">
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-4"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="40"
                                            class="m_-6923522299734663521spacingHeight-40"
                                            style="line-height: 40px; min-height: 40px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      id="m_-6923522299734663521transactional-4"
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="padding: 0px 40px"
                                            class="m_-6923522299734663521mlContentOuter"
                                            align="center"
                                          >
                                            <table
                                              cellpadding="0"
                                              cellspacing="0"
                                              border="0"
                                              align="center"
                                              width="100%"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td align="center">
                                                    <a
                                                      style="
                                                        color: #111111;
                                                        font-family: 'Open Sans', Arial,
                                                          Helvetica, sans-serif;
                                                        font-size: 12;
                                                        line-height: unset;
                                                        font-weight: normal;
                                                        font-style: normal;
                                                        text-decoration: none;
                                                      "
                                                    >
                                                      <img
                                                        src="https://www.socialzinger.com/assets/img/logo-1.png"
                                                        id="m_-6923522299734663521logoBlock-4"
                                                        border="0"
                                                        alt=""
                                                        width="auto"
                                                        style="display: block"
                                                        name="m_-6923522299734663521_logoBlock-4"
                                                        class="CToWUd"
                                                        data-bit="iit"
                                                    /></a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="10"
                                            style="line-height: 10px; min-height: 10px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="10"
                                            style="line-height: 10px; min-height: 10px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-6"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="30"
                                            class="m_-6923522299734663521spacingHeight-30"
                                            style="line-height: 30px; min-height: 30px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width=""
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            width=""
                                            style="padding: 0px 40px"
                                            class="m_-6923522299734663521mlContentOuter"
                                          >
                                            <h1
                                              style="
                                                font-family: 'Open Sans', Arial,
                                                  Helvetica, sans-serif;
                                                color: #111111;
                                                font-size: 24px;
                                                line-height: 150%;
                                                font-weight: 400;
                                                margin: 0 0 10px 0;
                                                text-align: center;
                                              "
                                            >
                                              <strong
                                                >Confirm your email address</strong
                                              >
                                            </h1>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="20"
                                            class="m_-6923522299734663521spacingHeight-20"
                                            style="line-height: 20px; min-height: 20px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-9"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    ></table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width=""
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            width=""
                                            style="padding: 0px 40px"
                                            class="m_-6923522299734663521mlContentOuter"
                                          >
                                            <p
                                              style="
                                                font-family: 'Open Sans', Arial,
                                                  Helvetica, sans-serif;
                                                color: #6d6d6d;
                                                font-size: 16px;
                                                line-height: 150%;
                                                margin: 0 0 10px 0;
                                                font-weight: 400;
                                                margin-bottom: 0;
                                              "
                                            >
                                              <span
                                                style="
                                                  line-height: 150%;
                                                  color: rgb(109, 109, 109);
                                                "
                                                >Hello Thank you for choosing Social
                                                Zinger! Please click on the button below
                                                to verify your email address.</span
                                              >
                                            </p>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-12"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="20"
                                            class="m_-6923522299734663521spacingHeight-20"
                                            style="line-height: 20px; min-height: 20px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="padding: 0px 40px"
                                            class="m_-6923522299734663521mlContentOuter"
                                          >
                                            <table
                                              cellpadding="0"
                                              cellspacing="0"
                                              border="0"
                                              align="center"
                                              width="100%"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td
                                                    align="center"
                                                    class="m_-6923522299734663521mlContentButton"
                                                  >
                                                    <a
                                                      class="m_-6923522299734663521mlContentButton"
                                                      href="${emailVerificationUrl}"
                                                      style="
                                                        font-family: 'Open Sans', Arial,
                                                          Helvetica, sans-serif;
                                                        background-color: #0bc269;
                                                        border-radius: 3px;
                                                        color: #ffffff;
                                                        display: inline-block;
                                                        font-size: 15px;
                                                        font-weight: bold;
                                                        font-style: normal;
                                                        text-decoration: none;
                                                        line-height: 21px;
                                                        padding: 15px 0 15px 0;
                                                        text-align: center;
                                                        width: 560px;
                                                      "
                                                      target="_blank"
                                                      data-saferedirecturl="#"
                                                      >Confirm your email</a
                                                    >
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-14"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            height="20"
                                            class="m_-6923522299734663521spacingHeight-20"
                                            style="line-height: 20px; min-height: 20px"
                                          ></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            align="center"
                                            class="m_-6923522299734663521mlContentOuter"
                                          >
                                            <table
                                              cellpadding="0"
                                              cellspacing="0"
                                              border="0"
                                              align="center"
                                              width="560"
                                              style="
                                                border-top: 1px solid #eeeeee;
                                                border-collapse: initial;
                                              "
                                              class="m_-6923522299734663521mlContentTable"
                                            ></table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <table
                              align="center"
                              border="0"
                              bgcolor="#FFFFFF"
                              class="m_-6923522299734663521mlContentTable"
                              cellpadding="0"
                              cellspacing="0"
                              width="640"
                              style="width: 640px; min-width: 640px"
                              id="m_-6923522299734663521transactional-16"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width="640"
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    ></table>
                                    <table
                                      cellpadding="0"
                                      cellspacing="0"
                                      border="0"
                                      align="center"
                                      width=""
                                      style="width: 640px; min-width: 640px"
                                      class="m_-6923522299734663521mlContentTable"
                                    >
                                      <tbody></tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <table
                    align="center"
                    border="0"
                    bgcolor="#FFFFFF"
                    class="m_-6923522299734663521mlContentTable"
                    cellpadding="0"
                    cellspacing="0"
                    width="640"
                    style="width: 640px; min-width: 640px"
                    id="m_-6923522299734663521transactional-4"
                  >
                    <tbody>
                      <tr>
                        <td style="padding: 0px">
                          <table
                            style="width: 100%"
                            role="presentation"
                            cellspacing="0"
                            cellpadding="0"
                          >
                            <tbody>
                              <tr>
                                <td>
                                  <table
                                    style="width: 100%"
                                    role="presentation"
                                    cellspacing="0"
                                    cellpadding="0"
                                  >
                                    <tbody>
                                      <tr>
                                        <td
                                          style="width: 100%; padding-bottom: 0px"
                                          valign="top"
                                        >
                                          <table
                                            cellpadding="0"
                                            cellspacing="0"
                                            width="100%"
                                            role="presentation"
                                            style="min-width: 100%"
                                          >
                                            <tbody>
                                              <tr>
                                                <td>
                                                  <table
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    width="100%"
                                                  >
                                                    <tbody>
                                                      <tr>
                                                        <td align="center" valign="top">
                                                          <table
                                                            border="0"
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="640"
                                                            style="
                                                              width: 640px;
                                                              min-width: 640px;
                                                              background-color: #ffffff;
                                                            "
                                                          >
                                                            <tbody>
                                                              <tr>
                                                                <td
                                                                  bgcolor="#C6CBDE"
                                                                  height="1"
                                                                  style="
                                                                    line-height: 1px;
                                                                    font-size: 1px;
                                                                  "
                                                                >
                                                                  &nbsp;
                                                                </td>
                                                              </tr>
        
                                                              <tr>
                                                                <td
                                                                  align="center"
                                                                  valign="top"
                                                                >
                                                                  <table
                                                                    border="0"
                                                                    cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="
                                                                      margin-top: 10px;
                                                                    "
                                                                  >
                                                                    <tbody>
                                                                      <tr>
                                                                        <td
                                                                          align="center"
                                                                          style="
                                                                            font-weight: 400;
                                                                            line-height: 20px;
                                                                            font-size: 14px;
                                                                            text-align: center;
                                                                            color: #4d536e;
                                                                          "
                                                                          valign="middle"
                                                                        >
                                                                          <a
                                                                            href="https://www.socialzinger.com/privacy-policy"
                                                                            style="
                                                                              color: #4d536e;
                                                                              text-decoration-color: #c6cbde;
                                                                            "
                                                                            target="_blank"
                                                                            data-saferedirecturl="https://www.socialzinger.com/privacy-policy"
                                                                            >Privacy
                                                                            Policy</a
                                                                          >&nbsp;&nbsp;·&nbsp;&nbsp;<a
                                                                            href="https://www.socialzinger.com/faq"
                                                                            style="
                                                                              color: #4d536e;
                                                                              text-decoration-color: #c6cbde;
                                                                            "
                                                                            target="_blank"
                                                                            data-saferedirecturl="https://www.socialzinger.com/faq"
                                                                            >FAQ</a
                                                                          >
                                                                        </td>
                                                                        <td
                                                                          align="center"
                                                                          style="
                                                                            font-weight: 400;
                                                                            line-height: 20px;
                                                                            font-size: 14px;
                                                                            text-align: center;
                                                                            color: #4d536e;
                                                                          "
                                                                          valign="middle"
                                                                        >
                                                                          <span
                                                                            >&nbsp;&nbsp;·&nbsp;&nbsp;</span
                                                                          >
                                                                          <a
                                                                            href={$unsubscribe}
                                                                            style="
                                                                              color: #4d536e;
                                                                              text-decoration-color: #c6cbde;
                                                                            "
                                                                            target="_blank"
                                                                            >
                                                                            Unsubscribe</a>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td
                                                                  height="25"
                                                                  style="
                                                                    line-height: 25px;
                                                                    font-size: 25px;
                                                                  "
                                                                >
                                                                  &nbsp;
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td
                                                                  style="
                                                                    font-weight: 400;
                                                                    line-height: 20px;
                                                                    font-size: 14px;
                                                                    text-align: center;
                                                                    color: #4d536e;
                                                                  "
                                                                  valign="top"
                                                                >
                                                                  If you have any
                                                                  questions, please
                                                                  email us at
                                                                  info@socialzinger.com
                                                                  or visit our .
                                                                  <a
                                                                    href="https://www.socialzinger.com/faq"
                                                                    style="
                                                                      text-decoration: none;
                                                                    "
                                                                    title="Additional terms"
                                                                    target="_blank"
                                                                    data-saferedirecturl="https://www.socialzinger.com/faq"
                                                                    >FAQs</a
                                                                  ><br />
                                                                  <br />
                                                                  You have recieved this
                                                                  email as a registered
                                                                  user of
                                                                  Socialzinger.com
                                                                  <br />You can
                                                                  <a
                                                                    href="#"
                                                                    style="
                                                                      color: #4d536e;
                                                                      text-decoration-color: #c6cbde;
                                                                    "
                                                                    target="_blank"
                                                                    data-saferedirecturl=""
                                                                    >Unsubscribe</a
                                                                  >
                                                                  from these emails
                                                                  here<br />
        
                                                                  <br />
                                                                  © 2023 SocialZinger.
                                                                  All Rights Reserved
                                                                </td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table
                    align="center"
                    border="0"
                    bgcolor=""
                    class="m_-6923522299734663521mlContentTable"
                    cellpadding="0"
                    cellspacing="0"
                    width="640"
                    style="
                      width: 640px;
                      min-width: 640px;
                      background-color: #fff;
                      margin-bottom: 20px;
                    "
                  >
                    <tbody>
                      <tr>
                        <td align="center" class="m_-6923522299734663521mlFooterText">
                          <table
                            cellpadding="0"
                            cellspacing="0"
                            border="0"
                            align="center"
                            width="640"
                            class="m_-6923522299734663521mlContentTable"
                            id="m_-6923522299734663521transactional-27"
                          >
                            <tbody>
                              <tr>
                                <td
                                  height="20"
                                  class="m_-6923522299734663521spacingHeight-40"
                                  style="
                                    line-height: 20px;
        
                                    min-height: 20px;
                                  "
                                ></td>
                              </tr>
                              <tr>
                                <td style="padding: 10px 0px" align="center">
                                  <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    align="center"
                                  >
                                    <tbody>
                                      <tr>
                                        <td
                                          align="center"
                                          width="24"
                                          style="padding: 0px 5px"
                                        >
                                          <a
                                            href="https://www.instagram.com/social_zinger/"
                                            ><img
                                              width="24"
                                              alt="instagram"
                                              src="https://ci4.googleusercontent.com/proxy/wBul1BZ-4Oj8faTArAt82rahihDlKTZsu8w4IPvYTXGqacLdQiKG4igmR3D6eOKnPcuBiZT4gBaoy-6Aa8ecBTQRQkesr3X1SV8shvNaAcFy9hAr7s93Tt0KCGzaVieO4d6V5jw=s0-d-e1-ft#https://cdn.mailerlite.com/images/icons/default/rounded_corners/grey/instagram.png"
                                              style="display: block"
                                              border="0"
                                              class="CToWUd"
                                              data-bit="iit"
                                          /></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td
                                  bgcolor="#C6CBDE"
                                  height="1"
                                  style="line-height: 1px; font-size: 1px"
                                >
                                  &nbsp;
                                </td>
                              </tr>
                              <tr>
                                <td
                                  width=""
                                  style="padding: 0px 40px"
                                  class="m_-6923522299734663521mlContentOuter"
                                >
                                  <p
                                    style="
                                      font-family: 'Open Sans', Arial, Helvetica,
                                        sans-serif;
                                      color: #6d6d6d;
                                      font-size: 12px;
                                      line-height: 150%;
                                      margin: 0 0 10px 0;
                                      font-weight: 400;
                                      margin-bottom: 10;
                                      margin-top: 10;
                                    "
                                  >
                                    If you didn't subscribe, you can just delete this
                                    email. You will not be subscribed to this newsletter
                                    unless you click the confirmation button above.
                                  </p>
                                </td>
                              </tr>
                              <tr>
                                <td
                                  height="20"
                                  class="m_-6923522299734663521spacingHeight-20"
                                ></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </div>`;

          try {
               await sendEmail({
                    email: signup.email,
                    name: signup.name,
                    subject: `Verify your email - socialzinger.com`,
                    message,
               });
          } catch (error) {
               signup.emailVerificationToken = undefined;
               signup.emailVerificationExpire = undefined;
               signup.isVerified = false;

               await signup.save({ validateBeforeSave: true });

               return res
                    .status(500)
                    .json({ success: false, message: error.message });
          }

          res.status(200).json({
               success: true,
               message: `Email sent to ${signup.email} successfully!`,
          });
     } catch (error) {
          const message = error.message;
          res.status(400).json({ success: false, message });
     }
}
