import axios from "axios"
import connectMongo from "../../../../../utils/db";


export default async function handler(req, res) {
    await connectMongo()


    if(req.method==="POST"){

        const {data} = req.body;

        console.log(data);

        res.status(200).json({success: true});
    }else{
        res.status(500).json({success: false});
    }
    

}