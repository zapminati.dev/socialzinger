import axios from "axios";
import connectMongo from "../../../../../utils/db";
const Signup = require("../../../../../src/models/freemiumModel");

export default async function handler(req, res) {
     await connectMongo();

     try {
          if (req.method === "POST") {

               const signup = await Signup.findOne({ email: req.body.email, isVerified: false });
               if (signup) {
                    
                    signup.emailVerificationToken = undefined;
                    signup.emailVerificationExpire = undefined;
                    signup.isVerified = false;

                    await signup.save({ validateBeforeSave: true });

                    return res.status(200).json({ success: true });
               }

               await Signup.create(req.body);

               res.status(200).json({ success: true });
          }
     } catch (error) {
          // console.log(error.message);

          if (error.code == 11000) {
               const message = `Duplicate ${Object.keys(
                    error.keyValue
               )} entered.`;
               res.status(400).json({ success: false, message });
          } else if (error.name === "ValidationError") {
               const message = error.message;
               res.status(400).json({ success: false, message });
          } else {
               const message = error.message;
               res.status(400).json({ success: false, message });
          }
     }
}
