import axios from "axios";
import crypto from "crypto"
import connectMongo from "../../../../../utils/db";
const Signup = require("../../../../../src/models/freemiumModel");

export default async function handler(req, res) {
     await connectMongo();
     try {

          //Creating token hash
          const emailVerificationToken = crypto
               .createHash("sha256")
               .update(req.body.token)
               .digest("hex");

          const signup = await Signup.findOne({
                emailVerificationToken,
                emailVerificationExpire: { $gt: Date.now() },
          });

          if (!signup) {
               return res.status(400).json({success: false, message: "Email verification token is invailid or has been expired."})
          }

          signup.emailVerificationToken = undefined;
          signup.emailVerificationExpire = undefined;
          signup.isVerified = true;

          await signup.save();

          res.status(200).json({ success: true });
     } catch (error) {
          const message = error.message;
          res.status(400).json({ success: false, message });
     }
}
