import axios from "axios";
import connectMongo from "../../../utils/db";
const Orders = require("../../../src/models/ordersModel");

export default async function handler(req, res) {
    await connectMongo();

    let orders = await Orders.find({orderStatus: 'pending'})

    // const orderStatus = await orders.findOneAndUpdate({_id: '6476b20224160ecccaed5b05'}, {orderStatus: 'processing'})
    // await orderStatus.save()

    orders.forEach( async (o) => {
        await updateOrder(o._id);
    });

    orders = await Orders.find()

    res.status(200).json({ sucess: true, orders })
}

async function updateOrder(oid) {

    const order = await Orders.findById(oid);

    order.orderStatus = 'processing';

    await order.save({ validateBeforeSave: false });
}