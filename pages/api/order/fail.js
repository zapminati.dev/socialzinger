import axios from "axios";
import connectMongo from "../../../utils/db";
import sendEmail from "../../../utils/mailerSend";

export default async function handler(req, res) {
     await connectMongo();
     try {
          const { name, email, serviceName, url, amount, quantity, errorMessage } = req.body;

          const orderFailed = `<!DOCTYPE html>
          <html lang="en">
          <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Order Failed</title>
            <style>
              /* Add your custom CSS styles here */
              body {
                font-family: Arial, sans-serif;
              }
              .container {
                max-width: 600px;
                margin: 0 auto;
                padding: 20px;
              }
              h1 {
                color: #333;
              }
              table {
                width: 100%;
              
              }
              th, td {
                padding: 10px;
                text-align: left;
                border-bottom: 1px solid #ddd;
                word-wrap: break-word;
              }
              tfoot tr:last-child td {
                font-weight: bold;
              }
              a{
                text-decoration: none;
              color: #000;
              }
            </style>
          </head>
          <body>
            <div class="container">
              <h1>Order Failed</h1>
              <p>Dear ${name},</p>
              <p>We regret to inform you that there was an issue processing your recent order on our website. We apologize for any inconvenience caused.</p>
          
              <h2>Order Details</h2>
              <table style="width: 100%;">
                 <tbody>
                  <tr>
                    <th>Service Name</th>
                    <td>${serviceName}</td>
                  </tr>
                  <tr>
                    <th>Quantity</th>
                    <td>${quantity}</td> 
                  </tr>
                  <tr>
                    <th>Price</th>
                    <td>${amount}</td>     
                  </tr>
                   <tr>
                    <th>Url</th>
                    <td width="300px">${url}</td>        
                  </tr>
                  <!-- Add more rows for additional products if necessary -->
                </tbody>
                
              </table>
          
              <h4>Reason for Failure:</h4>
               <p>${errorMessage}</p>
               <p>We apologize for any inconvenience caused by this issue. If you have any questions or require further assistance, please do not hesitate to reach out to us.</p>
               <p>Thank you for your understanding.</p>
          
              <p>Best regards,</p>
              <p>Social Zinger Team</p>
          
              <hr>
          <div style="background-color: #F2F2F2;">
            <table style="width: 100%;">
              <tr>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/privacy-policy">Privacy Policy</a>
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/faq">FAQ</a>
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/terms-and-conditions">Terms & Conditions</a> 
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href={$unsubscribe}>Unsubscribe</a> 
                  </p>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <p style="text-align: center; font-size: 12px; color: #888;">201, N Broad St, Middletown, New Castle, Delaware 19709</p>
                  <p style="text-align: center; color: #888; font-size: 12px;">
                    © 2023 socialzinger.com All rights reserved.
                  </p>
                </td>
              </tr>
            </table>
          </div>
          
            </div>
          </body>
          </html>
                   
          `;

          try {
               await sendEmail({
                    email: email,
                    name: "Social Zinger",
                    subject: `Your order has been declined!`,
                    message: orderFailed,
               });
          } catch (error) {
               return res
                    .status(500)
                    .json({ success: false, message: error.message });
          }

          res.status(200).json({
               success: true,
          });
     } catch (error) {
          const message = error.message;
          res.status(400).json({ success: false, message });
     }
}
