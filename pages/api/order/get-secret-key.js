import Stripe from "stripe"

export default async function handler(req, res) {

    const stripe = new Stripe(process.env.STRIPE_SECRET_KEY)

    const myPayment = await stripe.paymentIntents.create({
        amount: req.body.amount,
        currency: "usd",
        metadata: {
            company: "Socialzinger",
        },
    })


    res.status(200).json({ success: true, client_secret: myPayment.client_secret })
  }
  