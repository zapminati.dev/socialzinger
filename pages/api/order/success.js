import axios from "axios";
import connectMongo from "../../../utils/db";
import sendEmail from "../../../utils/mailerSend";
const Orders = require("../../../src/models/ordersModel");

export default async function handler(req, res) {
     await connectMongo();
     try {
          const { name, email, serviceName, url, amount, quantity, payment } = req.body;

          await Orders.create(req.body);

          const message = `<!DOCTYPE html>
          <html>
          <head>
          <style>
          table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
          }
          </style>
          </head>
          <body>
          <table style="width:100%">
            <tr>
              <td>Email</td>
              <td>${email}</td>
            </tr>
            <tr>
              <td>Service</td>
              <td>${serviceName}</td>
            </tr>
            <tr>
              <td>Url</td>
              <td>${url}</td>
            </tr>
            <tr>
              <td>Quantity</td>
              <td>${quantity}</td>
            </tr>
            <tr>
              <td>Price</td>
              <td>${amount}</td>
            </tr>
            <tr>
              <td>Transaction Id</td>
              <td>${payment.transaction_id}</td>
            </tr>
            <tr>
              <td>Payment Status</td>
              <td>${payment.status}</td>
            </tr>
          </table>
          </body>
          </html>`;

          try {
               await sendEmail({
                    email: "info@socialzinger.com",
                    name: "Socialzinger",
                    subject: `New lead - socialzinger.com`,
                    message,
               });
          } catch (error) {
               return res
                    .status(500)
                    .json({ success: false, message: error.message });
          }

          const orderConfirmation = `<!DOCTYPE html>
          <html lang="en">
          <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Order Failed</title>
            <style>
              /* Add your custom CSS styles here */
              body {
                font-family: Arial, sans-serif;
              }
              .container {
                max-width: 600px;
                margin: 0 auto;
                padding: 20px;
              }
              h1 {
                color: #333;
              }
              table {
                width: 100%;
              
              }
              th, td {
                padding: 10px;
                text-align: left;
                border-bottom: 1px solid #ddd;
                word-wrap: break-word;
              }
              tfoot tr:last-child td {
                font-weight: bold;
              }
              a{
                text-decoration: none;
              color: #000;
              }
            </style>
          </head>
          <body>
            <div class="container">
              <h1>Your Order has been fulfilled</h1>
              <p>Dear ${name},</p>
              <p>Thank you for availing our services.<p>
          <p>We are pleased to inform you that your order has been successfully fulfilled.</p>
          <p>You are now one step closer to your social media goals.</p>
          
              <h2>Your order details are as follows:</h2>
              <table style="width: 100%;">
                 <tbody>
                  <tr>
                    <th>Service Name</th>
                    <td>${serviceName}</td>
                  </tr>
                  <tr>
                    <th>Quantity</th>
                    <td>${quantity}</td> 
                  </tr>
                  <tr>
                    <th>Price</th>
                    <td>${amount}</td>     
                  </tr>
                   <tr>
                    <th>Url</th>
                    <td width="300px">${url}</td>        
                  </tr>
                  <!-- Add more rows for additional products if necessary -->
                </tbody>
                
              </table>
          
              <h4>Unleash your growth potential!</h4>
          <p>Our growth expert would like to discuss your future business plans with you and provide personalized assistance to make sure you make the most out of it.</p>
          <p>Book a quick call now and skyrocket to success.</p>
          <a rel="noopener" target="_blank" href="https://calendly.com/social-zinger/30min" class="cta btn-yellow" style="background-color: #F4D66C; font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight:bold; text-decoration: none; padding: 14px 20px; color: #1D2025; border-radius: 5px; display:inline-block; mso-padding-alt:0; box-shadow:0 3px 6px rgba(0,0,0,.2);"><span style="mso-text-raise:15pt;">Add to your Calendar</span></a>
          <p>We look forward to serving you again and continuing our partnership as your trusted social media agency.</p>
          
              <p>Best regards,</p>
              <p>Social Zinger Team</p>
          
              <hr>
          <div style="background-color: #F2F2F2;">
            <table style="width: 100%;">
              <tr>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/privacy-policy">Privacy Policy</a>
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/faq">FAQ</a>
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href="https://www.socialzinger.com/terms-and-conditions">Terms & Conditions</a> 
                  </p>
                </td>
                <td>
                  <p style="text-align: center; color: #888; font-size: 12px; margin-bottom: 0;">
                    <a href={$subscribe}>Unsubscribe</a> 
                  </p>
                </td>
              </tr>
              <tr>
                <td colspan="4">
                  <p style="text-align: center; font-size: 12px; color: #888;">201, N Broad St, Middletown, New Castle, Delaware 19709</p>
                  <p style="text-align: center; color: #888; font-size: 12px;">
                    © 2023 socialzinger.com All rights reserved.
                  </p>
                </td>
              </tr>
            </table>
          </div>
          
            </div>
          </body>
          </html>          
          `;

          try {
               await sendEmail({
                    email: email,
                    name: "Social Zinger",
                    subject: `Your order has been confirmed!`,
                    message: orderConfirmation,
               });
          } catch (error) {
               return res
                    .status(500)
                    .json({ success: false, message: error.message });
          }

          res.status(200).json({
               success: true,
          });
     } catch (error) {
          const message = error.message;
          res.status(400).json({ success: false, message });
     }
}
