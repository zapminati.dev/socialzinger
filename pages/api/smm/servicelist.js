import axios from "axios"


export default async function handler(req, res) {

    try {
        const options = {
             method: "POST",
             url: "https://www.smmraja.com/api/v3",
             params: { 
                key: process.env.SMM_RAJA_API_KEY,
                action: "services",
              },
        };

        const { data } = await axios.request(options);

        res.status(200).json({ data });
        
   } catch (error) {
        let statusCode = error.status;
        let errorMessage = error.message;

        res.status(statusCode).json({ errorMessage });
   }

}