import Router from "next/router";
import { useEffect, useState } from "react";
import Layouts from "../../src/layouts/Layouts";

export default function Success(){
    const [orderStatus, setOrderStatus] = useState("")

    useEffect(()=>{
        if(orderStatus.status !== 'succeeded'){
            Router.push("/")
        }
        setOrderStatus(JSON.parse(localStorage.getItem("orderInfo")))
    },[])
    
    return (
        <Layouts pageTitle="">
            <section className="sign-in-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                            <div className="sign-in-up-wrapper">
                                <form action="#">
                                    <div className="form-groups">
                                        <div className="m-b-20">
                                            <img src="/assets/img/cta/check.webp" />
                                        </div>


                                        <h4 className="form-title">Opps!</h4>
                                        <p>Something went wrong, please try again!</p>

                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layouts>
    );
}