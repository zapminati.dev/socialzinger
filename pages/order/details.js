
import Link from "next/link";
import Layouts from "../../src/layouts/Order";
import Header5 from "../../src/layouts/headers/Header5";
import PreLoader from "../../src/layouts/PreLoader";
import { useEffect, useState } from "react";
import { FormCheck } from "react-bootstrap";
import { useRouter } from "next/router"
import { useDispatch, useSelector } from "react-redux";
import { resetPackage, setPackage, setStatus } from "../../store/plans/packageSlice";
import { STATUSES } from "../../utils/status";

export default function Details () {

    const {data, status} = useSelector((state)=>state.package)

    const dispatch = useDispatch()

    const router = useRouter()

    const [selectedPackage, setSelectedPackage] = useState("")
    const [error, setError] = useState(false)

    useEffect(()=>{
        localStorage.clear()
        setSelectedPackage(JSON.parse(sessionStorage.getItem("packageDetails")))
    },[])

    const [email, setEmail] = useState("")
    const [link, setLink] = useState("")

    const handleCheckoutDetails = (e) => {
        e.preventDefault()

        dispatch(setStatus(STATUSES.LOADING))

        const constRegex = new RegExp( selectedPackage.regex )
        console.log(constRegex);

        if(!link.match(constRegex)){
            setError(true)
            dispatch(setStatus(STATUSES.IDLE))
            return true
        }
        setError(false)

        const orderDetails = {
            id: selectedPackage.id,
            serviceName: selectedPackage.serviceName,
            qty: selectedPackage.qty,
            sPrice: selectedPackage.sPrice,
            custEmail: email,
            custLink: link,
            custName: "",
            billingAddress: {
                state: "",
                zipcode: "",
            }
        }

        dispatch(setPackage(orderDetails))

        dispatch(setStatus(STATUSES.IDLE))

        if(status === STATUSES.IDLE){
            window.localStorage.setItem("orderDetails", JSON.stringify(orderDetails))

            router.push("/order/checkout")
        }
        
    }

    useEffect(()=>{
        dispatch(resetPackage())
        
    },[dispatch])

    return (
        <Layouts noHeader>
            <Header5 />
            <section className="data-analysis-section p-t-55 p-b-125">
                <div className="container p-t-100">
                    <div className="row align-items-top justify-content-center">

                        <div className="col-xl-7 col-lg-6 col-md-10 col-sm-10 ">
                            <div className="sign-in-up-wrapper">
                                <form onSubmit={(e)=> handleCheckoutDetails(e)}>
                                    <div className="form-groups">
                                        <h1 className="title">
                                            Order
                                        </h1><br/>
                                        <span className="text-danger">{error ? selectedPackage.errMsg : "" }</span>
                                        <hr />
                                        <div className="field-group">
                                            <label className="m-r-50 text-dark">{selectedPackage.serviceName}</label>
                                            <label className="m-r-50"><span className="text-dark">Quantity:</span> {selectedPackage.qty}</label>
                                            <label className="m-r-50"><span className="text-dark">Price:</span> ${selectedPackage.sPrice}</label>
                                        </div>
                                        <div className="field-group">
                                            <div className="icon">
                                                <i className="fa fa-envelope"></i>
                                            </div>
                                            <input
                                                type="email"
                                                name="email"
                                                placeholder="Email Address"
                                                required={true}
                                                value={email}
                                                onChange={(e)=> setEmail(e.target.value)}
                                            />
                                        </div>

                                        <div className="field-group" style={error?{border: "1px solid red"} : null}>
                                            <div className="icon">
                                                <i className="fas fa-solid fa-file-spreadsheet"></i>
                                            </div>
                                            <input
                                                type="url"
                                                placeholder="Link"
                                                required={true}
                                                name="link"
                                                value={link}
                                                onChange={(e)=> setLink(e.target.value)}
                                            />
                                        </div>

                                        <div className="field-group m-t-20">
                                            <button
                                                type="submit"
                                                className="template-btn  template-btn-2"
                                                // href="/service-checkout"
                                                required={true}
                                            >
                                                Checkout{" "}
                                                <i className="far fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div className="form-note">
                                        <p className="small">
                                            By completing your order,
                                            you agree to the{" "}
                                            <a href="/terms-and-conditions">
                                                terms of services
                                            </a>{" "}
                                            and{" "}
                                            <a href="/privacy-policy">
                                                privacy policy
                                            </a>
                                            .
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layouts>
    );
};

