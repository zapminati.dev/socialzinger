import Link from "next/link";
import Layouts from "../../src/layouts/Order";
import Header5 from "../../src/layouts/headers/Header5";
import { useEffect, useState } from "react";
import {Elements} from "@stripe/react-stripe-js"
import {loadStripe} from "@stripe/stripe-js"
import PaymentForm from "../../src/element/PaymentForm";
import Router from "next/router";


const Checkout = () => {

    const [selectedPackage, setSelectedPackage] = useState("")

    useEffect(()=>{
        const orderDetails = JSON.parse(window.localStorage.getItem("orderDetails"));

        if(!orderDetails){
            Router.push("/")
        }

        setSelectedPackage(orderDetails)
    },[])

    return (

        <Elements stripe={loadStripe(process.env.STRIPE_API_KEY)}>

        <Layouts noHeader>
            <Header5 />
            
            <section className="data-analysis-section p-t-155 p-b-125">
                <div className="container">
                    <div className="row align-items-top justify-content-center">

                        <div className="col-xl-7 col-lg-6 col-md-10 col-sm-10">

                            <div className="sign-in-up-wrapper">
                                {selectedPackage? <PaymentForm/> : null}
                            </div>
                        </div>

                        {/* <div className="col-lg-5 col-md-10">
                            <div className="sign-in-up-wrapper">
                                <form action="#">
                                    <div className="form-groups">

                                        <hr />

                                        <div className="field-group">
                                            <div className="icon follower-list">
                                                <i className="far fa-heart"></i>
                                            </div>
                                            <span className="likes-all"><strong>50 likes
                                            </strong> </span> <span className="right-side-text ">$2.97</span>


                                        </div>
                                        <div className="field-group special-offer mb-5 mt-5">
                                            <div className="icon follower-list">
                                                <i className="far fa-heart"></i>
                                            </div>

                                            <span>Add 12 likes per post and save 25%</span>

                                            <div className="masonry-item pt-3">
                                                <div className="more-btn text-center">
                                                    <a className="template-btn  template-btn-2 success-bg-2" href="/">+ Add for $0.55 <i className="far fa-arrow-right"></i></a>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="field-group text-left p-t-0">

                                            <span className="pay-value">Total to pay</span> <span className="right-side-text pay-value">$1.47</span>

                                        </div>
                                        <hr />
                                    </div>
                                </form>
                            </div>
                            <div className="r-feature">
                                <div className="benefit-content">

                                    <ul
                                        className="check-list-3 wow fadeInUp"
                                        data-wow-delay="0.2s"
                                    >
                                        <li>
                                            <p>Real likes from real people</p>
                                        </li>
                                        <li>
                                            <p>Split likes on multiple posts</p>
                                        </li>
                                        <li>
                                            <p>Video views included</p>
                                        </li>
                                        <li>
                                            <p>No Instagram password required</p>
                                        </li>
                                        <li>
                                            <p>Fast delivery, up to 10 mins</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> */}
                    </div>
                </div >
            </section >

        </Layouts >

        </Elements>
    );
};

export default Checkout;
