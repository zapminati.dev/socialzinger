import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Layouts from "../../src/layouts/Layouts";
import Preloader from "../../src/layouts/PreLoader";

export default function Success() {
     const router = useRouter();

     const [orderStatus, setOrderStatus] = useState("");
     const [loader, setLoader] = useState(true);

     const saveData = async (email, link, qty, sPrice, serviceName) => {
          try {
               const { data } = await axios.post(`/api/order/success`, {
                    name: router.query?.name ? router.query?.name : 'Customer',
                    email: email,
                    serviceName: serviceName,
                    url: link,
                    amount: sPrice,
                    quantity: qty,
                    payment: {
                         transaction_id: router.query?.transaction_id,
                         status: router.query?.status,
                    },
               });
               //console.log(data.success);
               if (data.success) {
                    setLoader(false);
                    // sessionStorage.clear();
               }
          } catch (error) {
               setLoader(false);
               console.log(error.message);
          }
     };

     useEffect(async()=>{
          const order = JSON.parse(localStorage.getItem('orderDetails'));

          if (router.isReady) {
               if (!router.query.transaction_id) {
                    router.push("/");
               } else {
                    await saveData(order.custEmail, order.custLink, order.qty, order.sPrice, order.serviceName);
               }
          }
     },[])

     return (
          <Layouts pageTitle="">
               {loader ? (
                    <Preloader />
               ) : (
                    <section className="sign-in-section p-t-50 p-b-50">
                         <div className="container">
                              <div className="row justify-content-center">
                                   <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                                        <div className="sign-in-up-wrapper">
                                             <form action="#">
                                                  <div className="form-groups">
                                                       <div className="m-b-20">
                                                            <img src="/assets/img/particle/check.webp" />
                                                       </div>

                                                       <h4 className="form-title">
                                                            Success!
                                                       </h4>
                                                       <p>
                                                            We received your
                                                            purchase request.
                                                            we'll be in touch
                                                            shortly!
                                                       </p>
                                                  </div>
                                             </form>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
               )}
          </Layouts>
     );
}
