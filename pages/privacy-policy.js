import Link from "next/link";
import Layouts from "../src/layouts/Layouts";
const PrivacyPolicy = () => {
    return (
        <Layouts pageName="PrivacyPolicy">


            {/*====== Start Design & Layout Section ======*/}
            <section className="sof-design-layout p-t-50 p-b-0">
                <div className="container">
                    <div className="row align-items-center justify-content-center no-gutters">

                        <div className="col-lg-12 col-md-10">
                            <div className="software-text-block p-l-50 p-l-lg-30 p-l-md-0">
                                <div className="common-heading m-b-40">

                                    <h2 className="title"> Privacy Policy</h2>
                                </div>
                                <p>
                                    We appreciate you using our goods and services (the "Services"). Social Zinger is the provider of the Services. By using this website, you accept its Terms and Conditions of Use, all applicable laws and regulations, and the responsibility for adhering to any local laws that may be in force in your area. You are not permitted to use or access this website if you disagree with any of these terms.

                                </p>
                                <p className="m-t-25">The information on the Social Zinger website is given "as is." Social Zinger makes no representations or warranties, whether express or implied, and hereby disclaims and negates any and all representations and warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other rights. Additionally, Social Zinger makes no warranties or representations of any kind regarding the truthfulness, possible outcomes, or reliability of the use of the materials on its Internet website or otherwise relating to such materials or any sites linked to this one.
                                </p>

                                <div className="software-text-block">
                                    <div className="common-heading m-t-20">
                                        <span className="tagline color-primary-5">
                                            Revision
                                        </span>
                                    </div>
                                    <p className="m-b-25">
                                        There could be technical, typographical, or photographic errors in the content on Social Zinger's website. None of the content on Social Zinger's website is guaranteed to be accurate, full, or up to date. Social Zinger has the right to make changes to the information on its website at any time and without prior notice. Social Zinger does not, however, guarantee that the materials will be updated.
                                    </p>
                                    <div className="common-heading m-t-20">
                                        <span className="tagline color-primary-5">
                                            Our Support
                                        </span>
                                    </div>
                                    <p className="m-b-25">
                                        We make an effort to give our customers service that is unmatched in the business. Please email us at support@socialzinger.com if you have any sales or technical inquiries.

                                    </p>
                                    <div className="common-heading m-t-20">
                                        <span className="tagline color-primary-5">
                                            Refunds
                                        </span>
                                    </div>
                                    <p className="m-b-25">
                                        Refunds Within the first 30 days of their purchase, customers who are not entirely pleased with our services may ask for a full refund. Our customer service representatives, who may be reached at support@socialzinger.com, will handle refund processing. Free credits or services are not eligible for refunds. Social Zinger reserves the right to satisfy customers, and all refunds must be handled with care and according to the rules.

                                    </p>
                                    <div className="common-heading m-t-20">
                                        <span className="tagline color-primary-5">
                                            Service Related:
                                        </span>
                                    </div>

                                    <ul className="check-list m-t-30 m-b-30">
                                        <li>Social Zinger has absolutely nothing to do with Instagram, Facebook, or any of its third-party partners.</li>
                                        <li>It is entirely your obligation to abide by Instagram's policies as well as any applicable laws. Social Zinger is used at the user's own risk.
                                        </li>
                                        <li>Your choices and their effects are not our responsibility. If your Instagram account is blocked for whatever reason, we are not to fault.
                                        </li>
                                        <li>To get the necessary data for the Instagram API, we need your Instagram username. Your username is not kept, distributed, or otherwise made available to outside parties by us.
                                        </li>
                                        <li>You are in no way guaranteed to receive the anticipated amount of likes, follows, or views.</li>
                                        <li>We are unable to guarantee that the services will operate continuously, without interruption, or without errors.</li>
                                    </ul>
                                    <div className="common-heading m-t-20">
                                        <span className="tagline color-primary-5">
                                            Privacy Policy

                                        </span>
                                    </div>
                                    <p className="m-b-25">
                                        At Social Zinger, we place a high importance on our clients' security and privacy, both current and potential. You can find a brief summary of the data we typically learn about website visitors on this page, along with clear and succinct definitions of each. Please don't hesitate to write us a note and contact us through our Contact Us page if you have any additional questions. This page's information is applicable to Social Zinger. You agree to grant us your agreement to accept the collection and storage of the information described below by using our website.

                                    </p>
                                    <h6 className="title m-b-25">Social Zinger has no association whatsoever with Instagram, Facebook, or any of its third-party partners. Instagram is not affiliated with, supported by, or sponsored by us.</h6>
                                    <p className="m-b-25">Any and every information gathered during your visit to a website is intended to make browsing simpler and faster. This is accomplished by examining the patterns in your choices and behaviors that lead you to websites of interest, including but not exclusively our services and affiliated companies. This promotes the expansion and development of not just our own company, but also other companies. Typically, the information we gather and store about you is used to assist you learn more about any of our services and products as well as those of our affiliates. Additionally, it enables us to make changes to our website that we otherwise might not have been able to.</p>
                                    <p>The information on this page might not always be applicable to other links and websites, and by visiting those websites, you consent to their respective policies and terms of use. Please be aware that no data or information can ever be guaranteed to be secure, and Social Zinger disclaims all liability for any losses or damages that may result from data or information corruption or unauthorized access.
                                    </p>
                                    <p className="m-b-25">Please send us a message through our Contact Us page if you are an existing customer who wants to make changes to your accounts or decides you no longer want to be a part of Social Zinger, and we will take care of it for you. However, please be aware that any significant changes may have a significant impact on any services you may currently have with us.
                                    </p>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>
            </section>
            {/*====== End Design & Layout Section ======*/}




        </Layouts>
    );
};

export default PrivacyPolicy;
