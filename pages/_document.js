import { Html, Head, Main, NextScript } from "next/document";
import Script from "next/script";

export default function Document() {
     return (
          <Html>
               <Head>
                    <link
                         rel="stylesheet"
                         href="/assets/css/bootstrap.min.css"
                    />
                    <link rel="stylesheet" href="/assets/css/slick.min.css" />
                    <link
                         rel="stylesheet"
                         href="/assets/css/magnific-popup.min.css"
                    />
                    <link
                         rel="stylesheet"
                         href="/assets/css/nice-select.min.css"
                    />
                    <link
                         rel="stylesheet"
                         href="/assets/css/animate.min.css"
                    ></link>
                    <link
                         rel="stylesheet"
                         href="/assets/fonts/fontawesome/css/all.min.css"
                    />
                    <link
                         rel="stylesheet"
                         href="/assets/fonts/flaticon/flaticon.css"
                    ></link>
                    <link
                         rel="stylesheet"
                         href="/assets/css/spacing.min.css"
                    ></link>
                    <link rel="stylesheet" href="/assets/css/style.css" />
                    <link rel="stylesheet" href="/assets/css/responsive.css" />
                    
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
                    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet" />

                    <meta
                         name="google-site-verification"
                         content="0ye7fSI6KdYLsdVfziHh9OiSK-BVEEGkWJHsBmzcBto"
                    />

                    <Script
                         strategy="afterInteractive"
                         async
                         src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js"
                    ></Script>


                    {/* Google tag (gtag.js) */}
                    {/* <Script
                         id="GTM-MP4C5BP"
                         strategy="afterInteractive"
                         dangerouslySetInnerHTML={{
                              __html: `
                              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                              })(window,document,'script','dataLayer','GTM-MP4C5BP');
               `,
                         }}
                    ></Script> */}
                    {/* Google tag (gtag.js) */}
                    <Script strategy="afterInteractive" async src="https://www.googletagmanager.com/gtag/js?id=G-5VD0DSEZRZ"></Script>
                    <Script
                         id="GTM-MP4C5BP"
                         strategy="afterInteractive"
                         dangerouslySetInnerHTML={{
                              __html: `
                              window.dataLayer = window.dataLayer || [];
                              function gtag(){dataLayer.push(arguments);}
                              gtag('js', new Date());

                              gtag('config', 'G-5VD0DSEZRZ');
               `,
                         }}
                    ></Script>
                    
                    

               </Head>
               <body>
                    <Main />
                    <NextScript />
                    {/* <Script
                         strategy="afterInteractive"
                         dangerouslySetInnerHTML={{
                              __html: `
    window.omnisend = window.omnisend || [];
    omnisend.push(["accountID", "64141989b7d5173400850e40"]);
    omnisend.push(["track", "$pageViewed"]);
    !function(){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="https://omnisnippet1.com/inshop/launcher-v2.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
`,
                         }}
                    ></Script> */}
                    {/* <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MP4C5BP"
                    height="0" width="0" style={{display:"none",visibility:"hidden"}}></iframe></noscript> */}

                    {/* <!--Start of Tawk.to Script--> */}
                    {/* <Script 
                         strategy="afterInteractive"
                         dangerouslySetInnerHTML={{__html: `var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                         (function(){
                         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                         s1.async=true;
                         s1.src='https://embed.tawk.to/64788859ad80445890f06ded/1h1rd97vq';
                         s1.charset='UTF-8';
                         s1.setAttribute('crossorigin','*');
                         s0.parentNode.insertBefore(s1,s0);
                         })();`}}></Script> */}

               <Script
                    strategy="afterInteractive"
                    dangerouslySetInnerHTML={{
                         __html: `var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date(); Tawk_API.embedded='tawk_64788859ad80445890f06ded';
                         (function(){
                         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                         s1.async=true;
                         s1.src='https://embed.tawk.to/64788859ad80445890f06ded/1h31a93cs';
                         s1.charset='UTF-8';
                         s1.setAttribute('crossorigin','*');
                         s0.parentNode.insertBefore(s1,s0);})();`,
                    }}
               ></Script>

                    {/* <!--End of Tawk.to Script--> */}
               </body>
          </Html>
     );
}
