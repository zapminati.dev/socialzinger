import Link from "next/link";
import Layouts from "../src/layouts/Layouts";
import AccordionV15 from "../src/components/Accordion/AccordionV15";
import { Tab } from "react-bootstrap";
import Slider from "react-slick";
import Plans from "../src/components/Packages";
import TestimonialMain from "../src/components/Testimonial/testimonial";

const InstagramComments = () => {

    return (
        <Layouts pageName="instaComments">
            {/* <!--====== Service Section Start ======--> */}

            {/* <!--====== Start Section 1 ======--> */}
            <section className="data-analysis-section p-t-10 p-b-50">
                <div className="container">
                    <div className="row align-items-top text-center justify-content-center">
                        <div className="col-lg-7 col-md-10">
                            <div className="analysis-text-block  p-l-md-0">
                                <div className="common-heading m-b-30">
                                    <h1 className="tagline">Buy Instagram comments</h1>
                                    <h2 className="title">Buy Instagram Comments with <span
                                        // className="color-4" 
                                        style={{ "color": "rgba(213, 62, 145, 1)" }}> Instant Delivery</span> </h2>
                                </div>

                            </div>
                        </div>

                        <Plans servicePageName="InstagramComments" />
                        <div className="col-lg-7 col-md-10">
                            <div className="analysis-text-block ">
                                <div className="common-heading ">
                                    <p className="color-5">
                                        Don't bother wasting time and energy looking for ways to have people leave comments on your Instagram content.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section >

            {/*====== Service Section Start ======*/}


            <section className="service-section border-bottom-primary-3 p-t-50 p-b-70 "
            >
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-6 col-lg-8">
                            <div className="main-top-heading title-line-bottom text-center m-b-20">
                                <h2 className="title">
                                    How It Works
                                </h2>
                            </div>
                        </div>
                        <div className="col-xl-12 col-lg-12">
                            <div className="row fancy-icon-boxes-v2">
                                <div className="col-lg-4 d-flex wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-2 m-t-30" >

                                        <div className="box-content works-content">
                                            <h3 className="title">
                                                Handpick Your Package
                                            </h3>
                                            <p>
                                                Browse through our exciting selection and unleash the power of engagement
                                                on your profile. Got questions or looking for a killer deal on a bulk order?
                                                Don't hesitate to reach out – our team is here to make your Insta dreams come true!
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 d-flex wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-4 m-t-30" >

                                        <div className="box-content works-content">
                                            <h3 className="title">
                                                Little Detail About You
                                            </h3>
                                            <p>
                                                Your Instagram security is our top priority and will never, ever ask for sensitive information like your password. Simply provide us with your Instagram username, and let us handle the rest! and watch your profile flourish without any worries.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 d-flex wow fadeInUp" data-wow-delay="0.4s">
                                    <div className="fancy-icon-box color-3 m-t-30" >

                                        <div className="box-content works-content">
                                            <h3 className="title">
                                                Let the magic unfold
                                            </h3>
                                            <p>
                                                No time to waste! Once you place your order, expect lightning-fast results. Our efficient system ensures that all orders kick off within minutes, meaning you'll witness rapid growth in your Instagram account in no time.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <TestimonialMain />
            {/* <!--====== End Faq With SEO score box ======--> */}

            <section className="fag-section p-t-50 p-b-50" style={{ "background": "#F6F6F6" }}>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-12">
                            <div className=" main-top-heading tagline-boxed m-b-30">
                                <span className="tagline-red">Ask Away!</span>
                                <h3 className="heading">Frequently Asked Questions</h3>
                            </div>
                            <Tab.Container defaultActiveKey="general">
                                <div className="accordion-tab">

                                    <Tab.Content>
                                        <Tab.Pane eventKey="general">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="speakers">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="pricing">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="support">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="history">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="customers">
                                            <div className="landio-accordion-v2">
                                                <AccordionV15 />
                                            </div>
                                        </Tab.Pane>
                                    </Tab.Content>
                                </div>
                            </Tab.Container>

                            <div className="field-group" style={{ "textAlign": "left", "marginTop": "20px" }}>

                                <Link href="/">
                                    <a className="btn-blue">
                                        Sign me up NOW!
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </Layouts >
    );
};

export default InstagramComments;
