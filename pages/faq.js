import Link from "next/link";
import { useState } from "react";
import Layouts from "../src/layouts/Layouts";
import AccordionV1 from "../src/components/Accordion/AccordionV1";

const Index9 = () => {
    const [video, setVideo] = useState(false);
    return (
        <Layouts>

            {/* <!--====== Start FAQ section ======--> */}
            <section className="faq-section bg-soft-grey-color p-t-120 p-b-120">
                <div className="container">
                    <div className="row align-items-center justify-content-center">
                        <div className="col-lg-6 col-md-10">
                            <div className="faq-content p-r-60 p-r-lg-30 p-r-md-0">
                                <div className="common-heading tagline-boxed-two title-line m-b-80">

                                    <h2 className="title">
                                        The SocialZinger Process is Simple:
                                        Instagram {" "}
                                        <span className="color-4">
                                            Popularity!{" "}

                                        </span>
                                    </h2>
                                </div>
                                <div className="landio-accordion-v1">
                                    <AccordionV1 />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-9">
                            <div className="faq-image text-lg-right m-t-md-60">
                                <img
                                    src="assets/img/faq/bg4.png"
                                    alt="faq image"
                                    className="animate-float-bob-y"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* <!--====== End Faq With SEO score box ======--> */}



            {/*====== Start Footer ======*/}

        </Layouts>
    );
};

export default Index9;
