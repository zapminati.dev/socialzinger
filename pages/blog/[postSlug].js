import React from "react";
import Layouts from "../../src/layouts/Layouts";
import Link from "next/link";
import Slider from "react-slick";
import { testimonialwidgetactive } from "../../src/sliderProps";
import {
     getAllCategories,
     getMetaData,
     getPostSlugs,
     getSinglePost,
} from "../../src/lib/posts";
import Date from "../../src/components/Blog/Date";
import FeaturedImage from "../../src/components/Blog/FeaturedImage";
import PostComment from "../../src/components/Blog/PostComment";
import BlogSearch from "../../src/components/Blog/BlogSearch";
import BlogCategoryList from "../../src/components/Blog/BlogCategoryList";
import LatestArticle from "../../src/components/Blog/LatestArticle";
import PostFooter from "../../src/components/Blog/PostFooter";
import { getRequest } from "../../src/lib/fetchRequest";
import Head from "next/head";
import parse from "html-react-parser";

export async function getStaticPaths() {
     const postSlugs = await getPostSlugs();

     return {
          paths: postSlugs.map((s) => ({
               params: {
                    postSlug: s.slug,
               },
          })),
          fallback: false,
     };
}

export async function getStaticProps({ params }) {
     const postData = await getSinglePost(params.postSlug);
     const categories = await getAllCategories();
     const metaData = await getMetaData(params.postSlug);

     return {
          props: {
               postData,
               categories,
               metaData,
          },
          revalidate: 60
     };
}

export default function Post({ postData, categories, metaData }) {
     // console.log(metaData);

     return (
          <>
               <Head>
                    {parse(metaData)}
               </Head>
               <Layouts>
                    <section className="page-title-area p-5">
                         <div className="container">
                              <div className="page-title-content text-center">
                                   <h1 className="sub-title">
                                        {/* <Link href={`/blog/${postData?.slug}`}> */}
                                        {postData?.title}
                                        {/* </Link> */}
                                   </h1>
                                   <ul className="breadcrumb-nav mt-2">
                                        <li>
                                             <Link href="/">Home</Link>
                                        </li>
                                        <li className="active">
                                             <Link href="/blog">Blog</Link>
                                        </li>
                                   </ul>
                              </div>
                         </div>
                         <div className="page-title-effect d-none d-md-block">
                              <img
                                   className="particle-1 animate-zoom-fade"
                                   src="/assets/img/particle/particle-1.webp?png"
                                   alt="particle One"
                              />
                              <img
                                   className="particle-2 animate-rotate-me"
                                   src="/assets/img/particle/particle-2.webp?png"
                                   alt="particle Two"
                              />
                              <img
                                   className="particle-3 animate-float-bob-x"
                                   src="/assets/img/particle/particle-3.webp?png"
                                   alt="particle Three"
                              />
                              <img
                                   className="particle-4 animate-float-bob-y"
                                   src="../assets/img/particle/particle-4.webp?png"
                                   alt="particle Four"
                              />
                              <img
                                   className="particle-5 animate-float-bob-y"
                                   src="../assets/img/particle/particle-5.webp?png"
                                   alt="particle Five"
                              />
                         </div>
                    </section>
                    {/*====== Start Latest News ======*/}
                    <section className="blog-area p-t-130 p-b-130">
                         <div className="container">
                              <div className="row justify-content-center">
                                   <div className="col-lg-8">
                                        {/* <!-- Blog Content --> */}
                                        <div className="blog-details-content p-r-40 p-r-lg-0">
                                             <div className="post-thumbnail">
                                                  <FeaturedImage
                                                       post={postData}
                                                  />
                                             </div>

                                             <div className="post-content">
                                                  <ul className="post-meta">
                                                       <li>
                                                            <i className="far fa-user"></i>
                                                            {
                                                                 postData
                                                                      ?.author
                                                                      ?.node
                                                                      ?.name
                                                            }
                                                       </li>
                                                       <li>
                                                            <Date
                                                                 dateString={
                                                                      postData?.date
                                                                 }
                                                            />
                                                       </li>
                                                       {/* <li>
                                                            <a
                                                                 href="#"
                                                                 className="post-meta"
                                                            >
                                                                 <i className="far fa-comment-dots"></i>
                                                                 Comments (05)
                                                            </a>
                                                       </li> */}
                                                  </ul>
                                                  {/* <h3 className="post-title">
                                                       <Link
                                                            href={`/blog/${postData?.slug}`}
                                                       >
                                                            {postData?.title}
                                                       </Link>
                                                  </h3> */}
                                                  <div
                                                       dangerouslySetInnerHTML={{
                                                            __html: postData?.content,
                                                       }}
                                                  ></div>
                                             </div>
                                             {/* <PostFooter/> */}
                                        </div>
                                        {/* <!-- Comments Template --> */}
                                        {/* <PostComment /> */}
                                   </div>
                                   <div className="col-lg-4">
                                        <div className="blog-sidebar m-t-md-80">
                                             {/* <BlogSearch/> */}
                                             <BlogCategoryList
                                                  categories={categories}
                                             />
                                             {/* <LatestArticle/> */}
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
                    {/*====== End Latest News ======*/}
               </Layouts>
          </>
     );
}
