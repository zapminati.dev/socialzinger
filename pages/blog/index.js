import React, { useState } from "react";
import Layouts from "../../src/layouts/Layouts";
import Link from "next/link";
import BlogCard from "../../src/components/Blog/BlogCard";
import { getPostList } from "../../src/lib/posts";
import LoadMore from "../../src/components/Blog/LoadMore";

export async function getStaticProps() {
     const allPosts = await getPostList();
   
     return {
       props: {
         allPosts: allPosts,
       },
       revalidate: 60
     };
   }

export default function BlogHome({ allPosts }) {
     // console.log(allPosts);
     const[posts, setPosts] = useState(allPosts)

     return (
          <>
               <Layouts pageTitle="">
                    <section className="page-title-area p-5">
                         <div className="container">
                              <div className="page-title-content text-center">
                                   <h1 className="page-title">Latest News</h1>
                                   <ul className="breadcrumb-nav">
                                        <li>
                                             <a href="/">Home</a>
                                        </li>
                                        <li className="active">Blog</li>
                                   </ul>
                              </div>
                         </div>
                         <div className="page-title-effect d-none d-md-block">
                              <img
                                   className="particle-1 animate-zoom-fade"
                                   src="/assets/img/particle/particle-1.webp?png"
                                   alt="particle One"
                              />
                              <img
                                   className="particle-2 animate-rotate-me"
                                   src="/assets/img/particle/particle-2.webp?png"
                                   alt="particle Two"
                              />
                              <img
                                   className="particle-3 animate-float-bob-x"
                                   src="/assets/img/particle/particle-3.webp?png"
                                   alt="particle Three"
                              />
                              <img
                                   className="particle-4 animate-float-bob-y"
                                   src="/assets/img/particle/particle-4.webp?png"
                                   alt="particle Four"
                              />
                              <img
                                   className="particle-5 animate-float-bob-y"
                                   src="/assets/img/particle/particle-5.webp?png"
                                   alt="particle Five"
                              />
                         </div>
                    </section>
                    {/*====== Start Latest News ======*/}
                    <section className="latest-news-with-seo-box">
                         <div className="latest-news-section bg-light-color pb-5 pt-5">
                              <div className="container">
                                   
                                   <div className="row justify-content-center latest-news-v1">
                                        {posts?.nodes?.map((post) => (
                                             <BlogCard key={post?.slug} post={post} />
                                        ))}
                                   </div>
                                   <div className="row justify-content-center">
                                        <LoadMore posts={posts} setPosts={setPosts}/>
                                   </div>
                              </div>
                         </div>
                    </section>
                    {/*====== End Latest News ======*/}
               </Layouts>
          </>
     );
}
