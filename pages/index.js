import Link from "next/link";
import Slider from "react-slick";
import Layouts from "../src/layouts/Layouts";
import AccordionV27 from "../src/components/Accordion/AccordionV27";
import { Tab } from "react-bootstrap";
import { brandslidertwoactive } from "../src/sliderTestimonial";
import UseCaseOfficial from "../src/components/useCase";


const Index = ({ value }) => {
    const renderStars = () => {
        const stars = [];
        const maxRating = 5;

        for (let i = 1; i <= maxRating; i++) {
            const starClass = i <= value ? 'fa fa-star' : 'fa fa-star';
            stars.push(<i key={i} className={starClass}></i>);
        }

        return stars;
    };
    return (


        <Layouts pageName="homepage">
            {/* <!--====== Start Section 1 ======--> */}
            <section className="data-analysis-section p-t-55" style={{ backgroundColor: "#F6F5F8" }}>
                <div className="container">
                    <div className="row align-items-top justify-content-center">
                        <div className="col-lg-9 col-md-12">
                            <div className="analysis-text-block">
                                <div className="common-heading text-center m-b-30">
                                    <h2 className="heading-new-layout">
                                        Next-gen agency that<br />
                                        boosts your brand and social media
                                    </h2>
                                    <h1 className="tagline m-t-20">Transform your business revenue with the influential force of Social Zinger.</h1>
                                    <div
                                        className="seo-score-box newsletters-form particle-image-two wow "

                                    >
                                        <div className="row justify-content-center">
                                            <div className="col-xl-8 col-md-10 col-11">
                                                <form className="score-box-form">
                                                    <div className="form-group">
                                                        <input
                                                            type="email"
                                                            name="email"
                                                            id="emailAddress"
                                                            placeholder="Email Address"
                                                        />
                                                        <button
                                                            type="submit"
                                                            name="submit"
                                                            className="template-btn secondary-bg"
                                                        >
                                                            Get Started
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-lg-10 col-md-10">

                            <div className="hero-img text-center ">
                                <img className="custom-banner-image" src="/assets/img/Banner/topBanner.webp" alt="image" />
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            {/*======  Start Client Section======*/}
            <section className="service-area m-t-40 p-b-20">
                <div className="container">
                    <div className="">
                        <div className="row align-items-center">
                            <div className="col-md-4 col-sm-5 col-4">
                                <div className="icon-box justify-content-center">
                                    <div className="box-icon">
                                        <img src="/assets/img/logo/1.png" alt="followers-sold" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-5 col-4">
                                <div className="icon-box justify-content-center">
                                    <div className="box-icon">
                                        <img src="/assets/img/logo/2.png" alt="likes-sold" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-5 col-4">
                                <div className="icon-box justify-content-center">
                                    <div className="box-icon">
                                        <img src="/assets/img/logo/3.png" alt="likes-sold" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </section>


            {/*======  Start Rating Section======*/}

            <section className="feature-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-between custom-border">
                        <div className="col-xl-6 col-lg-5 col-md-12">
                            <div className=" title-line-bottom m-b-50">

                                <h5 className="title ">More than 10,000 creators from various domains<br></br>
                                    use social zinger</h5>
                                <p className="subheading">Boost revenue, gain engagement that help you grow in scale faster.</p>
                            </div>
                        </div>
                        <div className="col-lg-5">
                            {/* <!-- Feature Boxes --> */}
                            <div className="row justify-content-between iconic-boxes-v1">
                                <div className="col-xl-5 col-lg-6 col-md-5 col-sm-6">
                                    <div className=" no-shadow">

                                        <h4 className="title">4.9</h4>
                                        <div className="rating custom-rating">{renderStars()}</div>

                                    </div>
                                </div>
                                <div className="col-xl-5 col-lg-6 col-md-5 col-sm-6">
                                    <div className=" no-shadow">

                                        <h4 className="title">4.8</h4>
                                        <div className="rating custom-rating">{renderStars()}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/*====== start all service usercase======*/}
            <section className="service-area p-t-35" >
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7">
                            <div className="main-top-heading text-center m-b-65">
                                <h2 className="heading-new-layout">
                                    Explore our amazing services
                                </h2>
                                <p className="custom-para">We are team of experts and help you to reach where your audience is</p>
                                <h3 className="custom-heading-after">Multi Platform Services</h3>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="service-tab">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-7">
                                <div className="text-center">
                                    <ul
                                        className="nav service-tab-nav justify-content-around"
                                        id="myTab"
                                        role="tablist"
                                    >
                                        <li className="nav-item" role="presentation">
                                            <a
                                                data-toggle="tab"
                                                href="/facebook"
                                                role="tab"
                                            >
                                                <img src="/assets/img/icon/website.png" alt="likes-sold" />
                                                <h5 className="title custom-service-head">Website Development<br />
                                                    plan</h5>
                                            </a>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <a
                                                data-toggle="tab"
                                                href="/tiktok"
                                                role="tab"
                                            >
                                                <img src="/assets/img/icon/seo.png" alt="likes-sold" />
                                                <h5 className="title custom-service-head">SEO Development<br />
                                                    plan</h5>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="service-tab">
                    <div className="container">
                        <div className="service-tab-nav service-all ">
                            <ul
                                className="nav nav-tabs service-tab-nav justify-content-center"
                                id="myTab"
                                role="tablist"
                            >
                                <li className="nav-item" role="presentation">
                                    <a
                                        data-toggle="tab"
                                        href="/facebook"
                                        role="tab"
                                    >
                                        <img src="/assets/img/icon/facebook.png" alt="likes-sold" />
                                        <h5 className="title custom-service-head">Facebook Development<br /> Plan</h5>
                                    </a>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <a
                                        data-toggle="tab"
                                        href="/tiktok"
                                        role="tab"
                                    >
                                        <img src="/assets/img/icon/tiktok.png" alt="likes-sold" />
                                        <h5 className="title custom-service-head">Tiktok Development<br /> Plan</h5>
                                    </a>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <a
                                        data-toggle="tab"
                                        href="/instagram"
                                        role="tab"
                                    >
                                        <img src="/assets/img/icon/instagram.png" alt="likes-sold" />
                                        <h5 className="title custom-service-head">Instagram Development<br /> Plan</h5>
                                    </a>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <a
                                        data-toggle="tab"
                                        href="/twitter"
                                        role="tab"
                                    >
                                        <img src="/assets/img/icon/twitter.png" alt="likes-sold" />
                                        <h5 className="title custom-service-head">Twitter Development<br /> Plan</h5>
                                    </a>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <a
                                        data-toggle="tab"
                                        href="/youtube"
                                        role="tab"
                                    >
                                        <img src="/assets/img/icon/youtube.png" alt="likes-sold" />
                                        <h5 className="title custom-service-head">Youtube Development<br /> Plan</h5>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            {/*====== start usercase======*/}
            <UseCaseOfficial />


            {/*====== START  Success stories Section======*/}
            <section className="brands-section bg-soft-grey-color p-t-80 p-b-80" style={{ "background": "#F6F6F6" }}>
                <div className="container">
                    <div className="main-top-heading text-center m-b-40">
                        {/* <h2>Say
                            <span className="animated-word">
                            </span> to Zingsters
                        </h2> */}
                        <h2 className="heading-new-layout">Success Stories</h2>
                    </div>
                </div>
                <div className="container-fluid">
                    <Slider
                        {...brandslidertwoactive}
                        className="brand-items slider_gap brand-effect-one row brand-slider-two-active"
                    >
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/1.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>20%</p>
                                <h5>YouTube Engagement</h5>
                                <h6>Henry Mitchell</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/2.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>5k+</p>
                                <h5>Instagram Followers</h5>
                                <h6>Mathew Brice</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/3.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>12%</p>
                                <h5>Monthly Growth</h5>
                                <h6>BlueSky Technologies</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/4.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>240%</p>
                                <h5>Users</h5>
                                <h6>Abigail Forster</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/5.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>10k+</p>
                                <h5>Facebook Likes</h5>
                                <h6>Liam Anderson</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/6.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>15%</p>
                                <h5>Story Views</h5>
                                <h6>Amelia Cooper</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/7.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>40k+</p>
                                <h5>YouTube Views</h5>
                                <h6>CraftyChronicles</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/8.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>2.4X</p>
                                <h5>Instagram Comments</h5>
                                <h6>Oliver BRooks</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/9.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>3X</p>
                                <h5>Previous Growth</h5>
                                <h6>Evelyn Reed</h6>
                            </div>
                        </div>
                        <div className="col d-flex success-stories">
                            <div className="brand-item">
                                <a href="#">
                                    <img src="/assets/img/testimonial/10.png" alt="Brand" />
                                </a>
                            </div>
                            <div className="brand-stories">
                                <p>30%</p>
                                <h5>New Leads</h5>
                                <h6>Apex Strategies</h6>
                            </div>
                        </div>
                    </Slider>
                </div>
            </section>

            {/*====== START  form Section======*/}
            <section className="about-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-lg-between justify-content-center">
                        <div className="col-xl-6 col-lg-6 col-md-10">
                            <div className="main-top-heading tagline-boxed m-b-30">
                                <h2 className="heading-new-layout ">Your growth journey today</h2>
                                <p className="m-t-10">This box on the right takes all your information, passes it through internet fairies which fly at a speed of light so that our in-house growth scientist starts brewing the growth serum personalized for you.</p>
                            </div>


                        </div>
                        <div className="col-xl-5 col-lg-6 col-md-10">
                            <div className="about-text">
                                <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-40">
                                    <div className="sign-in-up-wrapper form-sign-all">
                                        <form action="#">
                                            <div className="form-groups">
                                                <div className="field-group">
                                                    <div className="icon">
                                                        <i className="far fa-user"></i>
                                                    </div>
                                                    <input type="text" placeholder="Name" />
                                                </div>
                                                <div className="field-group">
                                                    <div className="icon">
                                                        <i className="far fa-envelope"></i>
                                                    </div>
                                                    <input type="email" placeholder="Email" />
                                                </div>
                                                <div className="field-group" style={{ "textAlign": "left" }}>


                                                    <a className="btn-blue">
                                                        GET STARTED
                                                    </a>

                                                </div>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            {/*====== START accordion  ======*/}


            <section className="fag-section p-t-50 p-b-50" style={{ "background": "#F6F6F6" }}>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-10">
                            <div className=" main-top-heading tagline-boxed m-b-30">
                                <h2 className="heading-new-layout text-center">Frequently Asked Questions</h2>
                            </div>
                            <Tab.Container defaultActiveKey="general">
                                <div className="accordion-tab">

                                    <Tab.Content>
                                        <Tab.Pane eventKey="general">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="speakers">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="pricing">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="support">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="history">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="customers">
                                            <div className="landio-accordion-v2">
                                                <AccordionV27 />
                                            </div>
                                        </Tab.Pane>
                                    </Tab.Content>
                                </div>
                            </Tab.Container>

                            <div className="field-group" style={{ "textAlign": "left", "marginTop": "20px" }}>

                                <Link href="/">
                                    <a className="btn-blue">
                                        Sign me up NOW!
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </Layouts >
    );
};

export default Index;
