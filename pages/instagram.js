import Link from "next/link";
import { useState } from "react";
import Layouts from "../src/layouts/Layouts";

const Instagram = () => {
    const [video, setVideo] = useState(false);


    return (
        <Layouts pageName="Instagram">

            <section className="pricing-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                                <h2 className="title">
                                    All SERVICES {" "}
                                    <span>
                                        INSTAGRAM {" "}
                                        {/* <img src="/assets/img/particle/title-line.png" alt="Line" /> */}
                                    </span>{" "}

                                </h2>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

                            </div>

                        </div>
                    </div>

                    {/* <!-- Pricing Table --> */}
                    <div className="row justify-content-center m-b-50">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 " style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">FOLLOWERS</h4>
                                    <p className="plan-feature m-t-10">Profile Followers</p>
                                    <a href="/buy-instagram-followers" className="template-btn hero-btns  justify-content-center">
                                        Buy Followers
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-5" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">LIKES</h4>
                                    <p className="plan-feature m-t-10"> Post Likes</p>
                                    <a href="/buy-instagram-likes" className="template-btn hero-btns  justify-content-center">
                                        Buy Likes
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-4" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">VIEWS</h4>
                                    <p className="plan-feature m-t-10">Video Views</p>
                                    <a href="buy-instagram-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Views
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center m-b-50">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 " style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">STORY VIEWS</h4>
                                    <p className="plan-feature m-t-10">Profile Story Views</p>
                                    <a href="/buy-instagram-story-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Story Views
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-5" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">COMMENTS</h4>
                                    <p className="plan-feature m-t-10"> Post Comments</p>
                                    <a href="/buy-instagram-comments" className="template-btn hero-btns  justify-content-center">
                                        Buy Comments
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-4" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">IMPRESSIONS</h4>
                                    <p className="plan-feature m-t-10">Video Impressions</p>
                                    <a href="buy-instagram-impressions" className="template-btn hero-btns  justify-content-center">
                                        Buy Impressions
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center m-b-50">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 " style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">STORY PACKAGES</h4>
                                    <p className="plan-feature m-t-10">Profile Packages</p>
                                    <a href="/buy-instagram-packages" className="template-btn hero-btns  justify-content-center">
                                        Buy Packages
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-5" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">POWER LIKES</h4>
                                    <p className="plan-feature m-t-10"> Post Power Likes</p>
                                    <a href="/buy-instagram-power-likes" className="template-btn hero-btns  justify-content-center">
                                        Buy Power Likes
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table  pricing-secondary-4" style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">LIVE VIDEO VIEWS</h4>
                                    <p className="plan-feature m-t-10">Video Live Video Views</p>
                                    <a href="buy-instagram-live-video-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Live Video Views
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center m-b-50">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 " style={{
                                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
                            }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h4 className="title">STORY SHARES</h4>
                                    <p className="plan-feature m-t-10">Profile Shares</p>
                                    <a href="/buy-instagram-shares" className="template-btn hero-btns  justify-content-center">
                                        Buy Shares
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">

                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">

                        </div>
                    </div>

                </div>
            </section>



        </Layouts >
    );
};

export default Instagram;
