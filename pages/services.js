import Link from "next/link";
import { useState } from "react";
import Layouts from "../src/layouts/Layouts";

const Services = () => {
  const [video, setVideo] = useState(false);


  return (
    <Layouts pageName="Services">


      <section className="pricing-section p-t-50 p-b-50">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                <h2 className="title">
                  All SERVICES {" "}
                  <span>
                    INSTAGRAM {" "}
                    {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                  </span>{" "}

                </h2>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

              </div>

            </div>
          </div>

          {/* <!-- Pricing Table --> */}
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4 " style={{
                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
              }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/12.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">FOLLOWERS</h4>
                  <p className="plan-feature m-t-10">Profile Followers</p>
                  <a href="/buy-instagram-followers" className="template-btn hero-btns  justify-content-center">
                    Buy Followers
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table  pricing-secondary-5" style={{
                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
              }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/12.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">LIKES</h4>
                  <p className="plan-feature m-t-10"> Post Likes</p>
                  <a href="/buy-instagram-likes" className="template-btn hero-btns  justify-content-center">
                    Buy Likes
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table  pricing-secondary-4" style={{
                boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)"
              }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/12.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">VIEWS</h4>
                  <p className="plan-feature m-t-10">Video Views</p>
                  <a href="buy-instagram-views" className="template-btn hero-btns  justify-content-center">
                    Buy Views
                  </a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section className="pricing-section p-t-50 p-b-50">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                <h2 className="title">
                  All SERVICES {" "}
                  <span>
                    YOUTUBE {" "}
                    {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                  </span>{" "}

                </h2>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

              </div>

            </div>
          </div>

          {/* <!-- Pricing Table --> */}
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4 " style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/11.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">Subscribers</h4>
                  <p className="plan-feature m-t-10">Channel Subscribers</p>
                  <a href="/buy-youtube-subscribers" className="template-btn hero-btns  justify-content-center">
                    Buy Subscribers
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/11.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">LIKES</h4>
                  <p className="plan-feature m-t-10"> Post Likes</p>
                  <a href="/buy-youtube-likes" className="template-btn hero-btns  justify-content-center">
                    Buy Likes
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/11.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">VIEWS</h4>
                  <p className="plan-feature m-t-10">Video Views</p>
                  <a href="buy-youtube-views" className="template-btn hero-btns  justify-content-center">
                    Buy Views
                  </a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section className="pricing-section p-t-50 p-b-50">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                <h2 className="title">
                  All SERVICES {" "}
                  <span>
                    FACEBOOK {" "}
                    {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                  </span>{" "}

                </h2>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

              </div>

            </div>
          </div>

          {/* <!-- Pricing Table --> */}
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4 " style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/8.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">FOLLOWERS</h4>
                  <p className="plan-feature m-t-10">Profile Followers</p>
                  <a href="/buy-facebook-followers" className="template-btn hero-btns  justify-content-center">
                    Buy Followers
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/8.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">LIKES</h4>
                  <p className="plan-feature m-t-10"> Post Likes</p>
                  <a href="/buy-facebook-likes" className="template-btn hero-btns  justify-content-center">
                    Buy Likes
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/8.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">VIEWS</h4>
                  <p className="plan-feature m-t-10">Video Views</p>
                  <a href="buy-facebook-views" className="template-btn hero-btns  justify-content-center">
                    Buy Views
                  </a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section className="pricing-section p-t-50 p-b-50">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                <h2 className="title">
                  All SERVICES {" "}
                  <span>
                    TIKTOK {" "}
                    {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                  </span>{" "}

                </h2>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">

              </div>

            </div>
          </div>

          {/* <!-- Pricing Table --> */}
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4 " style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/10.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">FOLLOWERS</h4>
                  <p className="plan-feature m-t-10">Profile Followers</p>
                  <a href="/buy-tiktok-followers" className="template-btn hero-btns  justify-content-center">
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/10.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">LIKES</h4>
                  <p className="plan-feature m-t-10"> Post Likes</p>
                  <a href="/buy-tiktok-likes" className="template-btn hero-btns  justify-content-center">
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-4" style={{ boxShadow: "0px 10px 60px 0px rgb(215 212 255 / 70%)" }}>
                <div className="plan-title-area">
                  <img
                    src="/assets/img/icon/10.webp?png"
                    alt="Plan icon"
                    className="plan-icon"
                  />

                </div>
                <div className="plan-cost text-center">
                  <h4 className="title">VIEWS</h4>
                  <p className="plan-feature m-t-10">Video Views</p>
                  <a href="buy-tiktok-views" className="template-btn hero-btns  justify-content-center">
                    Buy Now
                  </a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>





      {/*====== End Scroll To Top ======*/}

    </Layouts >
  );
};

export default Services;
