import Link from "next/link";
import Layouts from "../src/layouts/Layouts";
import { heroSlider, heroSliderService } from "../src/sliderProps";
import Slider from "react-slick";
import AccordionV5 from "../src/components/Accordion/AccordionV5";
import { Tab } from "react-bootstrap";
import FbWork from "../src/layouts/fbWork";
import StaticSection from "../src/layouts/StaticSection";
import SliderStaticSection from "../src/layouts/SliderStaticSection";
import { testimonialActiveOne } from "../src/sliderProps";

const Review = () => {
    
    return (
        <Layouts pageName="review">

            <section className="testimonials-section bg-secondary-color-7 section-dots-pattern">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="common-heading heading-white  text-center p-t-50">
                                <span className="tagline color-primary-4">
                                    Excellent on Trustpilot
                                </span>
                                <h2 className="title ">
                                    Trustpilot  Review
                                </h2>
                            </div>
                        </div>
                    </div>
                    <Slider
                        {...testimonialActiveOne}
                        className="row testimonial-boxes-v1 testimonial-dots-3"
                        id="testimonialActiveOne"
                    >
                        <div className="col">
                            <div className="testimonial-box m-t-30">
                                <div className="author-rating">
                                    <ul className="rating-review">
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                    </ul>
                                    <div className="author-brand">
                                        <img
                                            src="/assets/img/testimonial/author-brand-1.png"
                                            alt="Brand"
                                        />
                                    </div>
                                </div>
                                <p className="author-comments">
                                    Sed perspicia unde omnis natuscis error sit volupt accusa
                                    doloremquey laudantium totam rem aperiam ses eaque quae
                                    denouncing
                                </p>
                                <div className="author-info">
                                    <img
                                        src="/assets/img/testimonial/author-small-1.png"
                                        alt="testimonial author"
                                    />
                                    <h6 className="name">
                                        Michael W. Kirwan{" "}
                                        <span className="title">CEO &amp; founder</span>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="testimonial-box m-t-30">
                                <div className="author-rating">
                                    <ul className="rating-review">
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                    </ul>
                                    <div className="author-brand">
                                        <img
                                            src="/assets/img/testimonial/author-brand-2.png"
                                            alt="Brand"
                                        />
                                    </div>
                                </div>
                                <p className="author-comments">
                                    At vero eos et accusamus et iustoy dignissimos ducimus quies
                                    blanditiis praesentium voluptatum.
                                </p>
                                <div className="author-info">
                                    <img
                                        src="/assets/img/testimonial/author-small-2.png"
                                        alt="testimonial author"
                                    />
                                    <h6 className="name">
                                        Eugene S. Smith{" "}
                                        <span className="title">Senior Manager</span>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="testimonial-box m-t-30">
                                <div className="author-rating">
                                    <ul className="rating-review">
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                    </ul>
                                    <div className="author-brand">
                                        <img
                                            src="/assets/img/testimonial/author-brand-4.png"
                                            alt="Brand"
                                        />
                                    </div>
                                </div>
                                <p className="author-comments">
                                    Nam libero tempore cum sole nobis eligendi cumque impedite
                                </p>
                                <div className="author-info">
                                    <img
                                        src="/assets/img/testimonial/author-small-4.png"
                                        alt="testimonial author"
                                    />
                                    <h6 className="name">
                                        Ronald A. Kawakami{" "}
                                        <span className="title">Web Developer</span>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="testimonial-box m-t-30">
                                <div className="author-rating">
                                    <ul className="rating-review">
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                        <li>
                                            <i className="fa fa-star" />
                                        </li>
                                    </ul>
                                    <div className="author-brand">
                                        <img
                                            src="/assets/img/testimonial/author-brand-6.png"
                                            alt="Brand"
                                        />
                                    </div>
                                </div>
                                <p className="author-comments">
                                    Quis autem vel eumr eprehenderit quinea voluptate.
                                </p>
                                <div className="author-info">
                                    <img
                                        src="/assets/img/testimonial/author-small-6.png"
                                        alt="testimonial author"
                                    />
                                    <h6 className="name">
                                        Ronald A. Kawakami{" "}
                                        <span className="title">Web Developer</span>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </Slider>
                </div>
            </section>


        </Layouts >
    );
};

export default Review;
