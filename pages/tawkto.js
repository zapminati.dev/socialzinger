import Script from "next/script";
import Header from "../src/layouts/headers/HeaderTawk";
import Layouts from "../src/layouts/Layouts";

export default function Tawkto() {
     return (
          <>
               {/* <!--Start of Tawk.to Script--> */}
               
               {/* <div id='tawk_64788859ad80445890f06ded'></div> */}

               {/* <!--End of Tawk.to Script--> */}

               <Layouts noHeader>
            <Header />
            <section className="data-analysis-section p-t-55 p-b-125">
                <div className="container p-t-100">
                    <div className="row align-items-top justify-content-center">

                        <div className="col-xl-7 col-lg-6 col-md-10 col-sm-10 ">
                            <div className="sign-in-up-wrapper">
                            <div id='tawk_64788859ad80445890f06ded' className="d-flex justify-content-center"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layouts>

          </>
     );
}
