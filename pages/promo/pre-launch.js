import Link from "next/link";
import Slider from "react-slick";
import Offers from "../../src/components/Offers";
import Header3 from "../../src/layouts/headers/Header3";
import Layouts from "../../src/layouts/Layouts";
import { createRef, useRef, useState } from "react";
import validator from "validator";
import axios from "axios";
import Router from "next/router";
import ReCAPTCHA from "react-google-recaptcha";

<div
     className="Component-Bg"
     style={{
          backgroundImage: `url("/assets/img/particle/particle-1.webp?png" )`,
          backgroundSize: "cover",
          backgroundPosition: "center center",
          backgroundRepeat: "no-repeat",
     }}
></div>;

const PreLaunch = () => {
     //state for form data
     const [formData, setFormData] = useState({
          name: "",
          email: "",
          service: "",
          // serviceType: "",
          link: "",
     });
     const [error, setError] = useState({
          name: false,
          email: false,
          link: false,
          recaptcha: false,
     });
     const [errorMessage, setErrorMessage] = useState("");
     const [loading, setLoading] = useState(false);
     const [reCaptcha, setReCaptcha] = useState("")

     const formSectionRef = useRef(null);
     const captchaRef = useRef(null)

     const handleScroll = (ref) => {
          window.scrollTo({
               top: ref.offsetTop,
               behavior: "smooth",
          });
     };

     const handleFormData = (input) => (e) => {
          const { value } = e.target;

          setFormData((prevState) => ({
               ...prevState,
               [input]: value,
          }));
     };

     const saveFormData = async (userData) => {
          try {
               const { data } = await axios.post(
                    "/api/promo/freemium/email",
                    {
                         name: userData.name,
                         email: userData.email,
                         service: userData.service,
                         // serviceType: userData.serviceType,
                         link: userData.link,
                    },
                    {
                         headers: {
                              "Content-Type": "application/json",
                         },
                    }
               );
               return data;
          } catch (error) {
               return error.response.data;
          }
     };

     const createOmnisend = async (userData) => {

          try {
               const options = {
                    headers: {
                         accept: "application/json",
                         "content-type": "application/json",
                         "X-API-KEY":
                              "64141989b7d5173400850e40-smQfHKVWjqmtwyTwmCbBShi5Vlvpg8im8w9JC4phK5gfQry4Gr",
                    },
               };
               return await axios.post(
                    "https://api.omnisend.com/v3/contacts",
                    {
                         identifiers: [
                              {
                                   channels: {
                                        email: {
                                             status: "subscribed",
                                        },
                                   },
                                   id: userData.email,
                                   type: "email",
                                   sendWelcomeMessage: true,
                              },
                         ],
                         tags: ["test contact"],
                         firstName: userData.name,
                         sendWelcomeEmail: true,
                    },
                    options
               );
          } catch (error) {
               console.log(error.message);
          }
     };

     const validateCaptcha = () =>{

          const token = captchaRef.current.getValue();
          
          if(token){
               setReCaptcha(token)
               setErrorMessage("")
               setError((err) => (err === true ? false : false))
          }
          
     }

     const submitFormData = async (e) => {
          e.preventDefault();

          try {
               if (formData.name.length > 30) {
                    setErrorMessage("Name shouldn't more than 30 character.");
                    setError({ name: true });
                    return;
               } else if (
                    formData.email.length > 60 ||
                    !validator.isEmail(formData.email)
               ) {
                    setErrorMessage("Please enter vailid email Id.");
                    setError({ email: true });
                    return;
               }else if(!reCaptcha){
                    setErrorMessage("Please check the Google Captcha.");
                    setError({ recaptcha: true });
                    return;
               } else {
                    setLoading(true);
                    setErrorMessage("");
                    setError((err) => (err === true ? false : false));

                    const data = await saveFormData(formData);

                    if (data.success) {
                         const omniData = await createOmnisend(formData);

                         if (omniData.status === 200) {
                              // setErrorMessage("Data submitted successfully!");
                              Router.push({
                                   pathname: "/promo/email/thankyou",
                                   query: { email: formData.email, name: formData.name },
                              });
                              setLoading(false);
                         }

                         //setFormData({name: "", email:"", service:"", serviceType:"", link:""})
                    } else {
                         setErrorMessage(data.message);
                         setLoading(false);
                    }
               }
          } catch (error) {
               console.log(error.message);
          }
     };

     return (
          <Layouts noHeader noFooter>
               <Header3 />
               {/* <!--====== Start Section 1 ======--> */}
               <section
                    className="hero-area-v3 landing-page-v3"
                    ref={formSectionRef}
               >
                    <div className="container">
                         <div className="row align-items-center justify-content-lg-between justify-content-center">
                              <div className="col-lg-7 col-md-8 p-l-70">
                                   <div className="hero-content">
                                        <h1
                                             className="hero-title wow fadeInLeft"
                                             data-wow-delay="0.3s"
                                        >
                                             BOOST YOUR SOCIAL
                                             <br /> MEDIA PRESENCE <br />{" "}
                                             <span>WITH SOCIAL ZINGER</span>
                                        </h1>
                                        <p
                                             className="wow fadeInLeft subtile-landing"
                                             data-wow-delay="0.4s"
                                        >
                                             SIGN UP NOW TO CLAIM FREE
                                             <br />
                                             SUBSCRIPTION LIKES, VIEWS & MORE
                                        </p>
                                   </div>
                              </div>
                              <div className="col-lg-5 col-md-8 d-sm-block p-r-30">
                                   <div className="sign-in-up-wrapper landing-form">
                                        <h4 className="form-title landing-login">
                                             Sign Up
                                        </h4>
                                        <form onSubmit={submitFormData}>
                                             {errorMessage ? (
                                                  <p className="bg-info text-dark">
                                                       {errorMessage}
                                                  </p>
                                             ) : null}
                                             <div className="form-groups">
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="far fa-user"></i>
                                                       </div>
                                                       <input
                                                            name="name"
                                                            defaultValue={
                                                                 formData.name
                                                            }
                                                            type="text"
                                                            placeholder="Name"
                                                            onChange={handleFormData(
                                                                 "name"
                                                            )}
                                                            className={
                                                                 error.name
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       />
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="far fa-envelope"></i>
                                                       </div>
                                                       <input
                                                            name="email"
                                                            defaultValue={
                                                                 formData.email
                                                            }
                                                            type="email"
                                                            placeholder="Email"
                                                            onChange={handleFormData(
                                                                 "email"
                                                            )}
                                                            className={
                                                                 error.email
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       />
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="fas fa-info"></i>
                                                       </div>

                                                       <select
                                                            name="service"
                                                            defaultValue={
                                                                 formData.service
                                                            }
                                                            onChange={handleFormData(
                                                                 "service"
                                                            )}
                                                            className={
                                                                 error.service
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       >
                                                            <option value="">
                                                                 Services
                                                            </option>
                                                            <option value="instagram">
                                                                 Instagram -
                                                                 Likes
                                                            </option>
                                                            <option value="tiktok">
                                                                 Tiktok -
                                                                 Followers
                                                            </option>
                                                       </select>
                                                  </div>
                                                  {/* <div className="field-group">
                                                       <div className="icon">
                                                            <i className="fas fa-star"></i>
                                                       </div>

                                                       <select
                                                            name="serviceType"
                                                            defaultValue={
                                                                 formData.serviceType
                                                            }
                                                            onChange={handleFormData(
                                                                 "serviceType"
                                                            )}
                                                            className={
                                                                 error.serviceType
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       >
                                                            <option value="">
                                                                 Service Types
                                                            </option>
                                                            <option value="like">
                                                                 Likes
                                                            </option>
                                                            <option value="views">
                                                                 Views
                                                            </option>
                                                            <option value="comments">
                                                                 Comments
                                                            </option>
                                                            <option value="subscribers">
                                                                 Subscribers
                                                            </option>
                                                       </select>
                                                  </div> */}
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="far fa-link"></i>
                                                       </div>
                                                       <input
                                                            name="link"
                                                            defaultValue={
                                                                 formData.link
                                                            }
                                                            type="url"
                                                            placeholder="Profile Url"
                                                            onChange={handleFormData(
                                                                 "link"
                                                            )}
                                                            className={
                                                                 error.link
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       />
                                                  </div>
                                                  <div className="field-group">
                                                       <ReCAPTCHA
                                                       
                                                            sitekey={
                                                                 process.env.SOCIALZINGER_APP_SITE_KEY
                                                            }
                                                            ref={captchaRef}
                                                            onChange={validateCaptcha}
                                                            className={
                                                                 error.recaptcha
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                       />
                                                  </div>
                                                  <div className="field-group">
                                                       {loading ? (
                                                            <button
                                                                 class="btn btn-primary"
                                                                 type="button"
                                                                 disabled
                                                            >
                                                                 <span
                                                                      class="spinner-border spinner-border-sm"
                                                                      role="status"
                                                                      aria-hidden="true"
                                                                 ></span>
                                                                 <span class="sr-only">
                                                                      Loading...
                                                                 </span>
                                                            </button>
                                                       ) : (
                                                            <button type="submit">
                                                                 REGISTER NOW
                                                            </button>
                                                       )}
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div className="hero-particle">
                         <img
                              src="/assets/img/particle/particle-8.webp?png"
                              alt="#"
                              className="particle-1 animate-rotate-me"
                         />
                         <img
                              src="/assets/img/particle/particle-9.webp?png"
                              alt="#"
                              className="particle-2 animate-zoom-fade"
                         />
                    </div>
               </section>

               <Offers />

               <section className="service-section  p-t-55 p-b-125">
                <div className="container">
                    {/* <!-- Common Heading --> */}
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="common-heading text-center m-b-35">
                                <h2 className="title">
                                    OUR FEATURES
                                </h2>
                                {" "}
                                <span><img src="/assets/img/15.webp?png" alt="Line" /></span>
                            </div>
                        </div>
                    </div>

                    {/* <!-- Icon Box One --> */}
                    <div className="row justify-content-center landing-features fancy-icon-boxes-v2 boxes-white-version">
                        <div className="col-md-6 col-sm-10 wow fadeInUp"
                            data-wow-delay="0.2s"
                        >
                            <div className="fancy-icon-box m-t-30">
                                <div className="box-icon">
                                    <img
                                        src="/assets/img/fancy-icon-box/02.webp?png"
                                        alt="service icon one"
                                    />
                                </div>
                                <div className="box-content text-white">
                                    <h4>

                                        Instant Delivery

                                    </h4>
                                    <p>
                                        With our fast and reliable delivery options, you can have your social media needs delivered to you within hours or even minutes.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-10 wow fadeInUp"
                            data-wow-delay="0.3s"
                        >
                            <div className="fancy-icon-box m-t-30">
                                <div className="box-icon">
                                    <img
                                        src="/assets/img/fancy-icon-box/03.webp?png?png"
                                        alt="service icon one"
                                    />
                                </div>
                                <div className="box-content">
                                    <h4>

                                        Trust

                                    </h4>
                                    <p>We strive to earn and maintain the trust of our customers every day. We provide our customers with that are genuine and authentic services.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-10 wow fadeInUp"
                            data-wow-delay="0.4s"
                        >
                            <div className="fancy-icon-box m-t-30">
                                <div className="box-icon">
                                    <img
                                        src="/assets/img/fancy-icon-box/04.webp?png"
                                        alt="service icon one"
                                    />
                                </div>
                                <div className="box-content">
                                    <h4>

                                        Customer Support

                                    </h4>
                                    <p>
                                        Our customer support team is highly trained and dedicated to helping our customers with any questions or issues they may have, instantly.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-10 wow fadeInUp"
                            data-wow-delay="0.5s"
                        >
                            <div className="fancy-icon-box m-t-30">
                                <div className="box-icon">
                                    <img
                                        src="/assets/img/fancy-icon-box/05.webp?png"
                                        alt="service icon one"
                                    />
                                </div>
                                <div className="box-content">
                                    <h4>

                                        Privacy and Security

                                    </h4>
                                    <p>
                                        We are committed to protecting the personal information of our customers.  We use the latest encryption technologies in data protection.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

               <section
                    className="cta-section bg-cover-center p-t-50 p-b-50"
                    style={{
                         backgroundImage: "url(/assets/img/cta/cta-bg-2.webp?png)",
                    }}
               >
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-xl-8 col-lg-7 col-md-10">
                                   <div className="cta-content text-center">
                                        <div className="common-heading heading-white m-b-65">
                                             <span className="tagline">
                                                  Still Curious?
                                             </span>
                                             <h2 className="title">
                                                  Why Social Zinger Is Your Best
                                                  SMM Partner
                                             </h2>
                                             <p className="text-white">
                                                  At Social Zinger, we provide a
                                                  boost to your instagram
                                                  accounts by offering real
                                                  instagram followers, like and
                                                  comments instantly delivered
                                                  to you. With competitive rates
                                                  and packages, our high quality
                                                  SMM services are best in the
                                                  industry. Just tell us what
                                                  you need, and we’ll enhance
                                                  your social media presence
                                                  instantly.
                                             </p>
                                        </div>
                                        <button
                                             onClick={() => {
                                                  handleScroll(
                                                       formSectionRef.current
                                                  );
                                             }}
                                             className="template-btn white-bg bordered-btn"
                                        >
                                             Buy Now{" "}
                                             <i className="far fa-arrow-right" />
                                        </button>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div
                         className="cta-absolute-image d-none d-lg-block wow fadeInUp"
                         data-wow-delay="0.3s"
                    ></div>
               </section>
               <section className="template-footer webinar-footer">
                    <div className="container">
                         <div className="copyright-area">
                              <div className="row">
                                   <div className="col-md-7">
                                        <ul className="footer-nav text-md-left text-center m-b-sm-15">
                                             <li>
                                                  <a href="/faq">FAQ</a>
                                             </li>
                                             <li>
                                                  <a href="/privacy-policy">
                                                       Privacy Policy
                                                  </a>
                                             </li>
                                             <li>
                                                  <a href="/terms-and-conditions">
                                                       Conditions
                                                  </a>
                                             </li>
                                        </ul>
                                   </div>
                                   <div className="col-md-5">
                                        <div className="copyright-text text-md-right text-center">
                                             <p className="copyright-text text-center text-sm-right pt-4 pt-sm-0">
                                                  © {new Date().getFullYear()}{" "}
                                                  <a href="/">SocialZinger</a>.
                                                  All Rights Reserved
                                             </p>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>
          </Layouts>
     );
};

export default PreLaunch;
