import React, { useEffect, useState } from "react";
import Router, { useRouter } from "next/router";
import axios from "axios";
import PreLoader from "../../../src/layouts/PreLoader";
import Link from "next/link";
import Slider from "react-slick";
import Header3 from "../../../src/layouts/headers/Header3";
import Layouts from "../../../src/layouts/Layouts";

const Thankyou = () => {
     const router = useRouter();
     //console.log(router.query.email)
     const [isEmailSent, setIsEmailSent] = useState(false);

     const sendEmailVerification = async (email, name) => {
          try {
               if (!email) {
                    Router.push({
                         pathname: "/promo/pre-launch",
                    });
               }
               const { data } = await axios.post(
                    `/api/promo/freemium/email/send?email=${email}&name=${name}`
               );
               
               if (data.success) {
                    setIsEmailSent(true);
               }
          } catch (error) {
               console.log(error.response.data);
               alert(error.response.data.message);
               Router.push({
                    pathname: "/promo/pre-launch",
               });
          }
     };

     useEffect(async () => {
          if (router.isReady) {
               await sendEmailVerification(router.query.email, router.query.name);
          }
     }, [router.isReady]);

     return (
          <>
               <Layouts noHeader noFooter>
                    <Header3 />
                    {isEmailSent ? (
                         <section className="service-section bg-soft-grey-color p-t-130 p-b-130">
                              <div className="container">
                                   <div className="row justify-content-center">
                                        <div className="col-lg-7 col-md-10 fancy-icon-boxes-v1">
                                             <div className="common-heading thank-you-box text-center fancy-icon-box1  m-b-50">
                                                  <img
                                                       src="/assets/img/fancy-icon-box/07.webp?png"
                                                       alt="service icon one"
                                                  />
                                                  <h4 className="title m-t-20">
                                                       Thank you for filling out
                                                       the form
                                                  </h4>
                                                  <h5>
                                                       Please verify your email
                                                       address. 
                                                  </h5>
                                                  <h5 className="text-success">Please check out your inbox or spam folder also.</h5>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </section>
                    ) : (
                         <PreLoader />
                    )}
                    {/*====== Thank you Section End ======*/}
                    <section className="template-footer webinar-footer">
                         <div className="container">
                              <div className="copyright-area">
                                   <div className="row">
                                        <div className="col-md-7">
                                             <ul className="footer-nav text-md-left text-center m-b-sm-15">
                                                  <li>
                                                       <a href="/faq">FAQ</a>
                                                  </li>
                                                  <li>
                                                       <a href="/privacy-policy">
                                                            Privacy Policy
                                                       </a>
                                                  </li>
                                                  <li>
                                                       <a href="/terms-and-conditions">
                                                            Conditions
                                                       </a>
                                                  </li>
                                             </ul>
                                        </div>
                                        <div className="col-md-5">
                                             <div className="copyright-text text-md-right text-center">
                                                  <p className="copyright-text text-center text-sm-right pt-4 pt-sm-0">
                                                       ©{" "}
                                                       {new Date().getFullYear()}{" "}
                                                       <a href="/">
                                                            SocialZinger
                                                       </a>
                                                       . All Rights Reserved
                                                  </p>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
               </Layouts>
          </>
     );
};

export default Thankyou;
