import React, { useEffect, useState } from "react";
import Router, { useRouter } from "next/router";
import axios from "axios";
import PreLoader from "../../../src/layouts/PreLoader";
import Layouts from "../../../src/layouts/Layouts";
import Header3 from "../../../src/layouts/headers/Header3";
import Head from "next/head";

const VerifyEmail = () => {
     const router = useRouter();
     console.log(router.isReady ? router.query.token : "");
     const [isTokenVerified, setIsTokenVerified] = useState(false);

     const verifyEmailToken = async (token) => {
          try {
               if (!token) {
                    return Router.push({
                         pathname: "/promo/pre-launch",
                    });
               }
               const { data } = await axios.post(
                    `/api/promo/freemium/email/verify`,
                    {
                         token,
                    }
               );
               console.log(data);
               if (data.success) {
                    setIsTokenVerified(true);
               }
          } catch (error) {
               console.log(error.response.data);
               alert(error.response.data.message);
               Router.push({
                    pathname: "/promo/pre-launch",
               });
          }
     };

     useEffect(async () => {
          if (router.isReady) {
               await verifyEmailToken(router.query.token);
          }
     }, [router.isReady]);

     return (
          <>
               <Layouts noHeader noFooter>
                    <Header3 />
                    {isTokenVerified ? (
                         <section className="service-section bg-soft-grey-color p-t-50 p-b-50">
                         <div className="container">
                              <div className="row justify-content-center">
                                   <div className="col-lg-7 col-md-10 fancy-icon-boxes-v1">
                                        <div className="common-heading thank-you-box text-center fancy-icon-box1  m-b-50">
                                             <img
                                                  src="/assets/img/fancy-icon-box/congratulation-07.webp?png"
                                                  alt="service icon one"
                                             />
                                             <h4 className="title m-t-20">
                                             Your order is being processed!
                                             </h4>
                                             <p>You're only one step away from unlocking your free likes/ followers. Please leave us a review to claim your prize.</p>
                                             <a
                                                  className="template-btn m-t-30"
                                                  target="_blank"
                                                  nofollow
                                                  href="https://www.trustpilot.com/review/socialzinger.com"
                                             >
                                                  Review Us{" "}
                                                  <i class="fas fa-arrow-right"></i>
                                             </a>
                                             <p>We will take 24-48 hours to process your order.</p>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
                    ) : (
                         <PreLoader />
                    )}
                    {/*====== Thank you Section End ======*/}
                    <section className="template-footer webinar-footer">
                         <div className="container">
                              <div className="copyright-area">
                                   <div className="row">
                                        <div className="col-md-7">
                                             <ul className="footer-nav text-md-left text-center m-b-sm-15">
                                                  <li>
                                                       <a href="/faq">FAQ</a>
                                                  </li>
                                                  <li>
                                                       <a href="/privacy-policy">
                                                            Privacy Policy
                                                       </a>
                                                  </li>
                                                  <li>
                                                       <a href="/terms-and-conditions">
                                                            Conditions
                                                       </a>
                                                  </li>
                                             </ul>
                                        </div>
                                        <div className="col-md-5">
                                             <div className="copyright-text text-md-right text-center">
                                                  <p className="copyright-text text-center text-sm-right pt-4 pt-sm-0">
                                                       ©{" "}
                                                       {new Date().getFullYear()}{" "}
                                                       <a href="/">
                                                            SocialZinger
                                                       </a>
                                                       . All Rights Reserved
                                                  </p>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
               </Layouts>
          </>
     );
};

export default VerifyEmail;
