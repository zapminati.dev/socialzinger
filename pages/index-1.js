import React, { useState } from "react";
import Link from "next/link";
import Slider from "react-slick";
import Layouts from "../src/layouts/Layouts";
import AccordionV27 from "../src/components/Accordion/AccordionV27";
import { Tab } from "react-bootstrap";
import { brandslidertwoactive } from "../src/sliderTestimonial";
import TestimonialMain from "../src/components/Testimonial/testimonial";
import BlogCard from "../src/components/Blog/BlogCard";
import { getHomePostList } from "../src/lib/posts";
import UseCase from "../src/components/Testimonial/useCase";

export async function getStaticProps() {
     const allPosts = await getHomePostList();

     return {
          props: {
               allPosts: allPosts,
          },
          revalidate: 60,
     };
}

const Home2 = ({ allPosts }) => {
     const [posts, setPosts] = useState(allPosts);

     return (
          <Layouts pageName="homepage">
               {/* <!--====== Start Section 1 ======--> */}
               <section className="data-analysis-section p-t-55 p-b-5">
                    <div className="container">
                         <div className="row align-items-top justify-content-center">
                              <div className="col-lg-6 col-md-10">
                                   <div className="analysis-text-block">
                                        <div className="common-heading tagline-boxed m-b-30">
                                             <span className="tagline-red">
                                                  Social media marketing agency
                                             </span>
                                             <h2 className="heading">
                                                  Ready- Set- Double Tap
                                             </h2>
                                        </div>
                                        <ul className="check-list m-t-30 m-b-30">
                                             <li>
                                                  WE GIVE YOU A GUY ON CHAIR 🔥{" "}
                                             </li>
                                             <li>
                                                  FAST, FAAASST, SUPER FAST 🚀
                                             </li>
                                             <li>
                                             JOIN THE TRIBE OF OVER 15K+ ZINGERS 🧑‍🤝‍
                                             </li>
                                        </ul>
                                        {/* <p>Fuel Your Growth with our Expertise! Gain followers, drive engagement, and dominate the digital landscape. Let us be your social media game-changer. Elevate your brand and soar to new heights!</p> */}
                                        <ul className="hero-btns d-flex justify-content-left">
                                             <li
                                                  className="wow fadeInUp"
                                                  data-wow-delay="0.4s"
                                             >
                                                  <Link href="/">
                                                       <a className="btn-blue">
                                                            LET’S MULTIPLY
                                                       </a>
                                                  </Link>
                                             </li>
                                             <li
                                                  className="wow fadeInUp trustpilotimg"
                                                  data-wow-delay="0.4s"
                                             >
                                                  <img
                                                       src="assets/img/icon/trustpilot.png"
                                                       style={{ width: "54%" }}
                                                  />
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                              <div className="col-lg-6 col-md-8 d-sm-block d-none">
                                   {/* <!-- Preview Gallery One --> */}
                                   <div className="preview-galley-v4 m-r-70 m-b-md-80">
                                        <img
                                             src="assets/img/Banner/bannerGif.gif"
                                             className="preview-image-1"
                                             alt="Live collaboration"
                                        />
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>

               <section
                    className="service-area m-t-40 p-b-20"
                    style={{ background: "#F7F7F7" }}
               >
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-lg-6">
                                   <div className="common-heading text-center p-t-10">
                                        <span className="tagline-red">
                                             Featured on
                                        </span>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <div className="container">
                         <div className="">
                              <div className="row align-items-center">
                                   <div className="col-md-4 col-sm-5 col-4">
                                        <div className="icon-box justify-content-center">
                                             <div className="box-icon">
                                                  <img
                                                       src="assets/img/logo/1.png"
                                                       alt="followers-sold"
                                                  />
                                             </div>
                                        </div>
                                   </div>
                                   <div className="col-md-4 col-sm-5 col-4">
                                        <div className="icon-box justify-content-center">
                                             <div className="box-icon">
                                                  <img
                                                       src="assets/img/logo/2.png"
                                                       alt="likes-sold"
                                                  />
                                             </div>
                                        </div>
                                   </div>
                                   <div className="col-md-4 col-sm-5 col-4">
                                        <div className="icon-box justify-content-center">
                                             <div className="box-icon">
                                                  <img
                                                       src="assets/img/logo/3.png"
                                                       alt="likes-sold"
                                                  />
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>
               {/*======  Multi Platform Service======*/}

               <UseCase />

               <section className="service-area p-t-35">
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-lg-6">
                                   <div className="main-top-heading text-center m-b-65">
                                        <span className="tagline-red">
                                             Reach Where your Audience Is
                                        </span>
                                        <h2 className="title">
                                             Multi Platform Service
                                        </h2>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div className="service-tab">
                         <div className="container">
                              <div className="service-tab-nav service-all">
                                   <ul
                                        className="nav nav-tabs service-tab-nav justify-content-center"
                                        id="myTab"
                                        role="tablist"
                                   >
                                        <li
                                             className="nav-item"
                                             role="presentation"
                                        >
                                             <a
                                                  data-toggle="tab"
                                                  href="/facebook"
                                                  role="tab"
                                             >
                                                  <img
                                                       src="assets/img/service-logo/facebook.svg"
                                                       alt="likes-sold"
                                                  />
                                                  <h5 className="title m-t-20">
                                                       Facebook Growth Plan
                                                  </h5>
                                             </a>
                                        </li>
                                        <li
                                             className="nav-item"
                                             role="presentation"
                                        >
                                             <a
                                                  data-toggle="tab"
                                                  href="/tiktok"
                                                  role="tab"
                                             >
                                                  <img
                                                       src="assets/img/service-logo/tiktok.svg"
                                                       alt="likes-sold"
                                                  />
                                                  <h5 className="title m-t-15">
                                                       Tiktok Growth Plan
                                                  </h5>
                                             </a>
                                        </li>
                                        <li
                                             className="nav-item"
                                             role="presentation"
                                        >
                                             <a
                                                  data-toggle="tab"
                                                  href="/instagram"
                                                  role="tab"
                                             >
                                                  <img
                                                       src="assets/img/service-logo/insta.svg"
                                                       alt="likes-sold"
                                                  />
                                                  <h5 className="title m-t-20">
                                                       Instagram Growth Plan
                                                  </h5>
                                             </a>
                                        </li>
                                        <li
                                             className="nav-item"
                                             role="presentation"
                                        >
                                             <a
                                                  data-toggle="tab"
                                                  href="/twitter"
                                                  role="tab"
                                             >
                                                  <img
                                                       src="assets/img/service-logo/twitter.svg"
                                                       alt="likes-sold"
                                                  />
                                                  <h5 className="title m-t-20">
                                                       Twitter Growth Plan
                                                  </h5>
                                             </a>
                                        </li>
                                        <li
                                             className="nav-item"
                                             role="presentation"
                                        >
                                             <a
                                                  data-toggle="tab"
                                                  href="/youtube"
                                                  role="tab"
                                             >
                                                  <img
                                                       src="assets/img/service-logo/youtube.svg"
                                                       alt="likes-sold"
                                                  />
                                                  <h5 className="title m-t-25">
                                                       Youtube Growth Plan
                                                  </h5>
                                             </a>
                                        </li>
                                   </ul>
                              </div>
                         </div>
                    </div>
               </section>
               {/*====== End  Multi Platform Service======*/}

               <section
                    className="brands-section bg-soft-grey-color p-t-80 p-b-80"
                    style={{ background: "#F6F6F6" }}
               >
                    <div className="container">
                         <div className="main-top-heading text-center m-b-40">
                              <span className="tagline-red">
                                   Growth Delivered
                              </span>
                              <h2>
                                   Say
                                   <span class="animated-word"></span> to
                                   Zingsters
                              </h2>
                         </div>
                    </div>
                    <div className="container-fluid">
                         <Slider
                              {...brandslidertwoactive}
                              className="brand-items slider_gap brand-effect-one row brand-slider-two-active"
                         >
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/1.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>20%</p>
                                        <h5>YouTube Engagement</h5>
                                        <h6>Henry Mitchell</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/2.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>5k+</p>
                                        <h5>Instagram Followers</h5>
                                        <h6>Mathew Brice</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/3.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>12%</p>
                                        <h5>Monthly Growth</h5>
                                        <h6>BlueSky Technologies</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/4.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>240%</p>
                                        <h5>Users</h5>
                                        <h6>Abigail Forster</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/5.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>10k+</p>
                                        <h5>Facebook Likes</h5>
                                        <h6>Liam Anderson</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/6.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>15%</p>
                                        <h5>Story Views</h5>
                                        <h6>Amelia Cooper</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/7.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>40k+</p>
                                        <h5>YouTube Views</h5>
                                        <h6>CraftyChronicles</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/8.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>2.4X</p>
                                        <h5>Instagram Comments</h5>
                                        <h6>Oliver BRooks</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/9.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>3X</p>
                                        <h5>Previous Growth</h5>
                                        <h6>Evelyn Reed</h6>
                                   </div>
                              </div>
                              <div className="col d-flex success-stories">
                                   <div className="brand-item">
                                        <a href="#">
                                             <img
                                                  src="assets/img/testimonial/10.png"
                                                  alt="Brand"
                                             />
                                        </a>
                                   </div>
                                   <div className="brand-stories">
                                        <p>30%</p>
                                        <h5>New Leads</h5>
                                        <h6>Apex Strategies</h6>
                                   </div>
                              </div>
                         </Slider>
                    </div>
               </section>
               {/*======   Your growth journey today======*/}
               <section className="about-section p-t-50 p-b-50">
                    <div className="container">
                         <div className="row justify-content-lg-between justify-content-center">
                              <div className="col-xl-6 col-lg-6 col-md-10">
                                   <div className="main-top-heading tagline-boxed m-b-30">
                                        <span className="tagline-red">
                                             Social media marketing agency
                                        </span>
                                        <h2 className="heading ">
                                             Let’s Talk Growth!
                                        </h2>
                                        <p className="m-t-10">
                                             This box on the right takes all
                                             your information, passes it through
                                             internet fairies which fly at a
                                             speed of light so that our in-house
                                             growth scientist starts brewing the
                                             growth serum personalized for you.
                                        </p>
                                   </div>
                                   {/* <div
                                        className="field-group"
                                        style={{ textAlign: "left" }}
                                   >
                                        <Link href="/">
                                             <a className="btn-blue">
                                                  READY TO GROW📈
                                             </a>
                                        </Link>
                                   </div> */}
                              </div>
                              <div className="col-xl-5 col-lg-6 col-md-10">
                                   <div className="about-text">
                                        <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-40">
                                             <div className="sign-in-up-wrapper form-sign-all">
                                                  <form action="#">
                                                       <div className="form-groups">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-address-card"></i>
                                                                 </div>
                                                                 <input
                                                                      type="text"
                                                                      placeholder="Your Website Address"
                                                                 />
                                                            </div>
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-user"></i>
                                                                 </div>
                                                                 <input
                                                                      type="text"
                                                                      placeholder="Name"
                                                                 />
                                                            </div>
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-browser"></i>
                                                                 </div>
                                                                 <input
                                                                      type="text"
                                                                      placeholder="Business Name"
                                                                 />
                                                            </div>
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-envelope"></i>
                                                                 </div>
                                                                 <input
                                                                      type="email"
                                                                      placeholder="Email"
                                                                 />
                                                            </div>
                                                            <div
                                                                 className="field-group"
                                                                 style={{
                                                                      textAlign:
                                                                           "left",
                                                                 }}
                                                            >
                                                                 <button type="button" className="btn-blue">
                                                                      READY TO GROW📈
                                                                 </button>
                                                            </div>
                                                       </div>
                                                  </form>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>

               {/*======  End Your growth journey today======*/}
               <TestimonialMain bgColor="#F6F6F6" />

               <section className="service-area d-none p-t-35">
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-lg-6">
                                   <div className=" main-top-heading text-center m-b-65">
                                        <span className="tagline-red">
                                             Easy Signup
                                        </span>
                                        <h3 className="title">
                                             Fast & Measurable Results
                                        </h3>
                                        <ul className=" d-flex justify-content-center  m-t-20">
                                             <li
                                                  className="wow fadeInUp"
                                                  data-wow-delay="0.4s"
                                             >
                                                  <Link href="/">
                                                       <a className="btn-blue">
                                                            Get Started
                                                       </a>
                                                  </Link>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div className="service-tab">
                         <div className="container"></div>
                    </div>
               </section>

               {/*====== Start Latest News ======*/}
               <section className="service-area p-t-50">
                    <div className="latest-news-section">
                         <div className="container">
                              <div className="row align-items-center justify-content-between">
                                   <div className="col-lg-5">
                                        <div className="main-top-heading tagline-boxed">
                                             <span className="tagline-red">
                                                  Social Zinger Academy
                                             </span>
                                             <h2 className="heading ">
                                                  Knowledge Hub
                                             </h2>
                                        </div>
                                   </div>
                              </div>
                              <div className="row justify-content-center latest-news-v1">
                                   {posts?.nodes?.map((post) => (
                                        <BlogCard
                                             key={post?.slug}
                                             post={post}
                                        />
                                   ))}
                              </div>

                              <div className="row justify-content-center">
                                   <div className="col-lg-6">
                                        <div className=" main-top-heading text-center m-b-65">
                                             <ul className=" d-flex justify-content-center  m-t-20">
                                                  <li
                                                       className="wow fadeInUp"
                                                       data-wow-delay="0.4s"
                                                  >
                                                       <Link href="/blog">
                                                            <a className="template-btn2">
                                                                 Read more
                                                            </a>
                                                       </Link>
                                                  </li>
                                             </ul>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>

               {/*====== End TestimonialMain  ======*/}

               <section
                    className="fag-section p-t-50 p-b-50"
                    style={{ background: "#F6F6F6" }}
               >
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-lg-10">
                                   <div className=" main-top-heading tagline-boxed m-b-30">
                                        <span className="tagline-red">
                                             Ask Away!
                                        </span>
                                        <h3 className="heading">
                                             Frequently Asked Questions
                                        </h3>
                                   </div>
                                   <Tab.Container defaultActiveKey="general">
                                        <div className="accordion-tab">
                                             <Tab.Content>
                                                  <Tab.Pane eventKey="general">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                                  <Tab.Pane eventKey="speakers">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                                  <Tab.Pane eventKey="pricing">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                                  <Tab.Pane eventKey="support">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                                  <Tab.Pane eventKey="history">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                                  <Tab.Pane eventKey="customers">
                                                       <div className="landio-accordion-v2">
                                                            <AccordionV27 />
                                                       </div>
                                                  </Tab.Pane>
                                             </Tab.Content>
                                        </div>
                                   </Tab.Container>

                                   {/* <div
                                        className="field-group"
                                        style={{
                                             textAlign: "left",
                                             marginTop: "20px",
                                        }}
                                   >
                                        <Link href="/">
                                             <a className="btn-blue">
                                                  Sign me up NOW!
                                             </a>
                                        </Link>
                                   </div> */}
                              </div>
                         </div>
                    </div>
               </section>
          </Layouts>
     );
};

export default Home2;
