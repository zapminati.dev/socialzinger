import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { STATUSES } from "../../utils/status";


export const serviceSlice = createSlice({
    name: "services",
    initialState: {
        data: [],
        category: [],
        status: STATUSES.IDLE,
    },
    reducers:{
        // setCategory(state, action) {
        //     state.category = state.data?.map((catg)=>catg.category )
        // },
        // setStatus(state, action) {
        //     state.status = action.payload
        // },
    },
    extraReducers: (builder)=>{
        builder
        .addCase(fetchServices.pending, (state)=>{
            state.status = STATUSES.LOADING 
        })
        .addCase(fetchServices.fulfilled, (state, action)=>{
            state.data = action.payload
            state.status = STATUSES.IDLE
            const catgData  = state.data.map((catg)=>catg.category)
            state.category = catgData.filter((item, index) => catgData.indexOf(item) === index);
        })
        .addCase(fetchServices.rejected, (state)=>{
            state.status = STATUSES.ERROR
        })
        
    }
})


export const {setServices, setStatus} = serviceSlice.actions

export default  serviceSlice.reducer


//Thunks
export const fetchServices = createAsyncThunk('services/fetch', async()=>{
    const res = await fetch('/api/smm/servicelist')
    const {data} = await res.json()

    return data;

})

// export function fetchServices(inputCategory) {
//     return async function fetchServiceThunk(dispatch, getState) {
//         dispatch(setStatus(STATUSES.LOADING))
//          try {

//             const res = await fetch('http://localhost:3001/api/smm/servicelist')
//             const allService = await res.json()
//             const data = allService.data.filter((catg)=> catg.category === inputCategory)

//             dispatch(setServices(data))
//             dispatch(setStatus(STATUSES.IDLE))

//          } catch (err) {

//             console.log(err);
//             dispatch(setStatus(STATUSES.ERROR ))

//          }
//     }
// }