import { configureStore } from "@reduxjs/toolkit";
import servicesReducer from "./smm/serviceSlice"
import packageReducer from "./plans/packageSlice"

export const store = configureStore({
    reducer: {
        services: servicesReducer,
        package: packageReducer,
    }
})
