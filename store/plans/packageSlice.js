import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { STATUSES } from "../../utils/status";

let packagePlan = ""
let paymentAttempt = 0

if(typeof window !== 'undefined') {
    packagePlan = localStorage.getItem('package') !== null ? JSON.parse(window.localStorage.getItem('package')) : []
    paymentAttempt = localStorage.getItem('paymentAttempt') !== null ? window.localStorage.getItem('paymentAttempt') : 0
}

export const serviceSlice = createSlice({
    name: "services",
    initialState: {
        data: packagePlan,
        paymentAttempt: paymentAttempt,
        status: STATUSES.IDLE,
    },
    reducers:{
        setPackage(state, action){
            state.data = action.payload
            window.localStorage.setItem("package", JSON.stringify(action.payload))
        },
        resetPackage(state, action){
            state.data = []
            state.paymentAttempt = 0
            window.localStorage.removeItem("package")
            window.localStorage.removeItem("paymentAttempt")
        },
        setPaymentAttempt(state, action){
            state.paymentAttempt = action.payload
            window.localStorage.setItem("paymentAttempt", action.payload)
        },
        setStatus(state, action){
            state.status = action.payload
        }
    }
})


export const {setPackage, resetPackage, setPaymentAttempt, setStatus} = serviceSlice.actions

export default  serviceSlice.reducer


//Thunks
// export const fetchServices = createAsyncThunk('services/fetch', async()=>{
//     const res = await fetch('/api/smm/servicelist')
//     const {data} = await res.json()

//     return data;

// })

// export function fetchServices(inputCategory) {
//     return async function fetchServiceThunk(dispatch, getState) {
//         dispatch(setStatus(STATUSES.LOADING))
//          try {

//             const res = await fetch('http://localhost:3001/api/smm/servicelist')
//             const allService = await res.json()
//             const data = allService.data.filter((catg)=> catg.category === inputCategory)

//             dispatch(setServices(data))
//             dispatch(setStatus(STATUSES.IDLE))

//          } catch (err) {

//             console.log(err);
//             dispatch(setStatus(STATUSES.ERROR ))

//          }
//     }
// }