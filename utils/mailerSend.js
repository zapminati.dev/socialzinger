import { MailerSend, EmailParams, Sender, Recipient } from "mailersend";

export default async function sendEmail (options){
    const mailerSend = new MailerSend({
        apiKey: process.env.MAILERSEND_VERIFICATION_AUTH_TOKEN,
      });
      
      const sentFrom = new Sender("welcome@socialzinger.com", "Social zinger");
      
      const recipients = [
        new Recipient(options.email, options.name)
      ];
      
      const emailParams = new EmailParams()
        .setFrom(sentFrom)
        .setTo(recipients)
        .setReplyTo(sentFrom)
        .setSubject(options.subject)
        .setHtml(options.message)
        .setText("This is the text content");
      
      const res = await mailerSend.email.send(emailParams);
      return res
}