import mongoose from "mongoose";

const DB = process.env.MONGODB_URI;
console.log(DB);

const connectDatabase = () => {
     mongoose.set("strictQuery", false);
     mongoose
          .connect(DB, {
               useNewUrlParser: true,
               useUnifiedTopology: true,
          })
          .then((data) => {
               console.log(`MongoDB connected with server: ${data.connection.host}`);
          })
          .catch((err) => {
               console.log(`No connection! ${err}`);
          });
};

module.exports = connectDatabase;
