import Link from "next/link";
import { useState } from "react";
import { Blog, Instagram, Tiktok, Twitter, Youtube, Facebook, Demos, Pages1st, Pages2nd, Portfolio, Services } from "./Menus";
const MobileMenu = ({ show, close }) => {
  const [activeMenu, setActiveMenu] = useState("");
  const activeMenuSet = (value) =>
    setActiveMenu(activeMenu === value ? "" : value),
    activeLi = (value) =>
      value === activeMenu ? { display: "block" } : { display: "none" };
  return (
    <div className={`mobile-slide-panel ${show ? "panel-on" : ""}`}>
      <div className="panel-overlay" onClick={() => close()}></div>
      <div className="panel-inner">
        <div className="mobile-logo">
          <Link href="/">
            <a>
              <img src="/assets/img/logo/dark-logo.png" alt="Landio" />
            </a>
          </Link>
        </div>
        <nav className="mobile-menu">
          <ul>
            <li>
              <Link href="/">
                <a>
                  Home
                  <span
                    className="dd-trigger"
                    onClick={() => activeMenuSet("demo")}
                  >

                  </span>
                </a>
              </Link>
      
            </li>
            <li>
              <Link href="/services">
                <a>
                  Services
                  <span
                    className="dd-trigger"
                    onClick={() => activeMenuSet("Services")}
                  >
                    <i className="fas fa-angle-down"></i>
                  </span>
                </a>
              </Link>
              <ul className="sub-menu" style={activeLi("Services")}>
                <li>
                  <Link href="/instagram">
                    <a>
                      Instagram
                      <span className="dd-trigger">
                        <i className="fas fa-angle-down" />
                      </span>
                    </a>
                  </Link>
                  <ul className="sub-menu">
                    <Instagram />
                  </ul>
                </li>
                <li>
                  <Link href="/youtube">
                    <a>
                      Youtube
                      <span className="dd-trigger">
                        <i className="fas fa-angle-down" />
                      </span>
                    </a>
                  </Link>
                  <ul className="sub-menu">
                    <Youtube />
                  </ul>
                </li>
                <li>
                  <Link href="/facebook">
                    <a>
                      Facebook
                      <span className="dd-trigger">
                        <i className="fas fa-angle-down" />
                      </span>
                    </a>
                  </Link>
                  <ul className="sub-menu">
                    <Facebook />
                  </ul>
                </li>
                <li>
                  <Link href="/tiktok">
                    <a>
                      Tiktok
                      <span className="dd-trigger">
                        <i className="fas fa-angle-down" />
                      </span>
                    </a>
                  </Link>
                  <ul className="sub-menu">
                    <Tiktok />
                  </ul>
                </li>
                <li>
                  <Link href="/">
                    <a>
                      Twitter
                      <span className="dd-trigger">
                        <i className="fas fa-angle-down" />
                      </span>
                    </a>
                  </Link>
                  <ul className="sub-menu">
                    <Twitter />
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <Link href="/blog">
                <a>
                  Blog
                  <span
                    className="dd-trigger"
                    onClick={() => activeMenuSet("Blog")}
                  >
                  </span>
                </a>
              </Link>
              <li>
                <Link href="/faq">
                  <a>
                    FAQ

                  </a>
                </Link>

              </li>

            </li>
            <li>
              <Link href="/contact">Contact</Link>
            </li>
          </ul>
        </nav>
        <a href="#" className="panel-close" onClick={() => close()}>
          <i className="fal fa-times"></i>
        </a>
      </div>
    </div>
  );
};

export default MobileMenu;