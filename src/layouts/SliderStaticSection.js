import Link from "next/link";


const SliderStaticSection = () => {
    return (

        <>
            <ul className="hero-btns d-flex justify-content-left ">
                <li className="wow fadeInUp" data-wow-delay="0.4s">
                    {/* <Link href="/service-list"> */}
                        <a className="template-btn">
                            Buy Now <i className="fas fa-arrow-right"></i>
                        </a>
                    {/* </Link> */}
                </li>
                {/* <li className="wow fadeInUp" data-wow-delay="0.4s">
                    <Link href="/services">
                        <a className="template-btn bordered-btn">
                            Login <i className="fas fa-arrow-right"></i>
                        </a>
                    </Link>
                </li> */}
            </ul>

            <div className="row m-t-20">
                <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                        <div className="box-icon box-space-all">
                            <img src="/assets/img/icon/orders.webp?png" alt="testimonial-author" />
                        </div>
                        <div>
                            <h5>909 K+</h5>
                            <span>CUSTOMERS</span>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                        <div className="box-icon box-space-all">
                            <img src="/assets/img/icon/followers-sold.webp?png" alt="followers-sold" />
                        </div>
                        <div>
                            <h5>295
                                M+</h5>
                            <span>FOLLOWERS SOLD</span>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                        <div className="box-icon box-space-all">
                            <img src="/assets/img/icon/likes-sold.webp?png" alt="likes-sold" />
                        </div>
                        <div>
                            <h5>1
                                B+</h5>
                            <span>LIKES SOLD</span>
                        </div>
                    </div>
                </div>
            </div>


        </>

    );
};

export default SliderStaticSection;
