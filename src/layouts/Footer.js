import Link from "next/link";
const Footer = () => {
  return (
    <footer className="template-footer  border-top-primary">
      <div className="container">
        <div className="footer-widgets p-t-80 p-b-30">
          <div className="row">
            {/* <!-- Single Footer Widget --> */}
            <div className="col-lg-3 col-md-6 col-sm-6">

              <div className="widget text-block-widget">
                <img src="/assets/img/logo/white-logo.png" alt="social zinger" className="m-0" />
                <p>
                  Social Zinger is a one stop solution for all your social media needs. Be it your Facebook or Instagram growth, your  TikTok growth or Youtube reach, we take care of it all. Give a boost to your social media and  enhance your fanbase with us!
                </p>
              </div>
            </div>
            {/* <!-- Single Footer Widget --> */}
            {/* <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="d-lg-flex justify-content-center">
                <div className="widget nav-widget">
                  <h5 className="widget-title">Resources</h5>
                  <ul>

                    <li>
                      <Link href="/buy-instagram-likes">
                        <a>Instagram Likes</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/buy-instagram-followers">
                        <a>Instagram Followers</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/buy-instagram-views">
                        <a>Instagram Views</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/buy-tiktok-likes">
                        <a>Tiktok Likes</a>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div> */}
            {/* <!-- Single Footer Widget --> */}
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="d-lg-flex justify-content-center">
                <div className="widget nav-widget">
                  <h5 className="widget-title">Resources</h5>
                  <ul>
                    <li>
                      <Link href="/buy-youtube-likes">
                        <a>Youtube </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/buy-instagram-likes">
                        <a>Instagram</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/buy-youtube-views">
                        <a>Facebook</a>
                      </Link>
                    </li>

                    <li>
                      <Link href="/buy-tiktok-views">
                        <a>Tiktok </a>
                      </Link>
                    </li>


                  </ul>
                </div>
              </div>
            </div>
            {/* <!-- Single Footer Widget --> */}
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="widget pl-xl-5">
                <div className="widget nav-widget">
                  <h5 className="widget-title">Quick Links</h5>
                  <ul>
                    <li>
                      <Link href="/faq">
                        <a>FAQ</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/privacy-policy">
                        <a>Privacy Policy</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/terms-and-conditions">
                        <a>Terms &amp; Conditions</a>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



      <section className="template-footer webinar-footer">
        <div className="container">

          <div className="copyright-area">
            <div className="row">
              <div className="col-md-7">
                <ul className="footer-nav text-md-left text-center m-b-sm-15">

                  <li>
                    <a href="/faq">FAQ</a>
                  </li>
                  <li>
                    <a href="/privacy-policy">Privacy Policy</a>
                  </li>
                  <li>
                    <a href="/terms-and-conditions">Conditions</a>
                  </li>

                </ul>
              </div>
              <div className="col-md-5">
                <div className="copyright-text text-md-right text-center">
                  <p className="copyright-text text-center text-sm-right pt-4 pt-sm-0">
                    © {new Date().getFullYear()} <a href="/">SocialZinger</a>. All Rights
                    Reserved
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </footer>

  );
};

export default Footer;
