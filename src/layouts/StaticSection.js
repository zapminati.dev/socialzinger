import Link from "next/link";

import Slider from "react-slick";
import { testimonialActiveFour } from "../sliderProps";

const YouLikes = () => {
    return (

        <>

            <section className="testimonial-project-section section-author-particle p-t-50 p-b-50" style={{background: "#f3f3fa"}}>
                <div className="container">
                    <div className="row align-items-center justify-content-center">
                        <div className="col-lg-8 col-md-9">
                        
                         <div className="row justify-content-center">
                              {/* <!-- TrustBox widget - Micro Review Count --> */}
                              <div
                                   class="trustpilot-widget"
                                   data-locale="en-US"
                                   data-template-id="5419b6a8b0d04a076446a9ad"
                                   data-businessunit-id="6422724cf20478e0d1248542"
                                   data-style-height="24px"
                                   data-style-width="100%"
                                   data-theme="light"
                                   data-min-review-count="10"
                                   data-style-alignment="center"
                              >
                                   <a
                                        href="https://www.trustpilot.com/review/socialzinger.com"
                                        target="_blank"
                                        rel="noopener"
                                   >
                                        Trustpilot
                                   </a>
                              </div>
                              {/* <!-- End TrustBox widget --> */}
                         </div>
                    
                            <div className="common-heading text-center m-b-50">
                                <span className="tagline color-primary-4">Customer Reviews</span>
                                <h2 className="title">
                                    Checkout What People Say About Us
                                </h2>

                            </div>
                        </div>
                    </div>
                    <Slider
                        {...testimonialActiveFour}
                        className="row testimonial-slider-v2"
                        id="testimonialActiveFour"
                    >
                        <div className="col-12">
                            <div className="testimonial-item">
                                <div className="author-quote">
                                    <img
                                        src="/assets/img/testimonial/author-2.webp?png"
                                        alt="testimonial-author"
                                    />
                                    <i className="flaticon-right-quote" />
                                </div>
                                <div className="testimonial-content">
                                    <p>
                                        I’ve been using social zinger and it has been a game changer for me. I legit got real followers and my account started to grow.
                                        The followers are still there and from real accounts.”
                                    </p>
                                    <h5 className="author-name">
                                        Mia Stallone
                                        <span className="author-position">Web Designer</span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="testimonial-item">
                                <div className="author-quote">
                                    <img
                                        src="/assets/img/testimonial/author-3.webp?png"
                                        alt="testimonial-author"
                                    />
                                    <i className="flaticon-right-quote" />
                                </div>
                                <div className="testimonial-content">
                                    <p>
                                        “My account has consistently been showing up on the explore page as I get likes and comments with social zinger instantly.
                                        My instagram business has changed completely
                                    </p>
                                    <h5 className="author-name">
                                        George Jacob
                                        <span className="author-position">Web Designer</span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="testimonial-item">
                                <div className="author-quote">
                                    <img
                                        src="/assets/img/testimonial/author-4.webp?png"
                                        alt="testimonial-author"
                                    />
                                    <i className="flaticon-right-quote" />
                                </div>
                                <div className="testimonial-content">
                                    <p>
                                        My business has skyrocketed by Social Zinger as it brought a lot of genuine leads on my instagram account.
                                        My statictics has never been so high.
                                    </p>
                                    <h5 className="author-name">
                                        John K
                                        <span className="author-position">Web Designer</span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="testimonial-item">
                                <div className="author-quote">
                                    <img
                                        src="/assets/img/testimonial/author-5.webp?png"
                                        alt="testimonial-author"
                                    />
                                    <i className="flaticon-right-quote" />
                                </div>
                                <div className="testimonial-content">
                                    <p>
                                        Social Zinger is really affordable for me and now I am using their services frequently.
                                    </p>
                                    <h5 className="author-name">
                                        Elba Idris

                                        <span className="author-position">Web Designer</span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </Slider>
                </div>
                <div className="author-particle-image d-none d-lg-block">
                    <img
                        src="/assets/img/testimonial/author-5.webp?png"
                        alt="Image"
                        className="image-1 animate-zoom-fade"
                    />
                    <img
                        src="/assets/img/testimonial/author-6.webp?png"
                        alt="Image"
                        className="image-2 animate-float-bob-x"
                    />
                    <img
                        src="/assets/img/testimonial/author-7.webp?png"
                        alt="Image"
                        className="image-3 animate-zoom-fade"
                    />
                    <img
                        src="/assets/img/testimonial/author-8.webp?png"
                        alt="Image"
                        className="image-4 animate-float-bob-x"
                    />
                </div>
            </section>

            <section
                className="cta-section bg-cover-center p-t-50 p-b-50"
                style={{ backgroundImage: "url(assets/img/cta/cta-bg-2.webp?png)" }}
            >
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-8 col-lg-7 col-md-10">
                            <div className="cta-content text-center">
                                <div className="common-heading heading-white m-b-65">
                                    <span className="tagline">Still Curious?</span>
                                    <h2 className="title">
                                        Why Social Zinger Is Your Best SMM Partner
                                    </h2>
                                    <p className="text-white">At Social Zinger, we provide a boost to your instagram accounts by offering real instagram followers, like and comments instantly delivered to you. With competitive rates and packages, our high quality SMM services are best in the industry.
                                        Just tell us what you need, and we’ll enhance your social media presence instantly.</p>
                                </div>
                                <a href="/services" className="template-btn white-bg bordered-btn">
                                    Buy Now <i className="far fa-arrow-right" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    className="cta-absolute-image d-none d-lg-block wow fadeInUp"
                    data-wow-delay="0.3s"
                >

                </div>
            </section>
            {/*====== End Call To Action ======*/}

        </>

    );
};

export default YouLikes;
