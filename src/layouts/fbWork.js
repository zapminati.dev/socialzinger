import Link from "next/link";

import Slider from "react-slick";
import { testimonialActiveFour } from "../sliderProps";

const FbWork = () => {
    return (

        <>

            <section className="service-section border-bottom-primary-3 p-t-50 p-b-70 " style={{ background: "#f3f3fa" }}>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-6 col-lg-8">
                            <div className="common-heading title-line-bottom text-center m-b-20">

                                <h2 className="title">
                                    Why Should You Go With Us?
                                </h2>

                                <img
                                    src="/assets/img/particle/title-line-2.webp?png"
                                    alt="Image"
                                    className="Line"
                                />
                            </div>
                        </div>
                        <div className="col-xl-12 col-lg-12">
                            <div className="row fancy-icon-boxes-v2">
                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-2 m-t-30" style={{ background: "rgb(232, 222, 255)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="box-content">
                                            <h3 className="title">
                                                Quickest Delivery
                                            </h3>
                                            <p>
                                                We offer the quickest Facebook Likes and Followers available. Your Likes and Followers will be delivered to you within an hour of placing your order.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                                    <div className="fancy-icon-box color-4 m-t-30" style={{ background: "rgb(228, 255, 223)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="box-content">
                                            <h3 className="title">
                                                Our Promise
                                            </h3>
                                            <p>
                                                We strive to leave a lasting impression on our clients. Do let us know if you are dissatisfied with the order's quality or delivery. Any orders that are not filled would be refunded.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                                    <div className="fancy-icon-box color-3 m-t-30" style={{ background: "rgb(223, 242, 255)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                                        <div className="box-content">
                                            <h3 className="title">
                                                365-day Customer Service
                                            </h3>
                                            <p>
                                                Our dedicated customer support staff is available 24*7. If you have any queries about our services with your order, please do not hesitate to contact us.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </>

    );
};

export default FbWork;
