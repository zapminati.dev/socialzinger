import Head from "next/head";
import { Fragment, useEffect, useState } from "react";
import {
     activeNavMenu,
     animation,
     aTagClick,
     index8Body,
     stickyNav,
} from "../utils";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import ScrollTop from "./ScrollTop";
import SEO from "../seo/index";
import seoData from "../seo/seoData.json";

const Layouts = ({ noHeader, noFooter, pageTitle, children, pageName }) => {
     const [metaData, setMetaData] = useState(
          seoData.filter((curElem, i) => curElem.pageName === pageName)
     );
     // console.log(metaData[0]?.schema);

     useEffect(() => {
          animation();
          activeNavMenu();
          index8Body();
          aTagClick();
          window.addEventListener("scroll", stickyNav);
     });
     return (
          <Fragment>
               {pageName ? <SEO
                    title={metaData[0]?.title}
                    description={metaData[0]?.metaDescription}
                    keywords={metaData[0]?.keywords}
                    subject={metaData[0]?.subject}
                    robots={metaData[0]?.robots}
                    object={metaData[0]?.object}
                    resourceType={metaData[0]?.resourceType}
                    schemaContent={
                      metaData[0]?.schema ? metaData[0]?.schema : ""
                    }
               /> : null}

               {!noHeader && <Header />}
               {pageTitle && <Banner pageName={pageTitle} />}
               {children}
               <ScrollTop />
               {!noFooter && <Footer />}
          </Fragment>
     );
};

export default Layouts;
