import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV201 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can You Buy Views on Twitter?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            You can buy Twitter video views and it’s never been easier. The steps are simple.
                        </p>

                        <p>Begin by copying and pasting the URL of your video into the provided field.</p>
                        <p>Next, please enter the video view count you’d like to buy for this video.</p>
                        <p>You’ll see another box pop up, and you may adjust and choose the discounts/pricing at that time. Don’t reload the page; we work in real-time. Remember, buy Twitter views in bulk to save money!
                        </p>
                        <p>Recheck the cost to make sure you’re comfortable with the amount.</p>
                        <p>Now click “buy now.” Then, finish the payment.
                        </p>
                        <p>Congrats! You have seen how easy it is to buy Twitter views.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Do I Increase My Twitter Views?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            We know the answer, it is “Buy Twitter views.” Our service is fast, cost-effective, and legit with active users. We can have your Twitter post views and Twitter video views rising in no time.
                        </p>

                        <p>You can also take steps to make your content popular on your own. Firstly, use the correct hashtags- keep them short! #BuyTwitterViews is much more effective than #BuyViewsOnTwitterWithSocialZinger.
                        </p>
                        <p>Ensure you are posting content relevant to your audience. Talk to your followers and engage with them instead of posting AT them. You may post an open-ended question or a joke.
                        </p>
                        <p>Don’t neglect the importance of calls to action. Tell your followers what you need from them, so they know how to react. You can say things like, “Please Like, Reply, & Retweet this!” </p>
                        <p>Finally, be a human. Brands get more interaction when they act like human beings. Check out some of your favorite brands- chances are, you’ll see humorous tweets and replies from several of them.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How Much Does It Cost to Buy 1000 Twitter Views?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>We charge $5 ( approx) for 1000 views on Twitter.</p>
                        <p>We know not everyone has thousands of dollars to spend promoting their social media accounts. </p>
                        <p>Aside from your need to buy Twitter views, you likely have accounts on different social media platforms such as Facebook, Instagram, and YouTube to promote. Therefore, purchasing Twitter views is cost-effective with Social Zinger.</p>
                        <p>Remember, it pays to buy Twitter views. The more you order, the less you pay as buying in bulk saves money.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Why Social Zinger?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>Our services are legitimate and high-quality. You can buy Twitter views from real Twitter profile holders here.</p>
                        <p>Our brand delivers only genuine Twitter users so that your account garners the appropriate engagement and exposure. Social Zinger does not use bots or scammers; therefore, your account is safe from a ban or a strike.</p>
                        <p>Twitter does not permit bots. Don’t expect to get far using fake profiles to boost your analytics.</p>
                        <p>Our company is the real deal for people who want to buy Twitter views. We have thousands of satisfied clientele thanks to their increased Twitter engagement. </p>
                        <p>Simply purchase the view package from our site that makes sense to you, and let the views come trickling in.</p>
                        <p>Check out our customer feedback to learn more about buying Twitter views. Our customers are happy with our quick service, lifetime guarantee, and the fact that we use real accounts.</p>
                        <p>The affordable rates and great customer care are other reasons so many choose to buy real Twitter video views with us.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        Can I Buy Twitter Likes Too?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            Yes. We know that it is one thing to buy Twitter views but getting Likes on your Tweets is another aspect altogether.
                        </p>
                        <p>Therefore, we’ve established a comprehensive range of services to suit your Twitter needs. </p>
                        <p>We will deliver as many likes as you need. Purchase the number of likes necessary using our website, and we’ll have them going out within the hour. <a href="https://www.socialzinger.com/buy-twitter-likes">Buy Twitter likes</a>  from Social Zinger at cheap prices now!</p>
                        <p>Be sure to check out our other social media marketing services too. We can help you build recognition and impressions on all your social media- TikTok, YouTube, Instagram, Twitter, and others. We offer services such as <a href="https://www.socialzinger.com/buy-twitter-comments">buy Twitter comments</a> , likes, and more.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV201;
