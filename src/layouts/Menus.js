import Link from "next/link";
import { Fragment } from "react";

const Demos = () => (
  <Fragment>
    <li>
      <Link href="/">Homepage Sass</Link>
    </li>

  </Fragment>
),
  Services = () => (
    <Fragment>
      <li>
        <Link href="/services">Our Services</Link>
      </li>

    </Fragment>
  ),
  instagramViews = () => (
    <Fragment>
      <li>
        <Link href="/instagramViews">Instagram Views</Link>
      </li>

    </Fragment>
  ),
  Pages1st = () => (
    <Fragment>
      <li>
        <Link href="/about">About Us</Link>
      </li>


    </Fragment>
  ),
  Instagram = () => (
    <Fragment>
      <li>
        <Link href="/buy-instagram-likes">Instagram Likes</Link>
      </li>
      <li>
        <Link href="/buy-instagram-followers">Instagram Followers</Link>
      </li>
      <li>
        <Link href="/buy-instagram-views">Instagram Views</Link>
      </li>
      <li>
        <Link href="/buy-instagram-story-views">Instagram Story Views</Link>
      </li>
      <li>
        <Link href="/buy-instagram-comments">Instagram Comments</Link>
      </li>
      <li>
        <Link href="/buy-instagram-impressions">Instagram  Impressions</Link>
      </li>
      <li>
        <Link href="/buy-instagram-packages">Instagram  Packages</Link>
      </li>
      <li>
        <Link href="/buy-instagram-power-likes">Instagram  Power Likes</Link>
      </li>
      <li>
        <Link href="/buy-instagram-live-video-views">Instagram Live Video Views</Link>
      </li>
      <li>
        <Link href="/buy-instagram-shares">Instagram Shares</Link>
      </li>
    </Fragment>
  ),
  Youtube = () => (
    <Fragment>
      <li>
        <Link href="/buy-youtube-likes">Youtube Likes</Link>
      </li>
      <li>
        <Link href="/buy-youtube-subscribers">Youtube subscribers</Link>
      </li>
      <li>
        <Link href="/buy-youtube-views">Youtube Views</Link>
      </li>
    </Fragment>
  ),
  Facebook = () => (
    <Fragment>
      <li>
        <Link href="/buy-facebook-likes">Facebook Likes</Link>
      </li>
      <li>
        <Link href="/buy-facebook-followers">Facebook Followers</Link>
      </li>
      <li>
        <Link href="/buy-facebook-views">Facebook Views</Link>
      </li>
    </Fragment>
  ),
  Tiktok = () => (
    <Fragment>
      <li>
        <Link href="/buy-tiktok-likes">Tiktok Likes</Link>
      </li>
      <li>
        <Link href="/buy-tiktok-followers">Tiktok Followers</Link>
      </li>
      <li>
        <Link href="/buy-tiktok-views">Tiktok Views</Link>
      </li>

    </Fragment>
  ),
  Portfolio = () => (
    <Fragment>
      <li>
        <Link href="/portfolio">Portfolio One</Link>
      </li>
      <li>
        <Link href="/portfolio-2">Portfolio Two</Link>
      </li>
      <li>
        <Link href="/portfolio-details">Portfolio Details</Link>
      </li>
    </Fragment>
  ),
  Twitter = () => (
    <Fragment>
      <li>
        <Link href="/buy-twitter-comments">Twitter Comments</Link>
      </li>
      <li>
        <Link href="/buy-twitter-views">Twitter Views</Link>
      </li>
      <li>
        <Link href="/buy-twitter-likes">Twitter Likes</Link>
      </li>
      <li>
        <Link href="/buy-twitter-followers">Twitter Followers</Link>
      </li>
      <li>
        <Link href="/buy-twitter-retweets">Twitter Retweets</Link>
      </li>
      <li>
        <Link href="/buy-twitter-impressions">Twitter Impressions</Link>
      </li>

    </Fragment>
  ),
  Pages2nd = () => (
    <Fragment>
      <li>
        <Link href="/sign-in">Sign In</Link>
      </li>
      <li>
        <Link href="/sign-up">Sign Up</Link>
      </li>
      <li>
        <Link href="/coming-soon">Coming Soon</Link>
      </li>
      <li>
        <Link href="/404">404</Link>
      </li>
    </Fragment>
  ),
  Blog = () => (
    <Fragment>
      <li>
        <Link href="/blog-standard">Blog Standard</Link>
      </li>
      <li>
        <Link href="/blog-details">Blog Details</Link>
      </li>
    </Fragment>
  );

export { Demos, Services, Pages1st, Portfolio, Twitter, Pages2nd, Blog, Instagram, Youtube, Facebook, Tiktok };
