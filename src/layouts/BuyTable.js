import Link from "next/link";

import Slider from "react-slick";

import { heroSlider } from "../sliderProps";
import { heroSliderService } from "../sliderProps";

const a = [
    {
        id: "InstaHome",
        desc: "Need an Instagram story downloader, Facebook video download service, YouTube to Mp3 capability, or Twitter video downloader ? You'll find what you're looking for on Vidloder. Vidloder is the internet's premier site to download your favorite social media videos. It's free!s",
    },
    {
        id: "InstaAudio",
        desc: "Need an Instagram story downloader, Facebook video download service, YouTube to Mp3 capability, or Twitter video downloader ? You'll find what you're looking for on Vidloder. Vidloder is the internet's premier site to download your favorite social media videos. It's free!s",
    },
    {
        id: "InstaVideo",
        desc: "Need an Instagram story downloader, Facebook video download service, YouTube to Mp3 capability, or Twitter video downloader ? You'll find what you're looking for on Vidloder. Vidloder is the internet's premier site to download your favorite social media videos. It's free!s",
    },
    {
        id: "InstaPost",
        desc: "Need an Instagram story downloader, Facebook video download service, YouTube to Mp3 capability, or Twitter video downloader ? You'll find what you're looking for on Vidloder. Vidloder is the internet's premier site to download your favorite social media videos. It's free!s",
    },
]

function AccordionAll2({ pageId }) {

    const x = a.filter((elem) => { if (elem.id == pageId) return elem.desc })

    console.log(x);


    return (

        <>


            <Slider
                className="testimonial-slider-v1 m-t-20 testimonial-boxes-v2 testimonial-arrows-2 d-flex m-b-30"
                id="heroSliderService"
                {...heroSliderService}
            >
                <div className="col custum-row custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "20%" }}>
                                </span><i>Save 20%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>

                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "40%" }}>
                                </span><i>Save 40%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "60%" }}>
                                </span><i>Save 60%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "100%" }}>
                                </span><i>Save 100%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "20%" }}>
                                </span><i>Save 18%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "20%" }}>
                                </span><i>Save 18%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "20%" }}>
                                </span><i>Save 18%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col custum-row">
                    <div className="plan is-active">
                        <div className="plan__inner">
                            <div className="plan__bar">
                                <span style={{ width: "20%" }}>
                                </span><i>Save 18%</i>
                            </div>
                            <div className="plan__title"><strong>100</strong> Followers</div>
                            <div className="plan__price text-sm"><span>$3.63</span>
                                <strong className="text-orange">$2.97</strong>
                            </div>
                        </div>
                    </div>

                </div>
            </Slider>


        </>

    );
};

export default AccordionAll2;
