import Link from "next/link";
import { Fragment, useState } from "react";
import { Facebook, Instagram, Tiktok, Youtube, Twitter } from "../Menus";
import MobileMenu from "../MobileMenu";
import SearchTrigger from "../SearchTrigger";

const Header4 = () => {
  const [trigger, setTrigger] = useState(false);
  const [mobileMenuTrigger, setMobileMenuTrigger] = useState(false);

  return (
    <Fragment>
      <header className="template-header navbar-center sticky-header section-header">
        <div className="container-fluid container-1430">
          <div className="header-inner">
            <div className="header-left">
              <div className="branding-and-language-selection">
                <div className="brand-logo">
                  <Link href="/">
                    <a>
                      <img src="/assets/img/logo/dark-logo.png" alt="logo" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="header-center">

            </div>
            <div className="header-right">
              <nav className="nav-menu d-none d-xl-block">
                <ul>
                  <li>
                    <Link href="/">
                      <a>
                        Home

                      </a>
                    </Link>

                  </li>

                  <li>
                    <Link href="/services">
                      <a>
                        Services
                        <span className="dd-trigger">
                          <i className="fas fa-angle-down" />
                        </span>
                      </a>
                    </Link>
                    <ul className="sub-menu">
                      {/* <Pages1st /> */}
                      <li>
                        <Link href="/instagram">
                          <a>
                            Instagram
                            <span className="dd-trigger">
                              <i className="fas fa-angle-down" />
                            </span>
                          </a>
                        </Link>
                        <ul className="sub-menu">
                          <Instagram />
                        </ul>
                      </li>
                      <li>
                        <Link href="/youtube">
                          <a>
                            Youtube
                            <span className="dd-trigger">
                              <i className="fas fa-angle-down" />
                            </span>
                          </a>
                        </Link>
                        <ul className="sub-menu">
                          <Youtube />
                        </ul>
                      </li>
                      <li>
                        <Link href="/facebook">
                          <a>
                            Facebook
                            <span className="dd-trigger">
                              <i className="fas fa-angle-down" />
                            </span>
                          </a>
                        </Link>
                        <ul className="sub-menu">
                          <Facebook />
                        </ul>
                      </li>
                      <li>
                        <Link href="/tiktok">
                          <a>
                            Tiktok
                            <span className="dd-trigger">
                              <i className="fas fa-angle-down" />
                            </span>
                          </a>
                        </Link>
                        <ul className="sub-menu">
                          <Tiktok />
                        </ul>
                      </li>
                      <li>
                        <Link href="/twitter">
                          <a>
                            Twitter
                            <span className="dd-trigger">
                              <i className="fas fa-angle-down" />
                            </span>
                          </a>
                        </Link>
                        <ul className="sub-menu">
                          <Twitter />
                        </ul>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <Link href="https://blog.socialzinger.com/">
                      <a>
                        Blog
                        <span
                          className="dd-trigger"
                          onClick={() => activeMenuSet("Blog")}
                        >
                        </span>
                      </a>
                    </Link>

                  </li>

                  <li>
                    <Link href="/contact">
                      <a>
                        Contact Us

                      </a>
                    </Link>

                  </li>


                  <Link href="/">
                    <a className="btn-blue">
                      Get Started
                    </a>
                  </Link>
                </ul>
              </nav>
              <ul className="header-extra">
                <li className="d-xl-none">
                  <a
                    href="#"
                    className="navbar-toggler"
                    onClick={() => setMobileMenuTrigger(true)}
                  >
                    <span></span>
                    <span></span>
                    <span></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        {/* <!-- Start Mobile Slide Menu --> */}
        <MobileMenu
          show={mobileMenuTrigger}
          close={() => setMobileMenuTrigger(false)}
        />
        {/* <!-- End Mobile Slide Menu --> */}
      </header>
      <SearchTrigger close={() => setTrigger(false)} trigger={trigger} />
    </Fragment>
  );
};

export default Header4;
