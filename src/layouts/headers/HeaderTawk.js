import Link from "next/link";
import { useState } from "react";
import MobileMenu from "../MobileMenu";
import SearchTrigger from "../SearchTrigger";
import Nav from "./Nav";
const Header5 = () => {
  const [mobileMenuTrigger, setMobileMenuTrigger] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [searchToggle, setSearchToggle] = useState(false);
  return (
    <header className="template-header navbar-left logo-center absolute-header  nav-border-bottom">
      <SearchTrigger
        close={() => setSearchToggle(false)}
        trigger={searchToggle}
      />
      <div className="container-fluid container-1470">
        <div className="header-inner">
          <div className="header-left">
            {/* <Nav /> */}
          </div>
          <div className="header-center">
            <div className="brand-logo">
              <Link href="#">
                <a>
                  <img
                    src="/assets/img/logo/dark-logo.png"
                    alt="logo"
                    className="main-logo"
                  />
                </a>
              </Link>
            </div>
          </div>
          <div className="header-right">
            <ul className="header-extra">
              {/* <li className="d-none d-lg-block">
                <Link href="/services">
                  <a className="template-btn shadow-none">
                    Get Started <i className="fas fa-arrow-right" />
                  </a>
                </Link>
              </li> */}
              <li className="d-none d-sm-block">
                {/* <a
                  href="#"
                  data-toggle="modal"
                  data-target="#search-modal"
                  className="search-btn"
                  onClick={() => setSearchToggle(true)}
                >
                  <i className="fas fa-search" />
                </a> */}
              </li>
              <li className="d-none d-xl-block" onClick={() => setToggle(true)}>
                {/* <a href="#" className="off-canvas-btn">
                  <span />
                  <span />
                  <span />
                </a> */}
              </li>
              <li
                className="d-xl-none"
                onClick={() => setMobileMenuTrigger(true)}
              >
                <a href="#" className="navbar-toggler bg-dark">
                  <span />
                  <span />
                  <span />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* <div className="col-xl-12 ">
        <div className="mb-3"> */}
          {/* <div className="progress">
            <div
              className="progress-bar"
              role="progressbar"
              style={{ width: "30%" }}
              aria-valuenow="80"
              aria-valuemin="0"
              aria-valuemax="100"
            ></div>
          </div> */}
          {/* <hr/>
        </div>
      </div> */}
      {/* Start Mobile Slide Menu */}
      {/* <MobileMenu
        show={mobileMenuTrigger}
        close={() => setMobileMenuTrigger(false)}
      /> */}
      {/* End Mobile Slide Menu */}
      {/* Start Off Canvas */}
      
      {/* End Off Canvas */}
    </header>
  );
};

export default Header5;
