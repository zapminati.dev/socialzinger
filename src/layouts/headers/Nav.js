import Link from "next/link";
import { Blog, Demos, Pages1st, Pages2nd, Portfolio, Services } from "../Menus";
import Router from 'next/router'
const Nav = () => {

  return (
    <nav className="nav-menu d-none d-xl-block">
      <ul>


        <li>
          <a role="button" onClick={() => Router.back()}>
            <span className="dd-trigger">
              <i className="fas fa-solid fa-chevron-left"></i>

            </span>
            Back

          </a>

        </li>

      </ul>

    </nav>
  );
};

export default Nav;
