import Link from "next/link";

import Slider from "react-slick";
import { testimonialActiveFour } from "../sliderProps";

const AllPackages = () => {
    return (

        <>

            <section className="pricing-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">

                                <h2 className="title">
                                    Buy Instagram Followers easily with
                                    Real Instagram {" "}
                                    <span>
                                        Views, Followers, {" "}
                                        {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                                    </span>{" "}
                                    Likes!
                                </h2>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">
                                <span className="custum-heading">All Instagram marketing campaigns are different, and that's why we give you a
                                    wide variety of options to choose from.</span>
                            </div>

                        </div>
                    </div>

                    {/* <!-- Pricing Table --> */}
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-3 " style={{ background: "#C6FFF4" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/11.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">YOUTUBE</h3>
                                    <p className="plan-feature m-t-10">Subscribers</p>
                                    <a href="/buy-youtube-subscribers" className="template-btn hero-btns  justify-content-center">
                                        Buy Subscribers
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table" style={{ background: "#FFC6D1" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">INSTAGRAM</h3>
                                    <p className="plan-feature m-t-10"> Followers</p>
                                    <a href="/buy-instagram-followers" className="template-btn hero-btns  justify-content-center">
                                        Buy Followers
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-1" style={{ background: "#EEFFC6" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">INSTAGRAM</h3>
                                    <p className="plan-feature m-t-10">Likes</p>
                                    <a href="buy-instagram-likes" className="template-btn hero-btns  justify-content-center">
                                        Buy Likes
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-4 m-t-30" style={{ background: "#D7C6FF" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">INSTAGRAM</h3>
                                    <p className="plan-feature m-t-10">Views</p>
                                    <a href="buy-instagram-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Views
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-1 m-t-30" style={{ background: "#FFD7C6" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/10.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">TIKTOK</h3>
                                    <p className="plan-feature m-t-10">Likes</p>
                                    <a href="buy-tiktok-likes" className="template-btn hero-btns  justify-content-center">
                                        Buy Likes
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-8">
                            <div className="pricing-table pricing-secondary-3 m-t-30" style={{ background: "#C6FFD7" }}>
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/11.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />

                                </div>
                                <div className="plan-cost text-center">
                                    <h3 className="title">YOUTUBE</h3>
                                    <p className="plan-feature m-t-10">Views</p>
                                    <a href="buy-youtube-views" className="template-btn hero-btns  justify-content-center">
                                        Buy Views
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </>

    );
};

export default AllPackages;
