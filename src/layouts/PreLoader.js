const PreLoader = () => {
  return (
    <div id="preloader">
      <img
        className=""
        width="30%"
        src="/assets/img/preloader-logo.gif"
        alt="preloader"
      />
    </div>
  );
};

export default PreLoader;
