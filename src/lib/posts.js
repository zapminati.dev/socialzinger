import { getRequest } from "./fetchRequest";
import graphqlRequest from "./graphqlRequest";

export async function getPostList(endCursor = null, taxonomy = null) {
     let condition = `after: "${endCursor}", first: 9, where: {orderby: {field: DATE, order: DESC}}`;

     if (taxonomy) {
          condition = `after: "${endCursor}", first: 6, where: {orderby: {field: DATE, order: DESC}, ${taxonomy.key}: "${taxonomy.value}"}`;
     }

     const query = {
          query: `query getAllPosts {
            posts(${condition}) {
              nodes {
                date
                slug
                title
                excerpt(format: RENDERED)
                featuredImage {
                  node {
                    mediaDetails {
                      file
                      sizes {
                        sourceUrl
                        width
                        height
                      }
                    }
                  }
                }
                categories {
                  nodes {
                    name
                    slug
                  }
                }
              }
              pageInfo {
                endCursor
                hasNextPage
                hasPreviousPage
                startCursor
              }
            }
          }`,
     };

     const resJson = await graphqlRequest(query);

     const allPost = resJson.data.posts;

     return allPost;
}

export async function getHomePostList() {
  let condition = `after: "null", first: 4, where: {orderby: {field: DATE, order: DESC}}`;
  // where: {taxQuery: {taxArray: {taxonomy: CATEGORY, field: SLUG, terms: ["instagram", "youtube", "facebook", "tiktok", "twitter"], includeChildren: false, operator: IN}},

     const query = {
          query: `query getAllPosts {
        posts(${condition}) {
          nodes {
            date
            slug
            title
            excerpt(format: RENDERED)
            featuredImage {
              node {
                mediaDetails {
                  file
                  sizes {
                    sourceUrl
                    width
                    height
                  }
                }
              }
            }
            categories {
              nodes {
                name
                slug
              }
            }
          }
        }
      }`,
     };

     const resJson = await graphqlRequest(query);

     const homePosts = resJson.data.posts;

     return homePosts;
}

export async function getHomeClusterPostList() {
     let condition = `after: "null", first: 3, where: {taxQuery: {taxArray: {taxonomy: CATEGORY, field: SLUG, terms: ["instagram", "youtube", "facebook", "tiktok", "twitter"], includeChildren: false, operator: NOT_IN}}, orderby: {field: DATE, order: DESC}}`;

     const query = {
          query: `query getAllPosts {
        posts(${condition}) {
          nodes {
            date
            slug
            title
            excerpt(format: RENDERED)
            featuredImage {
              node {
                mediaDetails {
                  file
                  sizes {
                    sourceUrl
                    width
                    height
                  }
                }
              }
            }
            categories {
              nodes {
                name
                slug
              }
            }
          }
        }
      }`,
     };

     const resJson = await graphqlRequest(query);

     const clusterPosts = resJson.data.posts;

     return clusterPosts;
}

export async function getClusterPosts(clusterCatgName) {
  let condition = `after: "null", first: 6, where: {orderby: {field: DATE, order: DESC}, categoryName: "${clusterCatgName}"}`;

  const query = {
       query: `query getAllPosts {
     posts(${condition}) {
       nodes {
         date
         slug
         title
         excerpt(format: RENDERED)
         featuredImage {
           node {
             mediaDetails {
               file
               sizes {
                 sourceUrl
                 width
                 height
               }
             }
           }
         }
         categories {
           nodes {
             name
             slug
           }
         }
       }
     }
   }`,
  };

  const resJson = await graphqlRequest(query);

  const clusterPosts = resJson.data.posts;

  return clusterPosts;
}

export async function getClusterBlogPosts(clusterBlogCatgName) {
  let condition = `after: "null", first: 3, where: {orderby: {field: DATE, order: DESC}, categoryName: "${clusterBlogCatgName}"}`;

  const query = {
       query: `query getAllPosts {
     posts(${condition}) {
       nodes {
         date
         slug
         title
         excerpt(format: RENDERED)
         featuredImage {
           node {
             mediaDetails {
               file
               sizes {
                 sourceUrl
                 width
                 height
               }
             }
           }
         }
         categories {
           nodes {
             name
             slug
           }
         }
       }
     }
   }`,
  };

  const resJson = await graphqlRequest(query);

  const clusterPosts = resJson.data.posts;

  return clusterPosts;
}

export async function getSinglePost(slug) {
     const query = {
          query: `query getSinglePost {
      post(id: "${slug}", idType: SLUG) {
        content(format: RENDERED)
        date
        excerpt(format: RENDERED)
        modified
        slug
        title(format: RENDERED)
        featuredImage {
          node {
            mediaDetails {
              sizes {
                sourceUrl
                width
                height
              }
            }
          }
        }
        author {
          node {
            name
          }
        }
        categories {
          nodes {
            name
            slug
          }
        }
      }
    }`,
     };

     const resJson = await graphqlRequest(query);
     const singlePost = resJson.data.post;

     return singlePost;
}

export async function getMetaData(slug) {
     const resJson = await getRequest(slug);

     const metaData = resJson.head;
     return metaData;
}

export async function getPostSlugs() {
     const query = {
          query: `query getPostSlugs {
      posts {
        nodes {
          slug
        }
      }
    }`,
     };

     const resJson = await graphqlRequest(query);
     const slugs = resJson.data.posts.nodes;
     return slugs;
}

export async function getAllCategories() {

  let condition = `where: {orderby: NAME, order: ASC}`;

     const query = {
          query: `query getCategoryList {
      categories(${condition}) {
        nodes {
          name
          slug
          count
        }
      }
    }`,
     };

     const resJson = await graphqlRequest(query);
     const categories = resJson.data.categories.nodes;
     return categories;
}

export async function getCategorySlugs() {
     const query = {
          query: `query getCategorySlugs {
      categories {
        nodes {
          slug
        }
      }
    }`,
     };

     const resJson = await graphqlRequest(query);
     const categories = resJson.data.categories.nodes;
     return categories;
}

export async function getCategoryDetails(categoryName) {
     const query = {
          query: `query getCategoryDetails {
      category(id: "${categoryName}", idType: SLUG) {
        count
        name
        slug
      }
    }`,
     };

     const resJson = await graphqlRequest(query);
     const categoryDetails = resJson.data.category;
     return categoryDetails;
}
