export async function getRequest(query) {
    const url = `https://blog.socialzinger.com/wp-json/rankmath/v1/getHead?url=https://blog.socialzinger.com/${query}`
    const headers = {'Content-Type': 'application/json'}

    const res = await fetch(url, {
        headers,
        method: 'GET',
    })

    const resJson = await res.json()

    return resJson
}