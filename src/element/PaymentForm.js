import Link from "next/link";
import {
     card,
     CardNumberElement,
     CardExpiryElement,
     CardCvcElement,
     useStripe,
     useElements,
     AddressElement,
} from "@stripe/react-stripe-js";
import axios from "axios";
import Router from "next/router";
import { useEffect, useRef, useState } from "react";
// import Preloader from "../../src/layouts/PreLoader"
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";

import {setPaymentAttempt} from "../../store/plans/packageSlice"

export default function PaymentForm() {

     const {data, paymentAttempt} = useSelector((state)=>state.package)
    
     const dispatch = useDispatch()

     const [selectedPackage, setSelectedPackage] = useState("")

     const payBtn = useRef(null);
     const stripe = useStripe();
     const elements = useElements();

     const [orders, setOrders] = useState("");
     const [loader, setLoader] = useState(false);
     const [errorMessage, setErrorMessage] = useState("");

     useEffect(() => {
          setOrders(JSON.parse(localStorage.getItem("orderDetails")));
     }, []);

     const paymentData = {
          amount: Math.round(orders.sPrice * 100),
          //amount: orders.sPrice * 100,
     };


     const submitHandler = async (e) => {
          e.preventDefault();
          setLoader(true)

          if(paymentAttempt === 2) return Router.push("https://www.google.com")

          const { cardHolderName, country, postalCode } = e.target.elements;

          try {
               payBtn.current.disabled = true;

               const config = {
                    headers: {
                         "Content-Type": "application/json",
                    },
               };
               const { data } = await axios.post(
                    "/api/order/get-secret-key",
                    paymentData,
                    config
               );
               const client_secret = data.client_secret;

               if (!stripe || !elements) return;

               const result = await stripe.confirmCardPayment(client_secret, {
                    payment_method: {
                         card: elements.getElement(CardNumberElement),
                         billing_details: {
                              name: cardHolderName.value,
                              email: orders.custEmail,
                              address: {
                                   country: country.value,
                                   postal_code: postalCode.value,
                              },
                         },
                    },
               });

               if (result.error) {
                    dispatch(setPaymentAttempt(paymentAttempt + 1))

                    setErrorMessage(result.error.message)

                    await sendFailEmail(cardHolderName.value, orders.custEmail, orders.custLink, orders.qty, orders.sPrice, orders.serviceName, result.error.message)

                    payBtn.current.disabled = false;                    
               } else {
                    if (result.paymentIntent.status === "succeeded") {
                         //   const orderInfo = {
                         //       id: result.paymentIntent.id,
                         //       status: result.paymentIntent.status
                         //   }

                         //   localStorage.setItem("orderInfo", JSON.stringify(orderInfo))

                         Router.push({
                              pathname: "/order/success",
                              query: {
                                   transaction_id: result.paymentIntent.id,
                                   status: result.paymentIntent.status,
                                   name: cardHolderName.value,
                              },
                         });
                    } else {
                         setLoader(false)
                         alert("There's some issue while processing payment.");
                    }
               }
          } catch (error) {
               setLoader(false)
               payBtn.current.disabled = false;
               console.log(error.message);
          }
     };

     const sendFailEmail = async (custName, email, link, qty, sPrice, serviceName, errMsg) => {
          try {
               await axios.post(`/api/order/fail`, {
                    name: custName,
                    email: email,
                    serviceName: serviceName,
                    url: link,
                    amount: sPrice,
                    quantity: qty,
                    errorMessage: errMsg,
               });
          } catch (error) {
               return error.message;
          }
     };

     return (
          <>
               <form onSubmit={(e) => submitHandler(e)}>
                    <div className="form-groups">
                         <h1 className="title">Checkout</h1>
                         <br/>
                         <span className="text-danger">{errorMessage ? errorMessage : "" }</span>
                         <hr />
                         <div className="field-group">
                              <div className=" text-left p-3 ">
                                   <div className="d-flex align-items-center justify-content-between">
                                        <div className="icon">
                                             <i className="far fa-solid fa-lock"></i>
                                        </div>
                                        <h5 className="ml-5">
                                             Pay with Credit / Debit card
                                        </h5>
                                        <br/>
                                        <div className="icons">
                                             <img
                                                  src="https://i.imgur.com/2ISgYja.png"
                                                  width="30"
                                             />
                                             <img
                                                  src="https://i.imgur.com/W1vtnOV.png"
                                                  width="30"
                                             />
                                             <img
                                                  src="https://i.imgur.com/35tC99g.png"
                                                  width="30"
                                             />
                                             <img
                                                  src="https://i.imgur.com/2ISgYja.png"
                                                  width="30"
                                             />
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <div className="field-group">
                              <div className="icon">
                                   <i className="far fa-user"></i>
                              </div>
                              <input
                                   name="cardHolderName"
                                   type="text"
                                   placeholder="Cardholder Name"
                                   required={true}
                              />
                         </div>
                         <div className="field-group">
                              <div className="icon">
                                   <i className="fas fa-solid fa-file-spreadsheet"></i>
                              </div>
                              {/* <input type="Number" placeholder="Card Number" /> */}
                              <CardNumberElement className="stripeInput" />
                         </div>

                         <div className="row mt-3 mb-3">
                              <div className="col-md-6">
                                   <div className="field-group">
                                        <div className="icon">
                                             <i className="fas fa fa-calendar"></i>
                                        </div>
                                        <CardExpiryElement className="stripeInput" />
                                   </div>
                              </div>
                              <div className="col-md-6">
                                   <div className="field-group">
                                        <div className="icon">
                                             <i className="fa fa-lock"></i>
                                        </div>
                                        <CardCvcElement className="stripeInput" />
                                   </div>
                              </div>
                         </div>
                         <div className="field-group">
                              <hr />
                         </div>
                         <div className="field-group text-left p-t-20">
                              <h4>Billing Address</h4>
                         </div>

                         <div className="row mt-3 mb-3">
                              <div className="col-md-6">
                                   <div className="field-group">
                                        <div className="icon">
                                             <i className="far fa-city"></i>
                                        </div>
                                        <select className="paySelect_state" name="country" required={true}>
                                             <option disabled="" value="">
                                                  Select State
                                             </option>
                                             <option value="AF">
                                                  Afghanistan
                                             </option>
                                             <option value="AX">
                                                  Åland Islands
                                             </option>
                                             <option value="AL">Albania</option>
                                             <option value="DZ">Algeria</option>
                                             <option value="AD">Andorra</option>
                                             <option value="AO">Angola</option>
                                             <option value="AI">
                                                  Anguilla
                                             </option>
                                             <option value="AQ">
                                                  Antarctica
                                             </option>
                                             <option value="AG">
                                                  Antigua &amp; Barbuda
                                             </option>
                                             <option value="AR">
                                                  Argentina
                                             </option>
                                             <option value="AM">Armenia</option>
                                             <option value="AW">Aruba</option>
                                             <option value="AC">
                                                  Ascension Island
                                             </option>
                                             <option value="AU">
                                                  Australia
                                             </option>
                                             <option value="AT">Austria</option>
                                             <option value="AZ">
                                                  Azerbaijan
                                             </option>
                                             <option value="BS">Bahamas</option>
                                             <option value="BH">Bahrain</option>
                                             <option value="BD">
                                                  Bangladesh
                                             </option>
                                             <option value="BB">
                                                  Barbados
                                             </option>
                                             <option value="BY">Belarus</option>
                                             <option value="BE">Belgium</option>
                                             <option value="BZ">Belize</option>
                                             <option value="BJ">Benin</option>
                                             <option value="BM">Bermuda</option>
                                             <option value="BT">Bhutan</option>
                                             <option value="BO">Bolivia</option>
                                             <option value="BA">
                                                  Bosnia &amp; Herzegovina
                                             </option>
                                             <option value="BW">
                                                  Botswana
                                             </option>
                                             <option value="BV">
                                                  Bouvet Island
                                             </option>
                                             <option value="BR">Brazil</option>
                                             <option value="IO">
                                                  British Indian Ocean Territory
                                             </option>
                                             <option value="VG">
                                                  British Virgin Islands
                                             </option>
                                             <option value="BN">Brunei</option>
                                             <option value="BG">
                                                  Bulgaria
                                             </option>
                                             <option value="BF">
                                                  Burkina Faso
                                             </option>
                                             <option value="BI">Burundi</option>
                                             <option value="KH">
                                                  Cambodia
                                             </option>
                                             <option value="CM">
                                                  Cameroon
                                             </option>
                                             <option value="CA">Canada</option>
                                             <option value="CV">
                                                  Cape Verde
                                             </option>
                                             <option value="BQ">
                                                  Caribbean Netherlands
                                             </option>
                                             <option value="KY">
                                                  Cayman Islands
                                             </option>
                                             <option value="CF">
                                                  Central African Republic
                                             </option>
                                             <option value="TD">Chad</option>
                                             <option value="CL">Chile</option>
                                             <option value="CN">China</option>
                                             <option value="CO">
                                                  Colombia
                                             </option>
                                             <option value="KM">Comoros</option>
                                             <option value="CG">
                                                  Congo - Brazzaville
                                             </option>
                                             <option value="CD">
                                                  Congo - Kinshasa
                                             </option>
                                             <option value="CK">
                                                  Cook Islands
                                             </option>
                                             <option value="CR">
                                                  Costa Rica
                                             </option>
                                             <option value="CI">
                                                  Côte d’Ivoire
                                             </option>
                                             <option value="HR">Croatia</option>
                                             <option value="CW">Curaçao</option>
                                             <option value="CY">Cyprus</option>
                                             <option value="CZ">Czechia</option>
                                             <option value="DK">Denmark</option>
                                             <option value="DJ">
                                                  Djibouti
                                             </option>
                                             <option value="DM">
                                                  Dominica
                                             </option>
                                             <option value="DO">
                                                  Dominican Republic
                                             </option>
                                             <option value="EC">Ecuador</option>
                                             <option value="EG">Egypt</option>
                                             <option value="SV">
                                                  El Salvador
                                             </option>
                                             <option value="GQ">
                                                  Equatorial Guinea
                                             </option>
                                             <option value="ER">Eritrea</option>
                                             <option value="EE">Estonia</option>
                                             <option value="SZ">
                                                  Eswatini
                                             </option>
                                             <option value="ET">
                                                  Ethiopia
                                             </option>
                                             <option value="FK">
                                                  Falkland Islands
                                             </option>
                                             <option value="FO">
                                                  Faroe Islands
                                             </option>
                                             <option value="FJ">Fiji</option>
                                             <option value="FI">Finland</option>
                                             <option value="FR">France</option>
                                             <option value="GF">
                                                  French Guiana
                                             </option>
                                             <option value="PF">
                                                  French Polynesia
                                             </option>
                                             <option value="TF">
                                                  French Southern Territories
                                             </option>
                                             <option value="GA">Gabon</option>
                                             <option value="GM">Gambia</option>
                                             <option value="GE">Georgia</option>
                                             <option value="DE">Germany</option>
                                             <option value="GH">Ghana</option>
                                             <option value="GI">
                                                  Gibraltar
                                             </option>
                                             <option value="GR">Greece</option>
                                             <option value="GL">
                                                  Greenland
                                             </option>
                                             <option value="GD">Grenada</option>
                                             <option value="GP">
                                                  Guadeloupe
                                             </option>
                                             <option value="GU">Guam</option>
                                             <option value="GT">
                                                  Guatemala
                                             </option>
                                             <option value="GG">
                                                  Guernsey
                                             </option>
                                             <option value="GN">Guinea</option>
                                             <option value="GW">
                                                  Guinea-Bissau
                                             </option>
                                             <option value="GY">Guyana</option>
                                             <option value="HT">Haiti</option>
                                             <option value="HN">
                                                  Honduras
                                             </option>
                                             <option value="HK">
                                                  Hong Kong SAR China
                                             </option>
                                             <option value="HU">Hungary</option>
                                             <option value="IS">Iceland</option>
                                             <option value="IN">India</option>
                                             <option value="ID">
                                                  Indonesia
                                             </option>
                                             <option value="IQ">Iraq</option>
                                             <option value="IE">Ireland</option>
                                             <option value="IM">
                                                  Isle of Man
                                             </option>
                                             <option value="IL">Israel</option>
                                             <option value="IT">Italy</option>
                                             <option value="JM">Jamaica</option>
                                             <option value="JP">Japan</option>
                                             <option value="JE">Jersey</option>
                                             <option value="JO">Jordan</option>
                                             <option value="KZ">
                                                  Kazakhstan
                                             </option>
                                             <option value="KE">Kenya</option>
                                             <option value="KI">
                                                  Kiribati
                                             </option>
                                             <option value="XK">Kosovo</option>
                                             <option value="KW">Kuwait</option>
                                             <option value="KG">
                                                  Kyrgyzstan
                                             </option>
                                             <option value="LA">Laos</option>
                                             <option value="LV">Latvia</option>
                                             <option value="LB">Lebanon</option>
                                             <option value="LS">Lesotho</option>
                                             <option value="LR">Liberia</option>
                                             <option value="LY">Libya</option>
                                             <option value="LI">
                                                  Liechtenstein
                                             </option>
                                             <option value="LT">
                                                  Lithuania
                                             </option>
                                             <option value="LU">
                                                  Luxembourg
                                             </option>
                                             <option value="MO">
                                                  Macao SAR China
                                             </option>
                                             <option value="MG">
                                                  Madagascar
                                             </option>
                                             <option value="MW">Malawi</option>
                                             <option value="MY">
                                                  Malaysia
                                             </option>
                                             <option value="MV">
                                                  Maldives
                                             </option>
                                             <option value="ML">Mali</option>
                                             <option value="MT">Malta</option>
                                             <option value="MQ">
                                                  Martinique
                                             </option>
                                             <option value="MR">
                                                  Mauritania
                                             </option>
                                             <option value="MU">
                                                  Mauritius
                                             </option>
                                             <option value="YT">Mayotte</option>
                                             <option value="MX">Mexico</option>
                                             <option value="MD">Moldova</option>
                                             <option value="MC">Monaco</option>
                                             <option value="MN">
                                                  Mongolia
                                             </option>
                                             <option value="ME">
                                                  Montenegro
                                             </option>
                                             <option value="MS">
                                                  Montserrat
                                             </option>
                                             <option value="MA">Morocco</option>
                                             <option value="MZ">
                                                  Mozambique
                                             </option>
                                             <option value="MM">
                                                  Myanmar (Burma)
                                             </option>
                                             <option value="NA">Namibia</option>
                                             <option value="NR">Nauru</option>
                                             <option value="NP">Nepal</option>
                                             <option value="NL">
                                                  Netherlands
                                             </option>
                                             <option value="NC">
                                                  New Caledonia
                                             </option>
                                             <option value="NZ">
                                                  New Zealand
                                             </option>
                                             <option value="NI">
                                                  Nicaragua
                                             </option>
                                             <option value="NE">Niger</option>
                                             <option value="NG">Nigeria</option>
                                             <option value="NU">Niue</option>
                                             <option value="MK">
                                                  North Macedonia
                                             </option>
                                             <option value="NO">Norway</option>
                                             <option value="OM">Oman</option>
                                             <option value="PK">
                                                  Pakistan
                                             </option>
                                             <option value="PS">
                                                  Palestinian Territories
                                             </option>
                                             <option value="PA">Panama</option>
                                             <option value="PG">
                                                  Papua New Guinea
                                             </option>
                                             <option value="PY">
                                                  Paraguay
                                             </option>
                                             <option value="PE">Peru</option>
                                             <option value="PH">
                                                  Philippines
                                             </option>
                                             <option value="PN">
                                                  Pitcairn Islands
                                             </option>
                                             <option value="PL">Poland</option>
                                             <option value="PT">
                                                  Portugal
                                             </option>
                                             <option value="PR">
                                                  Puerto Rico
                                             </option>
                                             <option value="QA">Qatar</option>
                                             <option value="RE">Réunion</option>
                                             <option value="RO">Romania</option>
                                             <option value="RU">Russia</option>
                                             <option value="RW">Rwanda</option>
                                             <option value="WS">Samoa</option>
                                             <option value="SM">
                                                  San Marino
                                             </option>
                                             <option value="ST">
                                                  São Tomé &amp; Príncipe
                                             </option>
                                             <option value="SA">
                                                  Saudi Arabia
                                             </option>
                                             <option value="SN">Senegal</option>
                                             <option value="RS">Serbia</option>
                                             <option value="SC">
                                                  Seychelles
                                             </option>
                                             <option value="SL">
                                                  Sierra Leone
                                             </option>
                                             <option value="SG">
                                                  Singapore
                                             </option>
                                             <option value="SX">
                                                  Sint Maarten
                                             </option>
                                             <option value="SK">
                                                  Slovakia
                                             </option>
                                             <option value="SI">
                                                  Slovenia
                                             </option>
                                             <option value="SB">
                                                  Solomon Islands
                                             </option>
                                             <option value="SO">Somalia</option>
                                             <option value="ZA">
                                                  South Africa
                                             </option>
                                             <option value="GS">
                                                  South Georgia &amp; South
                                                  Sandwich Islands
                                             </option>
                                             <option value="KR">
                                                  South Korea
                                             </option>
                                             <option value="SS">
                                                  South Sudan
                                             </option>
                                             <option value="ES">Spain</option>
                                             <option value="LK">
                                                  Sri Lanka
                                             </option>
                                             <option value="BL">
                                                  St. Barthélemy
                                             </option>
                                             <option value="SH">
                                                  St. Helena
                                             </option>
                                             <option value="KN">
                                                  St. Kitts &amp; Nevis
                                             </option>
                                             <option value="LC">
                                                  St. Lucia
                                             </option>
                                             <option value="MF">
                                                  St. Martin
                                             </option>
                                             <option value="PM">
                                                  St. Pierre &amp; Miquelon
                                             </option>
                                             <option value="VC">
                                                  St. Vincent &amp; Grenadines
                                             </option>
                                             <option value="SR">
                                                  Suriname
                                             </option>
                                             <option value="SJ">
                                                  Svalbard &amp; Jan Mayen
                                             </option>
                                             <option value="SE">Sweden</option>
                                             <option value="CH">
                                                  Switzerland
                                             </option>
                                             <option value="TW">Taiwan</option>
                                             <option value="TJ">
                                                  Tajikistan
                                             </option>
                                             <option value="TZ">
                                                  Tanzania
                                             </option>
                                             <option value="TH">
                                                  Thailand
                                             </option>
                                             <option value="TL">
                                                  Timor-Leste
                                             </option>
                                             <option value="TG">Togo</option>
                                             <option value="TK">Tokelau</option>
                                             <option value="TO">Tonga</option>
                                             <option value="TT">
                                                  Trinidad &amp; Tobago
                                             </option>
                                             <option value="TA">
                                                  Tristan da Cunha
                                             </option>
                                             <option value="TN">Tunisia</option>
                                             <option value="TR">Turkey</option>
                                             <option value="TM">
                                                  Turkmenistan
                                             </option>
                                             <option value="TC">
                                                  Turks &amp; Caicos Islands
                                             </option>
                                             <option value="TV">Tuvalu</option>
                                             <option value="UG">Uganda</option>
                                             <option value="UA">Ukraine</option>
                                             <option value="AE">
                                                  United Arab Emirates
                                             </option>
                                             <option value="GB">
                                                  United Kingdom
                                             </option>
                                             <option value="US">
                                                  United States
                                             </option>
                                             <option value="UY">Uruguay</option>
                                             <option value="UZ">
                                                  Uzbekistan
                                             </option>
                                             <option value="VU">Vanuatu</option>
                                             <option value="VA">
                                                  Vatican City
                                             </option>
                                             <option value="VE">
                                                  Venezuela
                                             </option>
                                             <option value="VN">Vietnam</option>
                                             <option value="WF">
                                                  Wallis &amp; Futuna
                                             </option>
                                             <option value="EH">
                                                  Western Sahara
                                             </option>
                                             <option value="YE">Yemen</option>
                                             <option value="ZM">Zambia</option>
                                             <option value="ZW">
                                                  Zimbabwe
                                             </option>
                                        </select>
                                   </div>
                              </div>
                              <div className="col-md-6">
                                   <div className="field-group">
                                        <div className="icon">
                                             <svg
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  width="16"
                                                  height="16"
                                                  fill="currentColor"
                                                  className="bi bi-file-zip"
                                                  viewBox="0 0 16 16"
                                             >
                                                  <path d="M6.5 7.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v.938l.4 1.599a1 1 0 0 1-.416 1.074l-.93.62a1 1 0 0 1-1.109 0l-.93-.62a1 1 0 0 1-.415-1.074l.4-1.599V7.5zm2 0h-1v.938a1 1 0 0 1-.03.243l-.4 1.598.93.62.93-.62-.4-1.598a1 1 0 0 1-.03-.243V7.5z" />
                                                  <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm5.5-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9v1H8v1h1v1H8v1h1v1H7.5V5h-1V4h1V3h-1V2h1V1z" />
                                             </svg>
                                        </div>
                                        <input
                                             name="postalCode"
                                             type="number"
                                             placeholder="Zip code"
                                             required={true}
                                        />
                                   </div>
                              </div>
                         </div>

                         <div className="field-group">
                              {
                                   loader ? (<Spinner animation="border" role="status"/>) :
                                   (<button
                                   type="submit"
                                   value={`Pay $${orders && orders.sPrice}`}
                                   ref={payBtn}
                              >
                                   {" "}
                                   Pay ${orders.sPrice}
                                   </button>)
                              }
                         </div>
                    </div>
                    <div className="form-note">
                         <p className="small">
                              By completing your order, you agree to the{" "}
                              <a href="/terms-and-conditions">
                                   terms of services
                              </a>{" "}
                              and <a href="/privacy-policy">privacy policy</a>.
                         </p>
                    </div>
               </form>
          </>
     );
}
