import Link from "next/link";
import AccordionV1 from "../src/components/AccordionV1";
import Subscribe from "../src/components/Subscribe";
import Layouts from "../src/layouts/Layouts";
import { screenshotslider, heroSlider, testimonialActiveSix, heroSliderService } from "../src/sliderProps";
import { testimonialActiveOne } from "../src/sliderProps";
import Slider from "react-slick";
import Table from "../src/components/Table";


const Services = () => {
  return (
    <Layouts >
      {/* <!--====== Service Section Start ======--> */}

      {/* <!--====== Start Section 1 ======--> */}
      <section className="data-analysis-section p-t-55 p-b-125">
        <div className="container">
          <div className="row align-items-top justify-content-center">
            <div className="col-lg-7 col-md-10">
              <div className="analysis-text-block p-l-50 p-l-md-0">
                <div className="common-heading tagline-boxed m-b-30">
                  <h1 className="title">Buy Instagram Likes
                    with Instant Delivery</h1>
                </div>
                <p>
                  Hundreds of Instagram <span className="color-4">Followers, Likes,</span> and <span className="color-4">Views</span><br />
                  – Real Users, Real Accounts, and Quick Delivery!
                </p>


                <Slider
                  className="testimonial-slider-v1 m-t-20 testimonial-boxes-v2 testimonial-arrows-2 d-flex m-b-30"
                  id="heroSlider"
                  {...heroSlider}
                >
                  <div className="col custum-row custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "20%" }}>
                          </span><i>Save 20%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "40%" }}>
                          </span><i>Save 40%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "60%" }}>
                          </span><i>Save 60%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "100%" }}>
                          </span><i>Save 100%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "100%" }}>
                          </span><i>Save 100%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "20%" }}>
                          </span><i>Save 18%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div className="col custum-row">
                    <div className="plan is-active">
                      <div className="plan__inner">
                        <div className="plan__bar">
                          <span style={{ width: "20%" }}>
                          </span><i>Save 18%</i>
                        </div>
                        <div className="plan__title"><strong>100</strong> Followers</div>
                        <div className="plan__price text-sm"><span>$3.63</span>
                          <strong className="text-orange">$2.97</strong>
                        </div>
                      </div>
                    </div>

                  </div>
                </Slider>






                <ul className="hero-btns d-flex justify-content-left ">
                  <li className="wow fadeInUp" data-wow-delay="0.4s">
                    <Link href="/services">
                      <a className="template-btn">
                        Saas Solutions <i className="fas fa-arrow-right"></i>
                      </a>
                    </Link>
                  </li>
                  <li className="wow fadeInUp" data-wow-delay="0.4s">
                    <Link href="/services">
                      <a className="template-btn bordered-btn">
                        Learn More <i className="fas fa-arrow-right"></i>
                      </a>
                    </Link>
                  </li>
                </ul>

                <div className="row m-t-20">
                  <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                      <div className="icon">
                        <i className="fal fa-chair-office">
                        </i></div>
                      <div>
                        <h5>40</h5>
                        <span>Seat</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                      <div className="icon">
                        <i className="fal fa-clock">
                        </i>
                      </div>
                      <div>
                        <h5>08</h5>
                        <span>Am</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-sm-5 col-6">
                    <div className="icon-box m-t-30">
                      <div className="icon">
                        <i className="fal fa-clock">
                        </i>
                      </div>
                      <div>
                        <h5>08</h5>
                        <span>Am</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-5 col-md-8 d-sm-block d-none">
              {/* <!-- Preview Gallery One --> */}
              <div className="preview-galley-v1 m-b-md-70">
                <img
                  className="preview-image-1 wow fadeInUp"
                  src="/assets/img/preview-gallery/manage-data-1.jpg"
                  alt="data analysis One"
                />
                <img
                  className="preview-image-2 animate-float-bob-x wow fadeInUp"
                  data-wow-delay="0.2s"
                  src="/assets/img/preview-gallery/manage-data-2.jpg"
                  alt="data analysis Two"
                />
                <img
                  className="preview-image-3 wow fadeInUp"
                  data-wow-delay="0.3s"
                  src="/assets/img/preview-gallery/manage-data-3.jpg"
                  alt="data analysis Three"
                />
                <img
                  className="preview-image-4 wow fadeInUp"
                  data-wow-delay="0.4s"
                  src="/assets/img/preview-gallery/manage-data-4.jpg"
                  alt="data analysis Four"
                />
                <img
                  className="preview-image-5 animate-float-bob-y"
                  data-wow-delay="0.5s"
                  src="/assets/img/preview-gallery/manage-data-5.jpg"
                  alt="data analysis Five"
                />
                <img
                  className="preview-image-6 wow fadeInUp"
                  data-wow-delay="0.6s"
                  src="/assets/img/preview-gallery/manage-data-6.jpg"
                  alt="data analysis Six"
                />
              </div>
            </div>

          </div>
        </div>
      </section >

      {/*====== Service Section Start ======*/}

      < section className="service-section border-bottom-primary-3 p-t-10 p-b-50 " style={{ background: "#fff" }}>
        <div className="container">
          <div className="row align-items-top justify-content-center">
            <div className="col-lg-10 col-md-10">
              <div className="analysis-text-block text-center p-l-md-0">
                <div className="common-heading tagline-boxed m-b-10">
                  <h2 className="title ">Buy Instagram Likes with <span className="color-4" > Instant Delivery</span></h2>
                </div>
                <p> At Buzzoid, you can buy Instagram likes quickly, safely and easily with just a few clicks. See our deals below! </p>

                <div className="m-b-50"> <a className="box-link" href="/">What's the difference?</a></div>
              </div>
            </div>
          </div>
          <Slider
            className="testimonial-slider-v1 m-t-20 testimonial-boxes-v2 testimonial-arrows-2 d-flex m-b-30"
            id="heroSliderService"
            {...heroSliderService}
          >
            <div className="col custum-row custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "20%" }}>
                    </span><i>Save 20%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>

                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "40%" }}>
                    </span><i>Save 40%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "60%" }}>
                    </span><i>Save 60%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "100%" }}>
                    </span><i>Save 100%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "20%" }}>
                    </span><i>Save 18%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "20%" }}>
                    </span><i>Save 18%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "20%" }}>
                    </span><i>Save 18%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
            <div className="col custum-row">
              <div className="plan is-active">
                <div className="plan__inner">
                  <div className="plan__bar">
                    <span style={{ width: "20%" }}>
                    </span><i>Save 18%</i>
                  </div>
                  <div className="plan__title"><strong>100</strong> Followers</div>
                  <div className="plan__price text-sm"><span>$3.63</span>
                    <strong className="text-orange">$2.97</strong>
                  </div>
                </div>
              </div>

            </div>
          </Slider>

        </div>
      </section >

      {/*====== Service Section End ======*/}

      {/*====== Service Section Start ======*/}
      <section className="service-section border-bottom-primary-3 p-t-130 p-b-130 " style={{ background: "#f5eeee" }}>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-8">
              <div className="common-heading title-line-bottom text-center m-b-20">
                <span className="tagline">What We Do</span>
                <h2 className="title">
                  Ready to buy Instagram likes?
                </h2>
                <p>Buying likes for your Instagram posts is the best way to reach a wider
                  audience, encourage engagement, and ensure greater success.</p>
                <img
                  src="/assets/img/particle/title-line-2.webp?png"
                  alt="Image"
                  className="Line"
                />
              </div>
            </div>
            <div className="col-xl-12 col-lg-12">
              <div className="row fancy-icon-boxes-v2">
                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                  <div className="fancy-icon-box color-2 m-t-30" style={{ background: "rgb(232, 222, 255)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                    <div className="box-content">
                      <h4 className="title">
                        <a href="#">Instant Delivery Guaranteed</a>
                      </h4>
                      <p>
                        Don't wait to get your likes. Orders typically process within minutes of purchase
                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.3s">
                  <div className="fancy-icon-box color-4 m-t-30" style={{ background: "rgb(228, 255, 223)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                    <div className="box-content">
                      <h4 className="title">
                        <a href="#">100% Real Likes</a>
                      </h4>
                      <p>
                        High-quality likes delivered instantly from real users with real accounts (no bots or fake accounts).

                      </p>
                    </div>
                  </div>
                </div>

                <div className="col-lg-4 wow fadeInUp" data-wow-delay="0.4s">
                  <div className="fancy-icon-box color-3 m-t-30" style={{ background: "rgb(223, 242, 255)", boxShadow: " 0px 10px 20px 0px rgb(5 24 43 / 10%)" }}>

                    <div className="box-content">
                      <h4 className="title">
                        <a href="#">24/7 Customer Support</a>
                      </h4>
                      <p>
                        Twicsy's experienced staff prides itself on providing the best service possible.
                      </p>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== Service Section End ======*/}

      {/* <!--====== Start FAQ section ======--> */}
      {/* <!--====== Start Pricing Section ======--> */}
      <section className="pricing-section p-t-130 p-b-130">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">
                <span className="section__head">Radcred Instagram Packages</span>
                <h2 className="title">
                  Buy Instagram Likes easily with
                  Real Instagram {" "}
                  <span>
                    Views, Followers, {" "}
                    {/* <img src="/assets/img/particle/title-line.webp?png" alt="Line" /> */}
                  </span>{" "}
                  Likes!
                </h2>
              </div>
            </div>
            <div className="col-lg-12">
              <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">
                <span className="custum-heading">All Instagram marketing campaigns are different, and that's why we give you a
                  wide variety of options to choose from.</span>
              </div>

            </div>
          </div>

          {/* <!-- Pricing Table --> */}
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-1 m-t-30" style={{ background: "#C6FFF4" }}>
                <div className="plan-cost">
                  <h4 className="title">Instagram Likes Services</h4>
                </div>
                <span className="plan-type">Our Instagram likes packages instantly make your posts more popular. And a high number of IG likes mushrooms quickly into
                  more and more likes. Choose high-quality or premium likes; both are incredibly.</span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table m-t-30" style={{ background: "#FFC6D1" }}>
                <div className="plan-cost">
                  <h4 className="title">Instagram Likes Services</h4>
                </div>
                <span className="plan-type">And by "cheap," we mean we offer high-quality or premium followers at very affordable prices. Our services are cost-effective for an influencer or small business to build a social media presence quickly and effectively.
                </span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-1 m-t-30" style={{ background: "#EEFFC6" }}>
                <div className="plan-cost">
                  <h5 className="title">Instagram Likes Services</h5>

                </div>
                <span className="plan-type">Instagram video views, Instagram profile views, Instagram story views – fast growth in all of these metrics will bring increased credibility
                  to your account and will "spillover" to provide even more exposure.</span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-1 m-t-30" style={{ background: "#D7C6FF" }}>
                <div className="plan-cost">
                  <h4 className="title">Instagram Likes Services</h4>
                </div>
                <span className="plan-type">Our Instagram likes packages instantly make your posts more popular. And a high number of IG likes mushrooms quickly into
                  more and more likes. Choose high-quality or premium likes; both are incredibly.</span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table m-t-30" style={{ background: "#FFD7C6" }}>
                <div className="plan-cost">
                  <h4 className="title">Instagram Likes Services</h4>
                </div>
                <span className="plan-type">And by "cheap," we mean we offer high-quality or premium followers at very affordable prices. Our services are cost-effective for an influencer or small business to build a social media presence quickly and effectively.
                </span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-8">
              <div className="pricing-table pricing-secondary-1 m-t-30" style={{ background: "#C6FFD7" }}>
                <div className="plan-cost">
                  <h5 className="title">Instagram Likes Services</h5>

                </div>
                <span className="plan-type">Instagram video views, Instagram profile views, Instagram story views – fast growth in all of these metrics will bring increased credibility
                  to your account and will "spillover" to provide even more exposure.</span>
                <a href="#" className="template-btn hero-btns  justify-content-center">
                  Read More<i className="fas fa-solid fa-plus"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!--====== End Faq With SEO score box ======--> */}


      {/*====== Start Testimonials Section ======*/}
      <section className="testimonial-section p-t-50 p-t-md-130 p-b-30">
        <div className="container">
          <div className="common-heading text-center m-b-60">
            <span className="tagline color-primary-6">
              Still not convinced?
            </span>
            <h2 className="title">Here’s what our customers say</h2>
          </div>
          <Slider
            className="testimonial-slider-v1 testimonial-boxes-v2 testimonial-dots-2 row"
            id="testimonialActiveSix"
            {...testimonialActiveSix}
          >
            <div className="col">
              <div className="testimonial-box">
                <ul className="rating">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star-half-alt" />
                  </li>
                </ul>
                <h4 className="title">{`" Unique Design "`}</h4>
                <p>
                  Sed ut perspiciatis omnis natus error sit voluptatem
                  accusantium doloremque laudane totam rem.
                </p>
                <span className="author-name">James R. Kessler</span>
              </div>
            </div>
            <div className="col">
              <div className="testimonial-box">
                <ul className="rating">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star-half-alt" />
                  </li>
                </ul>
                <h4 className="title">{`" Clean Code "`}</h4>
                <p>
                  Sed ut perspiciatis omnis natus error sit voluptatem
                  accusantium doloremque laudane totam rem.
                </p>
                <span className="author-name">Lan K. Villeneuve</span>
              </div>
            </div>
            <div className="col">
              <div className="testimonial-box">
                <ul className="rating">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star-half-alt" />
                  </li>
                </ul>
                <h4 className="title">{`" Easy Editable "`}</h4>
                <p>
                  Sed ut perspiciatis omnis natus error sit voluptatem
                  accusantium doloremque laudane totam rem.
                </p>
                <span className="author-name">Michael J. Schultz</span>
              </div>
            </div>
            <div className="col">
              <div className="testimonial-box">
                <ul className="rating">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star-half-alt" />
                  </li>
                </ul>
                <h4 className="title">{`" Fresh Design "`}</h4>
                <p>
                  Sed ut perspiciatis omnis natus error sit voluptatem
                  accusantium doloremque laudane totam rem.
                </p>
                <span className="author-name">David A. Ames</span>
              </div>
            </div>
            <div className="col">
              <div className="testimonial-box">
                <ul className="rating">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star-half-alt" />
                  </li>
                </ul>
                <h4 className="title">{`" Unique Design "`}</h4>
                <p>
                  Sed ut perspiciatis omnis natus error sit voluptatem
                  accusantium doloremque laudane totam rem.
                </p>
                <span className="author-name">James R. Kessler</span>
              </div>
            </div>
          </Slider>
        </div>
      </section>
      {/*====== End Testimonials Section ======*/}

      {/*====== Start Call To Action ======*/}
      <section
        className="cta-section bg-cover-center p-t-90 p-b-90"
        style={{ backgroundImage: "url(assets/img/cta/cta-bg-2.jpg)" }}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-7 col-md-10">
              <div className="cta-content text-center">
                <div className="common-heading heading-white m-b-65">
                  <span className="tagline">Still Curious?</span>
                  <h2 className="title">
                    Why is SMM the Best Service
                    for Instagram Users?
                  </h2>
                  <p className="text-white">Social media accounts without followers, likes, and views seem like very lonely places – and they're
                    unlikely to attract visitors who will come back again and again, and they may not attract visitors at all.</p>
                </div>
                <a href="#" className="template-btn white-bg bordered-btn">
                  View More <i className="far fa-arrow-right" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="cta-absolute-image d-none d-lg-block wow fadeInUp"
          data-wow-delay="0.3s"
        >

        </div>
      </section>
      {/*====== End Call To Action ======*/}


    </Layouts >
  );
};

export default Services;
