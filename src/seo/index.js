import React, { useEffect, useState } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Script from "next/script";

export default function SEO({
     description,
     author = "Social Zinger",
     meta,
     title,
     keywords,
     robots,
     language = "en-USA",
     owner = "Social Zinger",
     distribution = "GLOBAL",
     country = "USA",
     object,
     email = "welcome@socialzinger.com",
     resourceType,
     subject,
     schemaContent,
}) {
     const metaData = [
          {
               name: `description`,
               content: description,
          },
          {
               name: `keywords`,
               content: keywords,
          },
          {
               name: `robots`,
               content: robots,
          },
          {
               name: `language`,
               content: language,
          },
          {
               name: `owner`,
               content: owner,
          },
          {
               name: `distribution`,
               content: distribution,
          },
          {
               name: `country`,
               content: country,
          },
          {
               name: `object`,
               content: object,
          },
          {
               name: `email`,
               content: email,
          },
          {
               name: `resource-type`,
               content: resourceType,
          },
          {
               name: `subject`,
               content: subject,
          },
          {
               name: `robots`,
               content: `index,follow`,
          },
          {
               name: `googlebot`,
               content: `index,follow`,
          },
          {
               property: `og:title`,
               content: title,
          },
          {
               property: `og:description`,
               content: title,
          },
          {
               property: `og:type`,
               content: `website`,
          },
          {
               name: `og:image`,
               content: `https://www.socialzinger.com/assets/img/logo-1.png`,
          },
          {
               name: `og:local`,
               content: `en_US`,
          },
          {
               name: `og:site_name`,
               content: `socialzinger | Official Website`,
          },
          {
               name: `twitter:card`,
               content: `summary`,
          },
          {
               name: `twitter:creator`,
               content: author,
          },
          {
               name: `twitter:title`,
               content: title,
          },
          {
               name: `twitter:description`,
               content: description,
          },
     ].concat(meta);

     const router = useRouter()

     const [schemaJson, setSchemaJson] = useState(schemaContent ? JSON.parse(schemaContent) : "")

     return (

          <Head>
               <title>{title}</title>
               <link
                    rel="icon"
                    type="image/jpg"
                    href="https://www.socialzinger.com/assets/img/logo/favicon.webp?png"
               />

               <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

               {metaData.map(({ name, content }, i) => (
                    content? <meta name={name} content={content} /> : null
               ))}

               {metaData.map(({ property, content }, i) => (
                    content? <meta property={property} content={content} /> : null
               ))}

               <link rel="canonical" href={`https://www.socialzinger.com${router.pathname}`} />
               <meta httpEquiv="content-language" content="en" />

               <meta
                    name="google-site-verification"
                    content="0ye7fSI6KdYLsdVfziHh9OiSK-BVEEGkWJHsBmzcBto"
               />

               {
                    schemaJson? <script type="application/ld+json" dangerouslySetInnerHTML={{__html: `${schemaJson}` }}></script> : null
               }
          </Head>
     );
}

SEO.defaultProps = {
     lang: `en`,
     meta: [],
};
