import Link from "next/link";
import Slider from "react-slick";
import { clientTestimonial } from "../../sliderTestimonial";

const TestimonialMain = ({bgColor}) => {
    return (

        <>

            {/*====== Start Testimonial Section ======*/}
            <section className="testimonials-section  p-t-50 p-b-50" style={{backgroundColor: bgColor? bgColor : "#fff"}}>
                <div className="container">
                    <div className="row align-items-center justify-content-lg-between justify-content-center">
                        <div className="col-lg-12 col-md-12 order-lg-first">
                            <div className="main-top-heading text-center">
                                <h2 className="title">
                                    Clients Speak Louder Than Us
                                </h2>
                            </div>
                            <Slider
                                {...clientTestimonial}
                                className="client_speak testimonial-slider-v1 testimonial-v1-fancy-boxed testimonial-extra-margin"
                                id="testimonialActiveThree"
                            >
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/testimonial-01.webp"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p className="">
                                                I've been using zinger for a few months now for multiple accounts, and I've seen noticeable growth in my social media accounts. The team delivers on their promises and provides excellent service. The customer support is friendly and quick to address any concerns. Overall, a great experience.
                                            </p>
                                            <h6>Lucas Benett</h6>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/testimonial-02.webp"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p className="">
                                                I can't say enough good things about the services provided by this social media marketing agency. They have significantly boosted my account's visibility and engagement.
                                            </p>
                                            <h6>William Hughes</h6>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/testimonial-03.jpg"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p>
                                                I'm extremely impressed with the results I've seen since using Social Zinger. They've helped me gain a significant number of followers and engagement on my accounts. The customer support team has been responsive and helpful throughout the process. Highly recommended!
                                            </p>
                                            <h6>Eva cater</h6>
                                        </div>
                                    </div>
                                </div>
                            </Slider>
                        </div>
                    </div>
                </div>
            </section>
            {/*====== End Testimonial Section ======*/}


        </>

    );
};

export default TestimonialMain;
