import Link from "next/link";
import Slider from "react-slick";
import { useCaseTestimonial } from "../../sliderTestimonial";

const UseCase = () => {
    return (

        <>

            {/*====== Start Testimonial Section ======*/}
            <section className="testimonials-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row align-items-center justify-content-lg-between justify-content-center">
                        <div className="col-lg-12 col-md-12 order-lg-first">
                            <div className="main-top-heading text-center">
                                <h2 className="title">
                                    Usecase
                                </h2>
                            </div>
                            <Slider
                                {...useCaseTestimonial}
                                className="client_speak testimonial-slider-v1 testimonial-v1-fancy-boxed testimonial-extra-margin m-t-40"
                                id="testimonialActiveThree"
                            >
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/usecase.gif"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p className="title text-orange">
                                                If a struggling actor had our growth plan, they would be an Oscar-winning A-lister commanding the red carpet. Take a bow, Leonardo DiCaprio!
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/usecase2.gif"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p className="title text-orange">
                                                If a student had our growth plan, they would graduate in the morning and become the CEO by the evening. Talk about accelerated success!
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <div className="bg-white">
                                    <div className="col-lg-12 d-flex flex-wrap align-items-center p-0">
                                        <div className="col-lg-4 p-0">
                                            <img
                                                src="/assets/img/testimonial/usecase3.gif"
                                                alt="testimonial author"
                                            />
                                        </div>
                                        <div className="col-lg-8">
                                            <div className="quote-icon">
                                                <i className="flaticon-right-quote h1" />
                                            </div>
                                            <p className="title text-orange">
                                                If a kitten had our growth plan, it would be a fierce lion ruling over the internet jungle.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </Slider>
                        </div>
                    </div>
                </div>
            </section>
            {/*====== End Testimonial Section ======*/}


        </>

    );
};

export default UseCase;
