import Link from "next/link";


const UseCaseOfficial = () => {
    return (

        <>

            {/*====== Start Testimonial Section ======*/}
            <section className="about-section p-t-50 ">
                <div className="container">
                    <div>
                        <h2 className="heading-new-layout text-center p-b-30">User Case</h2>
                    </div>
                    <div className="row justify-content-lg-between justify-content-center align-items-center">

                        <div className="col-xl-8 col-lg-6 col-md-10">
                            <div className="about-text">
                                <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-30">
                                    <p className="custom-usecase-heading">
                                        “Alex”{" "}

                                    </p>
                                </div>

                                <p className="custom-usecase">
                                    I've been using zinger for a few months now for
                                    multiple accounts, and l've seen noticeable growth
                                    in my social media accounts. The team delivers on
                                    their promises and provides excellent service.
                                    The customer support is friendly and quick to address
                                    any concerns. Overall, a great experience.Lucas Benett
                                </p>


                            </div>

                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-10 m-t-20">
                            <div className="preview-blob-image with-floating-icon ">
                                <img src="assets/img/usecase/img1.png" alt="Image" />


                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/*====== End Testimonial Section ======*/}

            <section className="about-section p-t-50 ">
                <div className="container">
                    <div className="row justify-content-lg-between d-flex flex-column flex-sm-row-reverse justify-content-center align-items-center">
                        <div className="col-xl-8 col-lg-6 col-md-10">
                            <div className="about-text">
                                <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-30">
                                    <p className="custom-usecase-heading">
                                        “Casse”{" "}

                                    </p>
                                </div>

                                <p className="custom-usecase">
                                    I can't say enough good things about the services
                                    provided by this social media marketing agency
                                    They have significantly boosted my account's visibility
                                    andengagement.
                                </p>


                            </div>

                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-10 m-t-20">
                            <div className="preview-blob-image with-floating-icon ">
                                <img src="assets/img/usecase/img2.png" alt="Image" />


                            </div>
                        </div>


                    </div>
                </div>
            </section>
            <section className="about-section p-b-50">
                <div className="container">
                    <div className="row justify-content-lg-between justify-content-center align-items-center">
                        <div className="col-xl-8 col-lg-6 col-md-10">
                            <div className="about-text">
                                <div className="common-heading tagline-boxed-two title-line line-less-bottom m-b-20 m-t-20">
                                    <p className="custom-usecase-heading">
                                        “Jerry”{" "}

                                    </p>
                                </div>

                                <p className="custom-usecase">I'm extremely impressed with the resultsI've seen
                                    since using Social Zinger.They've helped me gain a
                                    significant number of followers and engagement or
                                    my accounts. The customer support tea has been
                                    responsive and helpful throughout the process.
                                    Highly recommended!
                                </p>


                            </div>

                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-10 m-t-20">
                            <div className="preview-blob-image with-floating-icon ">
                                <img src="assets/img/usecase/img3.png" alt="Image" />


                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>

    );
};

export default UseCaseOfficial;
