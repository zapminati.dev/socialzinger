export default function PostFooter() {
     return (
          <>
               <div className="post-footer m-t-40">
                    <ul className="related-tags">
                         <li className="item-heading">Related Tags: </li>
                         <li>
                              <a href="#">Landing</a>
                         </li>
                         <li>
                              <a href="#">UI Design</a>
                         </li>
                         <li>
                              <a href="#">Development</a>
                         </li>
                         <li>
                              <a href="#">Mobile Apps</a>
                         </li>
                    </ul>
                    <ul className="social-links">
                         <li className="item-heading">Share :</li>
                         <li>
                              <a href="#">
                                   <i className="fab fa-facebook-f"></i>
                              </a>
                         </li>
                         <li>
                              <a href="#">
                                   <i className="fab fa-twitter"></i>
                              </a>
                         </li>
                         <li>
                              <a href="#">
                                   <i className="fab fa-instagram"></i>
                              </a>
                         </li>
                         <li>
                              <a href="#">
                                   <i className="fab fa-behance"></i>
                              </a>
                         </li>
                    </ul>

                    <div className="post-author-box">
                         <div className="author-thumbnail">
                              <img
                                   src="/assets/img/blog/author-thumbnail.jpg"
                                   alt="Post Author"
                              />
                         </div>
                         <div className="author-content">
                              <h4 className="name">Nathan George</h4>
                              <p>
                                   Quis autem veleum iure reprehenderit quinea
                                   voluptate esse quam molestiae consequatu
                                   velillum dolorem fugiat quo voluptas nulla
                                   pariano one rejects
                              </p>
                              <ul className="social-links">
                                   <li>
                                        <a href="#">
                                             <i className="fab fa-facebook-f"></i>
                                        </a>
                                   </li>
                                   <li>
                                        <a href="#">
                                             <i className="fab fa-twitter"></i>
                                        </a>
                                   </li>
                                   <li>
                                        <a href="#">
                                             <i className="fab fa-instagram"></i>
                                        </a>
                                   </li>
                                   <li>
                                        <a href="#">
                                             <i className="fab fa-behance"></i>
                                        </a>
                                   </li>
                                   <li>
                                        <a href="#">
                                             <i className="fab fa-dribbble"></i>
                                        </a>
                                   </li>
                              </ul>
                         </div>
                    </div>
               </div>
          </>
     );
}
