import Link from "next/link";
import React from "react";
import CategoryList from "./CategoryTag";
import Date from "./Date";
import BlogListImage from "./BlogListImage";

export default function BlogCard({ post }) {
     // console.log(post.excerpt);
     return (
          <>
               <div
                    className="col-lg-6 col-md-6 col-sm-10 wow fadeInUp"
                    data-wow-delay="0.2s"
               >
                    <div className="latest-news-box">
                         <div className="post-thumbnail hover-overly-zoom">
                              <BlogListImage post={post} />
                         </div>
                         <div className="post-content">
                              <h4 className="title blog-title-nowrap">
                                   <Link href={`/blog/${post?.slug}`}>
                                        {post?.title}
                                   </Link>
                              </h4>
                         </div>
                    </div>
               </div>
          </>
     );
}
