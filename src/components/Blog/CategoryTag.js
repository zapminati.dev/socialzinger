import Link from "next/link";


export default function CategoryList({post}){

    return (
        <>
            {post?.categories?.nodes?.map((category) => (
            <Link
              href={`/category/${category?.slug}`}
              key={category?.slug}
            >
              {category?.name}
            </Link>
          ))}
        </>
    )
}