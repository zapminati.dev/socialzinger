import Link from "next/link";

export default function LatestArticle() {
     return (
          <>
               <div className="widget latest-post-widget">
                    <h4 className="widget-title">Latest Articlse</h4>
                    <div className="popular-posts-wrapper">
                         <div className="popular-posts-item">
                              <div className="popular-posts-thumbnail">
                                   <Link href="/blog-details">
                                        <a>
                                             <img
                                                  src="/assets/img/blog/latest-post-thumbnail-1.jpg"
                                                  alt="latest post one"
                                             />
                                        </a>
                                   </Link>
                              </div>
                              <div className="popular-posts-item-content">
                                   <h5 className="popular-posts-title">
                                        <Link href="/blog-details">
                                             <a>
                                                  Build Seamless Spreadshet
                                                  Import Experience
                                             </a>
                                        </Link>
                                   </h5>
                                   <a href="#" className="posts-date">
                                        <i className="far fa-calendar-alt"></i>{" "}
                                        25 May 2021
                                   </a>
                              </div>
                         </div>
                         <div className="popular-posts-item">
                              <div className="popular-posts-thumbnail">
                                   <Link href="/blog-details">
                                        <a>
                                             <img
                                                  src="/assets/img/blog/latest-post-thumbnail-2.jpg"
                                                  alt="latest post two"
                                             />
                                        </a>
                                   </Link>
                              </div>
                              <div className="popular-posts-item-content">
                                   <h5 className="popular-posts-title">
                                        <Link href="/blog-details">
                                             <a>
                                                  Creating Online Environment
                                                  Work Well Older
                                             </a>
                                        </Link>
                                   </h5>
                                   <a href="#" className="posts-date">
                                        <i className="far fa-calendar-alt"></i>{" "}
                                        25 May 2021
                                   </a>
                              </div>
                         </div>
                         <div className="popular-posts-item">
                              <div className="popular-posts-thumbnail">
                                   <Link href="/blog-details">
                                        <a>
                                             <img
                                                  src="/assets/img/blog/latest-post-thumbnail-3.jpg"
                                                  alt="latest post three"
                                             />
                                        </a>
                                   </Link>
                              </div>
                              <div className="popular-posts-item-content">
                                   <h5 className="popular-posts-title">
                                        <Link href="/blog-details">
                                             <a>
                                                  Signs Website Feels More
                                                  Haunted House
                                             </a>
                                        </Link>
                                   </h5>
                                   <a href="#" className="posts-date">
                                        <i className="far fa-calendar-alt"></i>{" "}
                                        25 May 2021
                                   </a>
                              </div>
                         </div>
                    </div>
               </div>
          </>
     );
}
