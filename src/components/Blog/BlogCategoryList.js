import Link from "next/link";


export default function BlogCategoryList({categories}) {
     
     return (
          <>
               <div className="widget category-widget">
                    <h4 className="widget-title">Category</h4>

                    <ul className="category-link">
                         {
                              categories?.map((c)=>(
                                   <li key={c?.slug}>
                                        <Link href={`/category/${c?.slug}`}>{c?.name}</Link>
                                   </li>
                              ))
                         }
                    </ul>
               </div>
          </>
     );
}
