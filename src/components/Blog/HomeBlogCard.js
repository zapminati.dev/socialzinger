import Link from "next/link";
import React from "react";
import CategoryList from "./CategoryTag";
import Date from "./Date";
import BlogListImage from "./BlogListImage";

export default function HomeBlogCard({ post }) {
     return (
          <>
               <div
                    className="col-lg-4 col-md-6 col-sm-10 wow fadeInUp d-flex"
                    data-wow-delay="0.2s"
               >
                    <div className="content latest-news-box m-t-30 border border-1 p-0">
                         <div className="post-thumbnail hover-overly-zoom">
                              <div className="content-overlay"></div>
                              <BlogListImage post={post} />
                              <div className="content-details fadeIn-bottom">
                              <div
                                   className="mb-3 blog-list-text-overflow"
                                   dangerouslySetInnerHTML={{
                                        __html: post.excerpt,
                                   }}
                              ></div>
                                   {/* <p className="content-text"><i className="fa fa-map-marker"></i> India</p> */}
                              </div>
                         </div>
                         <div className="post-content">
                              <ul className="post-meta justify-content-between">
                                   <li>
                                   <i className="fas fa-list-alt" />&nbsp;<CategoryList post={post} />
                                   </li>
                                   <li>
                                        <a href="#">
                                             <i className="far fa-calendar-alt" />
                                             &nbsp;<Date dateString={post.date} />
                                        </a>
                                   </li>
                              </ul>
                              <h4 className="title">
                                   <Link href={`/blog/${post.slug}`}>
                                        {post.title}
                                   </Link>
                              </h4>
                              {/* <div
                                   className="mb-3 blog-list-text-overflow"
                                   dangerouslySetInnerHTML={{
                                        __html: post.excerpt,
                                   }}
                              ></div> */}
                              {/* <Link href="/blog-details">
                                                  <a className="read-more-btn">
                                                       Read more{" "}
                                                       <i className="fas fa-arrow-right" />
                                                  </a>
                                             </Link> */}
                         </div>
                    </div>
               </div>
          </>
     );
}
