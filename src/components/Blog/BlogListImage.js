import Image from "next/image";
import Link from "next/link";

export default function FeaturedImage({post}) {

    let img = '';
    const defaultFeaturedImage = "https://blog.socialzinger.com/wp-content/uploads/2023/03/default_image.png"
    const defaultWidth = "550px"
    const defaultHeight = "234px"

    if(post?.featuredImage){
        let size = post?.featuredImage?.node?.mediaDetails?.sizes[0]
        img = {
            src: size.sourceUrl,
            width: size.width,
            height: size.height,
        }
    }else{
        img = {
            src: defaultFeaturedImage,
            width: defaultWidth,
            height: defaultHeight,
        }
    }

    return (
        <>
        <div style={{}}>
        <Link href={`/blog/${post?.slug}`}>
            <Image className="content-image" style={{ borderRadius: "10px 10px 0 0", backgroundSize: "cover"}} width={`550px`} height={`234px`} objectFit="fill" src={img?.src} alt={post?.title} />
        </Link>
        </div>
        </>
    )
}