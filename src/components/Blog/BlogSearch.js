export default function BlogSearch() {
     return (
          <>
               <div className="widget search-widget">
                    <h4>Search Here</h4>
                    <form>
                         <input
                              type="search"
                              placeholder="Search"
                              name="search"
                         />
                         <button className="search-btn">
                              <i className="far fa-search"></i>
                         </button>
                    </form>
               </div>
          </>
     );
}
