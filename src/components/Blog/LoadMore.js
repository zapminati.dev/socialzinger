import { getPostList } from "../../lib/posts"
import { useState } from "react"

export default function LoadMore({posts, setPosts, taxonomy = null}){

    const [buttonText, setButtonText] = useState("Load More Posts")
    const [buttontDisabled, setButtonDisabled] =  useState(false)

    const handleOnClick = async(event)=>{
        let clickBtn = event.target

        setButtonText('Loading...')
        setButtonDisabled(true)

        const morePosts = await getPostList(posts?.pageInfo?.endCursor, taxonomy)

        let updatedPosts = {
            pageInfo: {
    
            },
            nodes: []
        }
    
        updatedPosts.pageInfo = morePosts.pageInfo

        posts.nodes.map((node)=>{
            updatedPosts.nodes.push(node)
        })

        morePosts.nodes.map((node)=>{
            updatedPosts.nodes.push(node)
        })

        setPosts(updatedPosts)
        
        if(morePosts.pageInfo.hasNextPage){
            setButtonText('Load More')
            setButtonDisabled(false)
        }else{
            setButtonText('No more post')
            setButtonDisabled(true)
        }

    }

    return(
        <>
        <button onClick={handleOnClick} disabled={posts?.pageInfo?.hasNextPage ? buttontDisabled : true} className="template-btn template-btn-2 primary-bg-3 m-t-40">
            {posts?.pageInfo?.hasNextPage ? buttonText : 'No more post'}  <i className="far fa-arrow-right"></i>
        </button>
        </>
    )
}