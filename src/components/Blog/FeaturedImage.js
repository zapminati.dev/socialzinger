import Image from "next/image";
import Link from "next/link";

export default function FeaturedImage({post}) {

    let img = '';
    const defaultFeaturedImage = "https://blog.socialzinger.com/wp-content/uploads/2023/03/default_image.png"
    const defaultWidth = "300"
    const defaultHeight = "200"

    if(post?.featuredImage){
        let size = post?.featuredImage?.node?.mediaDetails?.sizes[1] ? post?.featuredImage?.node?.mediaDetails?.sizes[1] : post?.featuredImage?.node?.mediaDetails?.sizes[0]
        img = {
            src: size.sourceUrl,
            width: size.width,
            height: size.height,
        }
    }else{
        img = {
            src: defaultFeaturedImage,
            width: defaultWidth,
            height: defaultHeight,
        }
    }

    return (
        <>
        <div style={{width: img?.width, height: img?.height, position: "relative"}}>
        <Link href={`/blog/${post?.slug}`}>
            <Image className="content-image" style={{ borderRadius: "10px 10px 0 0"}} src={img?.src} width={img?.width} height={img?.height} objectFit="cover" alt={post?.title} />
        </Link>
        </div>
        </>
    )
}