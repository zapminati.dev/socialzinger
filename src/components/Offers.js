import Link from "next/link";

import Slider from "react-slick";
import { testimonialActiveFour } from "../sliderProps";

const Offers = () => {
    return (

        <>

            <section className="pricing-section offer-landing-section p-t-20 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="common-heading tagline-boxed-two title-line m-b-10 text-center">
                                <div className="offer-landing">
                                    <img src="/assets/img/offer.webp?png" alt="Line" />
                                    <h2 className="title">
                                        {" "}
                                        <span>OFFERS<img src="/assets/img/15.webp?png" alt="Line" /></span>
                                    </h2>
                                </div>

                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="common-heading tagline-boxed-two title-line m-b-40 text-center">
                                <span className="custum-heading">Get ahead of the game with our pre-launch offer! Sign up now and receive a bonus of 100+
                                    Instagram followers, YouTube subscribers, and TikTok views absolutely free. This limited-time
                                    offer is the perfect way to boost your social media presence and grow your following.
                                    Don't miss out, join today!</span>
                            </div>

                        </div>
                    </div>

                    {/* <!-- Pricing Table --> */}
                    <div className="row justify-content-center">
                        {/* <div className="col-lg-3 col-md-6 col-sm-8">
                            <div className="pricing-table offer-table" >
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />
                                    <img
                                        src="/assets/img/free-top.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon free-icon"
                                    />

                                </div>
                                <div className="plan-cost">
                                    <h3 className="title">Instagram<br />
                                        Likes</h3>
                                    <p className="plan-feature m-t-10">Lorem ipsum dolor sit amet
                                        adicing elit maecenas sa
                                        faubus mollis interdum.</p>
                                    <a href="/buy-instagram-likes" className=" offer-button hero-btns justify-content-center">
                                        Claim Now
                                    </a>
                                </div>
                            </div>
                        </div> */}
                        <div className="col-lg-5 col-md-6 col-sm-8">
                            <div className="pricing-table offer-table"
                            // style={{ background: "linear-gradient(199.78deg, #EB5373 1.55%, #9D324A 97.56%)" }}
                            >
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/icon/12.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />
                                    <img
                                        src="/assets/img/free-top.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon free-icon"
                                    />

                                </div>
                                <div className="plan-cost ">
                                    <h3 className="title">Instagram<br />
                                        Likes</h3>
                                        <ul className="check-list m-t-30 m-b-30">
                                        <li>Quick Response to customer inquiries</li>
                                        <li>Instant delivery of services you have chosen</li>
                                        <li>Assurance of all original and authentic accounts</li>
                                    </ul>

                                    <a href="/buy-instagram-likes" className="template-btn  justify-content-center">
                                        Claim Now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 col-md-6 col-sm-8">
                            <div className="pricing-table offer-table pricing-secondary">
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/10.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />
                                    <img
                                        src="/assets/img/free-top.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon free-icon"
                                    />

                                </div>
                                <div className="plan-cost">
                                    <h3 className="title">TikTok<br />
                                        Followers</h3>
                                        <ul className="check-list m-t-30 m-b-30">
                                        <li>Quick Response to customer inquiries</li>
                                        <li>Instant delivery of services you have chosen</li>
                                        <li>Assurance of all original and authentic accounts</li>
                                    </ul>
                                    <a href="buy-instagram-likes" className="template-btn justify-content-center">
                                        Claim Now
                                    </a>
                                </div>
                            </div>
                        </div>
                        {/* <div className="col-lg-3 col-md-6 col-sm-8">
                            <div className="pricing-table offer-table pricing-secondary">
                                <div className="plan-title-area">
                                    <img
                                        src="/assets/img/11.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon"
                                    />
                                    <img
                                        src="/assets/img/free-top.webp?png"
                                        alt="Plan icon"
                                        className="plan-icon free-icon"
                                    />

                                </div>
                                <div className="plan-cost">
                                    <h3 className="title">Youtube<br />
                                        Subscribers</h3>
                                    <p className="plan-feature m-t-10">Lorem ipsum dolor sit amet
                                        adicing elit maecenas sa
                                        faubus mollis interdum.</p>
                                    <a href="buy-instagram-views" className=" offer-button justify-content-center">
                                        Claim Now
                                    </a>
                                </div>
                            </div>
                        </div> */}
                    </div>

                </div>
            </section>


        </>

    );
};

export default Offers;
