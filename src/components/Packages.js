
import Link from "next/link";
import { useEffect, useState } from "react";
import Router from "next/router";
import services from "../lib/services.json";
import {packageSlider} from "../sliderTestimonial"
import Slider from "react-slick";

const Plans = ({ servicePageName }) => {

    const [service, setService] = useState([])
    const [selectPakage, setSelectPakage] = useState()
    const [payAmount, setPayAmount] = useState(0)

    const filteredService = Object.keys(services)
        .filter((key) => key == servicePageName)
        .reduce((obj, key) => {
            //obj[key] = services[key];
            return services[key];
        }, {});

    const handlePackage = (e) => {
        const value = e.currentTarget.value
        const pakage = service.filter((curElem) => curElem.id == value)
        setSelectPakage(pakage[0].id)
        setPayAmount(pakage[0].salePrice)
    }

    const handleOrder = () => {
        if(payAmount <= 0){
            alert("Please select service before proceed to pay.")
            return false
        }

        const selectedPackage = service?.filter((curElem) => curElem.id == selectPakage)

        const packageDetails = {
            id: selectedPackage[0].id,
            serviceName: selectedPackage[0].name,
            qty: selectedPackage[0].quantity,
            sPrice: selectedPackage[0].salePrice,
            disc: selectedPackage[0].discountAmount,
            regex: selectedPackage[0].regex,
            errMsg: selectedPackage[0].errorMsg
        }

        sessionStorage.setItem("packageDetails", JSON.stringify(packageDetails))

        Router.push("/order/details")
    }

    useEffect(() => {
        // localStorage.clear();
        setService(filteredService)
    }, [])

    if (typeof document !== 'undefined') {
        // Get all the package elements
        const packageElements = document.querySelectorAll('.package');
        // Add click event listener to each package element
        packageElements.forEach(packageElement => {
            packageElement.addEventListener('click', () => {
                // Remove 'selected' class from all package elements
                packageElements.forEach(element => {
                    element.classList.remove('selected');
                });
                // Add 'selected' class to the clicked package element
                packageElement.classList.add('selected');
            });
        });
    }
    return (
        <>
            {
                service.length > 0 ? (
                    <div className="container service-list-all">
                <div className=" justify-content-center">
                    <div className="main-top-heading  m-b-20">
                        <h2 className="heading">
                            Choose Your Plan
                        </h2>
                    </div>
                    {/* <Slider
                        className="testimonial-slider-v1 testimonial-boxes-v2 testimonial-arrows-2 d-flex"
                        id="heroSlider"
                        {...heroSlider}
                    > */}
                    <Slider
                        className="testimonial-slider-v1 testimonial-boxes-v2 testimonial-arrows-2 d-flex align-items-center"
                        id="heroSlider"
                        {...packageSlider}
                    >
                        {
                            filteredService.map((data, i) =>
                                <a href={data.sellix}>
                                    <div className="col custum-row custum-row" key={i}>
                                    <div className="plan is-active package image-boxes-v2">
                                        <button className="btn-plan-inner" type="button" value={i}
                                            onClick={handlePackage}>
                                            <div className="plan__inner ">
                                                <div className="plan__title">
                                                    <strong>{data.quantity}</strong> {data.name}
                                                </div>
                                                <div className="plan__price text-sm">
                                                    <strong className="text-orange">${data.salePrice}</strong>
                                                </div>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                                </a>
                            )
                        }
                    </Slider>
                    {/* </Slider> */}

                    {/* <div className="row align-items-top text-center justify-content-center">
                        <div className="col-lg-7 col-md-10">
                            <div className="analysis-text-block  p-l-md-0">
                                <div className="common-heading m-t-20 m-b-10">
                                    <div className="plan__title"><strong> ${payAmount}</strong></div>
                                </div>

                                <button className="btn-blue-1" onClick={handleOrder}>
                                        <strong>Buy Now <i className="fas fa-arrow-right"></i></strong></button>

                            </div>
                        </div>
                    </div> */}
                </div>
            </div>
                ): null

            }

        </>
    );
};

export default Plans;
