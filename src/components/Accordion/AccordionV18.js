import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV18 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is Buying Instagram Power Likes Legal?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            We aren’t lawyers and cannot offer legal advice, but we can tell you that buying Instagram PowerLikes is safe with us. Our slow but steady approach to delivering your likes helps you remain safe, and your account won’t be flagged for the activity that goes against Instagram’s terms of service.
                        </p>

                        <p>Buying PowerLikes with us is easy. Begin by choosing the right package, and then add your username. Make your videos/posts then let our system detect them. We then “like” and interact with the post. You can review your analytics to see the difference.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Long Will It Take Me To Get My Power Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            It will not take long to get your power likes. Our system works fast, but also follows rules that will keep you protected from getting your account shut down. Our system takes your username and waits for you to make a post.
                        </p>

                        <p>Once you’ve made that post, we will send an account over to view and like it. Our accounts will wait a few seconds before liking and viewing/interacting with the post so that the traffic appears organic. Afterward, check your analytics to see the results.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Is It Safe To Buy Instagram Power Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            Yes. Our team knows that you do not want to be flagged by Instagram for buying Power Likes, so we have created a safe and secure process that ensures everybody is happy. The secret is simple. We merely make sure that all the likes and other interactions we do for our customers are real.
                        </p>
                        <p>Real followers go out and drop likes, engage with the post, and more- that way, nobody is violating the Terms of Service and risking their account safety. We do not ask for sensitive or identifying data when you buy with us, so don’t worry about having your identity or social media profile stolen. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Why Do Others Buy Instagram Power Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            When somebody sees your post, the probability of those posts getting noticed by other people goes up. When you get accounts with many followers liking your post, you will see it gains more traction.
                        </p>
                        <p>It’s a side effect of the Instagram algorithm, and your post could make it to the “Explore Page” of Instagram. If a person with 100K followers on Instagram likes your post, you now have the exposure of 100K people- increasing the chance it could go viral and be seen by thousands more people than you’re used to. </p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        What Is Instagram Powerlike?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            Let’s assume you post a video of yourself singing an Ariana Grande hit. Your classmate with 1000 followers likes your post. But then, Ari herself likes the post (and Ms. Grande has millions of followers).
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        Which Of Those Two Accounts Matters More?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            More than likely, Grande’s account will offer you greater exposure because her following is established.
                        </p>

                        <p>The Instagram algorithm puts a higher value on engagements from accounts with large followings. Interaction from huge accounts will always garner more traffic and boost your account, which can help you enjoy more growth on Instagram.
                        </p>

                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 9 ? 0 : 9)}
                        aria-expanded={toggle === 9 ? "true" : "false"}
                    >
                        How Many Followers Do The Power Likes Have?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            Our team works with many different accounts. The follower count for the Power Likes accounts we work with has hundreds to thousands of followers. You can benefit greatly from the power likes offered by our company.

                        </p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 10 ? 0 : 10)}
                        aria-expanded={toggle === 10 ? "true" : "false"}
                    >
                        How Much Does It Cost To Purchase Instagram Power Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            The total cost of buying Instagram power likes will vary based on what you’re buying. Choose your preference and remember, buying in bulk will help you save money.

                        </p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 11 ? 0 : 11)}
                        aria-expanded={toggle === 11 ? "true" : "false"}
                    >
                        What Are The Benefits Of Purchasing Power Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            Your journey to influencer status/better business/more engagement starts with Power Likes from Instagram.
                        </p>
                        <p>Without using Power Likes, you miss out on growing your following.</p>
                        <p>You can increase your reach with our Power Likes. The posts have a greater likelihood of being seen by even more people. Your posts will appear in people’s feeds when they get more likes. </p>
                        <p>The more people who see your post, the more likes, and followers you get. You can have an easier time growing your following. </p>
                        <p>You have a greater likelihood of appearing on the Explore page also. It’s where you go to locate exciting, fresh content on Instagram. If this content is featured there, you may enjoy plenty more likes and followers. <a href="https://www.socialzinger.com/buy-instagram-followers">Buy Instagram Followers</a>  from Social Zinger now!</p>
                        <p>The Power Likes help you make it to the Explore Page, where your posts will enjoy more likes. They will also have a better chance of getting noticed by the algorithm. </p>

                    </div>
                </Accordion.Collapse>
            </div>

        </Accordion>
    );
};

export default AccordionV18;
