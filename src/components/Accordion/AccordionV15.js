import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV15 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is Buying Instagram Comments A Good Idea?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Buying comments is a good idea for your business because it can help increase interactions with your target audience.
                        </p>
                        <p>You want to reach as many users as possible, so buying random comments may not be a bad idea. Comments are considered even more critical than likes on a post, so consider purchasing one of our packages. </p>
                        <p>You can make others more aware of your presence when you buy Instagram followers. Once you’ve posted something online, the comments and likes you receive draw attention to what you’ve posted.
                        </p>
                        <p>The account draws the crowd and interactions go faster. Purchasing Instagram comments from reliable sources could help you show others what you can offer and increase sales.</p>
                        <p>Gaining more followers is another advantage of purchasing Instagram comments. When your Instagram posts feature high engagement, the account is ranked higher on Instagram.</p>
                        <p>It means your account is generating interest among users and keeping them on the app longer. Your account reaches a greater population of people, and the likes, comments, and engagements will speak for themselves. </p>
                        <p>When you reach more people, the benefits begin to show themselves. For example, more comments mean more of a reach to other brands and users.
                        </p>
                        <p>You can instantly attract even more followers to your account. You can gain brands’ trust and they could reach out and ask for collaborations or paid advertising from you.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Is Buying Instagram Comments Safe?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            When you work with us, you can be assured that your account will stay in good standing. Why? We only use real people with real accounts to deliver the comments to your post.
                        </p>

                        <p>When you use low-quality Instagram/social media services, the account risks getting penalized. </p>
                        <p>Let us help you instead. We use real, active accounts to get it done right the first time.</p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Can I Purchase Instagram Comments If My Account Is Set to Private?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            No. We will need your account to be public, so that we may leave comments on it. We do not ask for personal/identifying information during the process.  </p>
                        <p>As you’ve come to expect from online merchants, our online purchase process is protected under SSL encryption, so all data is safe.
                        </p>
                        <p>We do not want your account details as we do want to violate Instagram’s Terms of Service. You will need to leave the post in public for us to do our work. Once the work is done, you can set your account back to private if you wish. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Do You Get Comments on Instagram Fast?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            The best way to get real Instagram comments is to buy them from our team.
                        </p>
                        <p>We have helped other companies grow their following and advertise products with ease, all while they connect to customers and answer questions.</p>
                        <p>We can help you increase engagement on your posts and get people to leave comments. </p>
                        <p>The steps are easy. </p>
                        <ul>
                            <li>Select the number of comments you want and place your order.</li>
                            <li>  Paste in the post URL so we can start working.</li>
                            <li>  Slowly but surely, we will deliver comments to that post, and the engagement will show improvement.
                            </li>
                        </ul>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        Is It Safe to Buy Instagram Comments?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            Yes. Our company follows all Instagram Terms of Service when we do our work. Our followers are made of real people who will go to your post and interact with it as though they found it organically. </p>
                        <p>You don’t need to worry about your account, either. We don’t ask for account information- only the URLs of the posts you want to increase engagement/interaction. </p>
                        <p>Once we have that, we can send our users over to start interacting with the content. We never ask for passwords as we do not work with private accounts. Everything must be public for our team.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        Can You Buy Real Likes on Instagram?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            Yes, buying likes is part of doing business on Instagram. You can find plenty of websites offering “likes” to increase your post’s engagement and performance. It’s not a bad idea if you want to increase visibility for your product or service.
                        </p>
                        <p>You’ll want to use a reputable service such as ours. If you aren’t using honest moves to create a following and foster engagement, your account can be deactivated.
                        </p>
                        <p>You can also <a href="https://www.socialzinger.com/buy-instagram-followers">buy instagarm followers</a>  along with instagram comments. </p>
                        <p>To get the word out regarding your goods or service, and to keep your account intact, use our  <a href="https://www.socialzinger.com/buy-instagram-likes">Instagram Likes</a>   instead and build connections with your customers that way. </p>

                    </div>
                </Accordion.Collapse>
            </div>



        </Accordion>
    );
};

export default AccordionV15;
