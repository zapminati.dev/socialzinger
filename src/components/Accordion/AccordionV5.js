import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV5 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is it safe to buy Youtube subscribers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            With our service, it is 100% safe to buy YouTube subscribers and you don’t have to worry about your channel getting flagged or removed, because we take special care to ensure that all the subscribers we provide comply with YouTube guidelines and can’t be removed. You can rest assured you won’t get flagged for any suspicious activity, something that is a genuine concern with other, untrustworthy websites out there that claim to sell subscribers.
                        </p>
                        <p>Buying YouTube subscribers is completely safe only if you avail of a reliable service like ours that provides trustworthy, lasting engagement that not only boosts your channel but does not put you at risk of getting shadow-banned by the algorithm or anything similar. The safety and promotion of your channel is our topmost priority, and when you work with us, you do so with the security of knowing that we would never compromise on that.
                        </p>
                        <p>Not only that, but we also have impeccable customer service available 24/7. So in case anything goes wrong with your account or any hiccups come up, our experts will always be there to help you navigate the situation and keep your account safe and running. In any case, when you purchase YouTube subscribers from us, we do our utmost to ensure you have peace of mind about your decision.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Are all youtube subscribers real?
                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            Some services use bots or computer codes to generate fake subscribers for your channel. We would never do that to you. When you purchase subscribers from us, you are getting 100% real and permanent subscribers with completed profiles, not anything fishy or suspicious that could get you in trouble. </p>
                        <p>There are many so-called ‘services’ out there that sell YouTube subscribers, but these subscribers are temporary and low-quality with empty profiles that eventually get removed by YouTube. Most such subscribers are generated with bots or computer codes and are easily detected by the algorithm. However, we maintain our high standards and quality commitment by promising never to resort to such tricks so you never have to worry about dealing with fake subscribers when purchasing from us.
                        </p>
                        <p>All the YouTube subscriber plans we offer are 100% real, reliable, and permanent. You can also <a href="https://www.socialzinger.com/buy-Youtube-views"> buy Youtube Views</a> and likes.  The only consequence they will have is promoting your channel and giving you a step-up in the YouTube algorithm so your channel can realize its full potential.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How To Buy Youtube Subscribers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            With us, all you have to do to buy YouTube subscribers is to select your favorite plan, enter your email address and make a one-time payment with your preferred payment method. That is it! We pride ourselves not only on our services but also on how easy and quick they are to avail. Deciding to boost your account doesn’t have to involve a lot of confusion or lengthy processes, as our streamlined, customer-focused process makes the entire method extremely simple and easy. Checkout is also 100% secure and reliable as customer safety is something we work very hard to prioritize.
                        </p>
                        <p>We offer various plans for the number of subscribers you want to purchase. What’s more, we guarantee a 30-day refill on your subscribers so you don’t have to worry about your engagement dropping at any point. Yes, it is that easy.
                        </p>
                        <p>After your purchase, we will continue to support you through refills, guidance, and 24/7 customer service through phone calls, email, and live chat. This means that at no point in the process do you have to feel confused or lost as our staff will always be there to support you.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Long Does It Take For Subscribers To Start Rising?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Waiting a long time for subscribers can be annoying and a hassle, and we completely understand. We also know that the best time to receive engagement is right after posting content, so we begin delivering your subscribers within 48 - 72 hours after you place your order. Your subscribers will increase organically so that you don’t get flagged for any suspicious instantaneous influx of new subscribers.
                        </p>
                        <p>These subscribers will also maintain timely and organic interactions with your videos so your channel engagement will be supplemented in the best way possible.
                        </p>
                        <p>This way, your channel can get picked up and promoted by the algorithm within a few days, and you can start building an audience within your niche as your videos are recommended to more and more people.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        Will subscribers be active on the new videos I will publish?
                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            Yes. Our service provides real, organic, and engaging subscribers who will be active on your videos through views, likes, and comments, ensuring that your channel is boosted not only through an increase in subscriber count but also an influx of organic interactions on your videos. Strategic, real engagement can have a multitude of benefits for your channel. Your content will be promoted through an increase in watch time, better representation in recommended videos, and better search engine rankings.
                        </p>
                        <p>Since our subscribers are real people and not bots, you can expect them to be active on your videos, unlike the subscribers you gain from availing of other unreliable services which are artificial, inactive, and will be removed over time.
                        </p>
                        <p>All this contributes to the rapid growth of your channel, building your niche audience and allowing you to focus on what matters: your creative content and the effort you put into it.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSeven">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse7"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 7 ? 0 : 7)}
                        aria-expanded={toggle === 7 ? "true" : "false"}
                    >
                        Does Social Zinger Provide Me The Privacy Of My Purchase?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse7">
                    <div className="accordion-body">
                        <p>
                            The subscriber plans you purchase with us are 100% private and confidential and the details of your purchase will never be disclosed to anyone else. The privacy of our customers is very important to us, so you can rest assured your information will never be leaked.
                        </p>
                        <p>Moreover, YouTube does not have any logistical way of finding out about any transaction you make outside of their platform, and since our subscribers are organic and active, they are not differentiable from normal subscribers and won’t be detected by any systems or users on the application. This means that you can buy subscribers without any sort of anxiety about your privacy being compromised.
                        </p>
                        <p>To boost your youtube channel more you can <a href="https://www.socialzinger.com/buy-Youtube-likes" >buy youtube likes</a> as well. Buy youtube subscribers now and amaze the world with your content!
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>

            {/* <div className="accordion-item">
                <div className="accordion-header" id="headingEight">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse8"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 8 ? 0 : 8)}
                        aria-expanded={toggle === 8 ? "true" : "false"}
                    >
                        Will people know that I have purchased YouTube subscribers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse8">
                    <div className="accordion-body">
                        <p>
                            No, no one has to know you purchased subscribers, as that information is not made public anywhere on your
                            channel or the internet. All people will see is your subscriber count and the interactions on your videos, which will only boost your credibility and repute on the platform and get your videos into users’ recommended pages by boosting them through the algorithm.

                        </p>
                        <p>The subscriber plans you purchase with us are 100% private and confidential and the details of your purchase
                            will never be disclosed to anyone else. The privacy of our customers is very important to us, so you can rest assured your information will never be leaked.

                        </p>
                        <p>Moreover, YouTube does not have any logistical way of finding out about any transaction you make outside
                            of their platform, and since our subscribers are organic and active, they are not differentiable from normal subscribers and won’t be detected by any systems or users on the application. This means that you can buy subscribers without any sort of anxiety about your privacy being compromised.

                        </p>
                        <p>Buy Youtube subscribers now and amaze the world with your content!
                        </p>
                    </div>
                </Accordion.Collapse>
            </div> */}

        </Accordion>
    );
};

export default AccordionV5;
