import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV14 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is Buying Instagram Story Views Legal?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            It’s OK to buy Instagram story views. Our service uses real accounts to provide the views you’ve paid for, unlike other sites that use bots and codes to make views increase. You can count on our story views to be real people from around the world.
                        </p>
                        <p>We comply with all of Instagram’s Terms of Service, and we do not use anyone except real users to supply you with the views you’ve purchased. </p>
                        <p>We have a good understanding of the Instagram algorithm and social media platforms, and we want to help your account get the exposure it needs. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Long Will It Take Me to Get My Story Views?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            Waiting forever to get your views is highly inconvenient. We don’t want you to wait too long. Directly after a video gets uploaded, you’ll want to start having interactions take place, and we get that.
                        </p>

                        <p>We get to work after you’ve posted your content, once we have the link. You’ll notice improved analytics thanks to our services.</p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Is It Safe to Buy Instagram Story Views?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            Yes. We understand that threats are ever-present in the online world today, especially when shopping for ways to improve your social media profiles. </p>
                        <p>We ensure all orders are protected with SSL encryption, so you can enter payment details without worrying whether or not they will be stolen.
                        </p>
                        <p>Our team also conducts business according to the Instagram Terms of Service, so you don’t need to worry about your account being flagged or banned.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Does Instagram Rank Your Story Views?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Just because somebody’s at the top of your view list, doesn’t mean they’ve come to your feed or watched your story dozens of times. The algorithm Instagram uses shows a viewer list based on who you’re closest to and your activity.
                        </p>
                        <p>Interaction data may be derived from profiles you seek, posts you comment/like on, and
                            swiping up for an Instagram Story. It’s the swipes and taps that tell Instagram who you care most about.
                        </p>
                        <p>The profiles located at the forefront of that list don’t deal with view counts, posts, unfollow ratios, or privacy settings. It won’t tell you if somebody’s “Insta-Stalking” you- it just shows you how you spend time using the app.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        Do I Have to Give My Password to Use This Service?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            <strong>Not at all! </strong>Our team will not ask you for your information now or ever. We need your account to be public so we can deliver as requested. We will not ask you for a password or username info.
                        </p>
                        <p>When making your order, ensure your account is public and include the URL for the post. If any service asks you for your password or other identifying information, do not give them that data and run away. You should also verify that the site is SSL encrypted so payment information is kept safe also.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        Do I Need to Have A Public Instagram Profile To Get Story Views?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            <strong>Yes.</strong> You can only get story views from approved users if you do not have a public profile.
                        </p>
                        <p>You can also see who has reviewed your Instagram Stories, their reactions, and much more. The network informs you about content interaction.
                        </p>
                        <p>When you open up the Instagram story, you can see who has seen it. You can check it out on the Insights tab. This info is available 24 hours after the story is first published. </p>
                        <p>Viewing this info is pretty easy on a desktop computer. Open up the Story and tap the users’ icons. You can also use your profile page. Open the Story, tap the icons for the users, and then switch to the tab with the “eye” icon.</p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSeven">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse7"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 7 ? 0 : 7)}
                        aria-expanded={toggle === 7 ? "true" : "false"}
                    >
                        How Do Buying Instagram Story Views Aid Your Story More Popular?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse7">
                    <div className="accordion-body">
                        <p>
                            Businesses and brands purchase Instagram views all the time when trying to make their business become widely known. When you purchase Instagram story views, the view count increases. You can <a href="https://www.socialzinger.com/buy-instagram-views">buy Instagram views</a> too.
                        </p>
                        <p>Then, when followers and other Instagram users stumble upon your story, they increase the view count. They like what they see. Then, they begin interacting with your other content, sharing it with their friends.</p>
                        <p>Your popularity increases and then the Instagram algorithm begins recommending it to more and more people. You make it onto the Explore page, garner lots of new followers, and watch your brand take off!
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingEight">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse8"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 8 ? 0 : 8)}
                        aria-expanded={toggle === 8 ? "true" : "false"}
                    >
                        What Is the Difference Between Video Views and Story Views?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse8">
                    <div className="accordion-body">
                        <p>
                            With our company, you can purchase Insta videos and real Instagram story views at great rates. But, what’s the primary difference between story views and video views?
                        </p>
                        <p>Our Instagram video views service is for the Instagram video postings that appear on your followers’ feeds. Meanwhile, Instagram story views services are designed for stories you’ve posted that appear on your followers’ story feeds.
                        </p>

                        <p>You only need to share your Instagram username so we can get you the story views you’ve paid for into your Instagram account. We don’t require profile passwords to make our services happen, and delivery happens instantly. Buy authentic views today at a cheap price! You can also avail of services like <a href="https://www.socialzinger.com/buy-instagram-followers">buy Instagram followers</a> , likes, and views.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV14;
