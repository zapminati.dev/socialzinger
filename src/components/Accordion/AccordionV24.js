import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV24 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can You Buy Real Twitter Followers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Yes. you can buy Twitter followers that are real. You must begin by choosing a high-quality website like Social Zinger to do the work for you. Lesser websites may use bot accounts to pull off their work, which we do not recommend.
                        </p>

                        <p>Most of the sites selling Twitter followers are selling bots. If you want to buy Twitter followers, you need to use your noggin and choose the correct provider (that’s us).  </p>
                        <p>It's a far better choice to put in some work and research companies first before using them; that way you can ensure you are buying high-quality Twitter followers that can help with brand growth.</p>
                        <p>Organic growth is the best way to buy Twitter followers. You can gain more likes and followers when you engage using real users on this platform. </p>
                        <p>Twitter growth services, like ours, where you can buy Twitter followers, can help you do this. We use real followers only to produce results. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Much Does It Cost To Buy 1,000 Twitter Followers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>Buying 1000 followers costs $40. These are real followers, and they do all the things you’d expect from Twitter followers. These users do everything an organic follower would do, and they will not breach the daily limits for engagement set forth by Twitter. </p>

                        <p>You don’t have to risk account safety when you buy Twitter followers using Social Zinger. We engage with real followers on Twitter exclusively. That way, when you buy Twitter followers with us, you’re getting the real deal.  </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Is It Illegal To Buy Twitter Accounts?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>No. You can buy more Twitter followers and you aren’t breaking the law. You don’t even have to worry about violating Twitter’s terms of service. It’s normal for businesses and brands to buy Twitter followers because it helps them get more followers onto their profiles.</p>
                        <p>You’ll want to ensure you choose a site that sells real Twitter followers. When you buy Twitter followers, you’ll likely be shown websites that sell low-quality bot accounts.  </p>
                        <p>You want to spend some extra money when you buy Twitter followers to purchase real accounts. That way, they will interact with your content as real followers do.
                        </p>
                        <p>The services at Social Zinger are safe. In moments, followers will begin to interact with your account and follow it. They may even retweet your Tweets and like your posts. </p>
                        <p>The genders and locations of the followers will be varied. Our team provides the finest-quality service, and you can also rest assured knowing our website is secure. We keep all private data encrypted. </p>
                        <p>That way, you can make your purchases and buy Twitter followers with ease. We accept all major credit cards and PayPal, so buy Twitter followers today.</p>
                        <p>Now you can enhance your social media presence not just on twitter but on other social media platforms as well. You can <a href="https://www.socialzinger.com/buy-twitter-views">buy twitter views</a>  and likes on Social Zinger.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Is It Easy to Get 1000 Twitter Followers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>Yes. We make it easy to buy 1000 followers. You can buy Twitter followers in a few easy steps with our website.</p>
                        <p>Allow us to show you how simple it is: </p>
                        <ul>
                            <li>Select the number of how many followers you’d like to buy.</li>
                            <li>Input your Twitter username into the provided box, and if this is done correctly, you should be able to see your account’s image next to that box. </li>
                            <li>Now input the followers you’d like to receive on that account (In this case it will be 1000).</li>
                            <li>The purchase price will adjust accordingly. Ensure you are OK with the price of the followers. </li>
                            <li>Once you’ve reached the follower count and price that makes sense to you, click “Add to cart” and then “Buy Now” so your purchase can be completed. </li>
                            <li>Complete the payment process.</li>
                        </ul>
                        <p>We begin delivering the followers immediately after you make your purchase. It depends on how many more twitter followers you’ve bought, but most deliveries start in about 15 minutes, and you’ll see results within the hour. </p>
                        <p>You can speak with our customer care team 24/7 if you have questions or concerns about the process to buy Twitter followers. We strive for excellence, and we want your repeat business.</p>
                        <p>Social Zinger offers a wide a variety of services for social media platforms on various social media networks including <a href="https://www.socialzinger.com/buy-twitter-likes">buy twitter likes</a> , twitter comments and Instagram followers.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV24;
