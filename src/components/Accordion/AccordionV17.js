import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV17 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Why Choose Social Zinger ?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <h4 className="title mt-2">1- Our delivery is lightning-fast </h4>
                        <p>
                            We offer the quickest delivery for Instagram followers on the market. Within one hour of ordering your followers, we can have them brought over to your page.
                        </p>

                        <p>Suppose you order a thousand followers, and you’re worried that a thousand followers turning up at once might alert Instagram that you bought followers.</p>

                        <p>Don’t worry because we will have them trickle in slowly. That way, growth looks organic. They are real people and will interact with your content as you’d expect from followers that you didn’t buy.</p>
                        <h3 className="title mt-2">2- We offer a guarantee like no other </h3>
                        <p>
                            For our valued customers, we want to leave a positive impression. That way, you’ll be excited to come back and use our services again soon.
                        </p>

                        <p>If you feel unsatisfied with your order’s delivery or quality, speak with our customer service personnel and they can help you. If any order remains unfulfilled your order will be refunded. </p>
                        <h4 className="title mt-2">3- We offer support year-round</h4>
                        <p>
                            The customer service support staff is always online and ready to help. Please contact the team if you need help or have questions/concerns about the order.  </p>
                        <h4 className="title mt-2">4- Our profiles are high-quality</h4>
                        <p>
                            One reason people keep choosing us is that we offer high-quality profiles that make your account look good. Most social media platforms know when such profiles are fake, and that’s when your account gets flagged.  </p>
                        <p>With us, you don’t need to worry about this. Our profiles are real people with real pictures and filled-out bios on Instagram. You’d never know the profiles were bought from a service like ours. </p>
                        <h4 className="title mt-2">5- The followers can help with your business</h4>
                        <p>
                            Using social sites like Instagram will help you promote your brand. Using Instagram and such platforms like it is a free way to get your message out there. If you want to boost your marketing, the plan is to purchase genuine Instagram followers who will help increase your visibility  </p>
                        <p>You’ll learn many reasons why it’s a great idea, such as having many followers increases your credibility. Having many followers indicates you take your business seriously and you care about getting the word out regarding what your business can do.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        When I Sign Up, Am I Agreeing To A Long-Term Contract?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            No. Our packages are not recurring subscriptions. You can buy the followers and come back later for more, or you can continuously purchase them each month when you feel ready.
                        </p>

                        <p>We do not offer subscriptions, but we are happy to work with all customers new and old. Whether you want 100 subscribers or you want thousands, there is no order too big for us to handle.  We can work with you to get your follower count where you’d like to see it.  </p>
                        <p>The quality of our Instagram followers is unmatched when compared with other websites like it. Our accounts are followed and operated by real people. You won’t need to worry about fake accounts getting your page shut down.  </p>
                        <p>We have plenty of satisfied clientele and lots of repeat business, all thanks to the great service we have provided. </p>
                        <p>Your task is easy- just pick the follower/like/view package that appeals most to you, buy it, and let us do the hard work. Your follower count will increase. We encourage you to check out reviews as well. </p>
                        <p>Our place is the best for Instagram packages thanks to our quality service, our customer care, and affordability.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How Much Do 1000 Followers Cost On Ig?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            It costs about $70 for a thousand IG followers.  </p>
                        <p>However, we have multiple packages waiting for our customers. We can meet all of your varied requirements including buying Instagram views, <a href="https://www.socialzinger.com/buy-instagram-followers">buying Instagram followers</a> , and buying Instagram likes. </p>
                        <p>All our unique packages will fit your requirements and help you gain the followers you want and need.</p>
                        <p>If you’re just beginning your journey, start with something small. You can then understand how our website works, and if you like what you got, you can go forward with our service.</p>
                        <p>The smaller packages are ideal for newer businesses because you can get something small- such as 100 followers, for example. Smaller orders help you see how well we can do, without spending a lot. </p>
                        <p>If you have a big account already but you need more credibility, we can fulfill your order. Nothing is too big or small for our team. </p>
                        <p>You can choose whichever package makes sense to you and enjoy instant delivery with zero fakes. All our packages are outfitted with 24-hour customer service. You can speak to us whenever you need help. </p>
                        <p>Finally, you’re sure to enjoy instant delivery because time is important. You don’t need to wait forever to buy followers to see that it helps your Instagram account. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Is There Any Money-Back Guarantee Refund?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Our team promises to fulfill your order. If you do not get all your followers, we will issue a refund to make it right.
                        </p>
                        <p>We promise high-quality followers with active accounts. Don’t settle for cheap or low-grade Instagram account-buying services. Use our company instead- we bring the best-quality followers to your page and get it noticed the genuine way. Moreover, you can count on us to stand by our work.  Social Zinger also offers to <a href="https://www.socialzinger.com/buy-Instagram-likes">buy Instagram likes</a>  and views.</p>
                    </div>
                </Accordion.Collapse>
            </div>



        </Accordion>
    );
};

export default AccordionV17;
