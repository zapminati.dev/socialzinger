import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV27 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button-home"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Q1. What is social media marketing, and why is it important for my business?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>Social media marketing is the process of using social media platforms to promote your brand, products, or services. It involves creating and sharing content, engaging with your target audience, and driving traffic to your website or other desired actions. Social media marketing is essential for businesses because it allows you to reach a wider audience, increase brand awareness, generate leads, and drive sales.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button-home"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Q2. How can a social media marketing agency help my business?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>A social media marketing agency can provide expertise and support in various areas to help your business succeed on social media. They can develop a comprehensive social media strategy tailored to your goals, manage your social media accounts, create and schedule engaging content, conduct audience research, run paid advertising campaigns, monitor performance and analytics, and provide valuable insights and recommendations to optimise your social media presence.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button-home"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Q3. Which social media platforms should my business be active on?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>The choice of social media platforms depends on your target audience and business objectives. It's important to identify where your target audience is most active and tailor your strategy accordingly. Popular platforms for businesses include Facebook, Instagram, Twitter, LinkedIn, YouTube, and Pinterest. A social media marketing agency can help you determine the most suitable platforms for your business and develop a cohesive presence across them.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button-home"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Q4. How can social media marketing help in increasing my brand's visibility?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>Social media marketing plays a crucial role in boosting brand visibility. By consistently sharing valuable content, interacting with your audience, and leveraging relevant hashtags and trends, you can increase your brand's reach and exposure. Additionally, social media advertising allows you to target specific demographics and reach a larger audience, further enhancing your brand's visibility.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV27;
