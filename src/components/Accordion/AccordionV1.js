import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV1 = () => {
  const [toggle, setToggle] = useState(1);
  return (
    <Accordion
      className="accordion"
      id="accordionFAQ"
      defaultActiveKey="collapse"
    >
      <div className="accordion-item">
        <div className="accordion-header">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse2"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 1 ? 0 : 1)}
            aria-expanded={toggle === 1 ? "true" : "false"}
          >
            I Did Not Receive My Order; What to Do Next?
          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse2">
          <div className="accordion-body">
            <p>
              Rest assured; we will make it right! </p>

            <p>First, we need you to do the following:

            </p>
            <ul className="check-list-2  m-t-20">
              <li className="text-dark">Ensure your profile is set to public </li>
              <li className="text-dark">You entered the correct link/username</li>
              <li className="text-dark">You didn't change usernames before you completed the order </li>
              <li className="text-dark">The account and post exist and weren't accidentally deleted</li>
              <li className="text-dark">The post wasn't banned, removed, or restricted in some nations</li>
              <li className="text-dark">Allow us until the estimated delivery date/time to complete the order</li>
            </ul>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="collapseOne">
          <Accordion.Toggle
            as="h3"
            eventKey="collapseOne"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 2 ? 0 : 2)}
            aria-expanded={toggle === 2 ? "true" : "false"}
          >
            How many followers can I buy at one time?
          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapseOne">
          <div className="accordion-body">
            <p>
              You can buy as many as you like. We can handle orders of many sizes, including huge ones. So, feel free to reach out and let us know.
              Whether you want 1000 or 1M likes/views/followers, our team loves creating plans to deliver results.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingThree">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse3"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 3 ? 0 : 3)}
            aria-expanded={toggle === 3 ? "true" : "false"}
          >
            Are the likes, followers, and subscribers real, or are they bots?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse3">
          <div className="accordion-body">
            <p>
              Unlike other services that puff up their followers, comments, and likes with bots, we refuse to take shortcuts. </p>

            <p>You'll get real people giving your posts likes, comments, and shares. Using our proven method, client accounts enjoy organic growth, leading to followers and fans not being involved with our business.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingFour">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse4"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 4 ? 0 : 4)}
            aria-expanded={toggle === 4 ? "true" : "false"}
          >
            Could my account get banned for buying followers, likes, or views?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse4">
          <div className="accordion-body">
            <p>
              Not with our service! You will enjoy real followers, so you don't have to worry about your account getting flagged, banned, or taken down due to bot followers.

              Our followers are authentic, and as pros in the business, we don't take shortcuts. So, you'll be guaranteed authentic followers, likes, views, and more.

            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingFive">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse5"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 5 ? 0 : 5)}
            aria-expanded={toggle === 5 ? "true" : "false"}
          >
            How fast can you deliver my order?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse5">
          <div className="accordion-body">
            <p>
              We begin our work directly after you place the order. We will show you the estimated delivery time after you've placed the order.

              Most of the time, we find that our clients receive their complete order faster than the estimated time! Please remember that it may take shorter or longer than estimated, as we are working with real people here.


            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingSix">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse6"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 6 ? 0 : 6)}
            aria-expanded={toggle === 6 ? "true" : "false"}
          >
            Is buying social media followers illegal?


          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse6">
          <div className="accordion-body">
            <p>
              Yes! We use secure, safe methods to provide followers and other engagement to your account. We will break no terms of service or rules.
              You can count on us to abide by all rules and regulations and keep everything in order.

            </p>
          </div>
        </Accordion.Collapse>
      </div>
    </Accordion>
  );
};

export default AccordionV1;
