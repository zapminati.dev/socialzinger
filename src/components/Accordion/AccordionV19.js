import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV19 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is Buying Instagram Watchers Legal?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Yes, you can buy Instagram watchers from our team. We do not violate the Instagram terms of service. We use real accounts to provide delivery and engagement to all of our followers.
                        </p>

                        <p>You never have to worry about violating Instagram’s Terms of Service. We do not ask for any of your identifying information when you place an order with us, and your data is kept secure thanks to SSL encryption.
                        </p>
                        <p>Because we use real viewers to produce results your account will remain safe. All Instagram Live viewers get sent directly to your profile, and they will begin watching at the designated time or when they drop in (the goal is to be as natural as possible). </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Soon Can I Expect Delivery?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            We provide delivery about five minutes after you place your order. Our team gets to work immediately after you’ve put your order in, letting the views trickle in slowly but surely.
                        </p>

                        <p>You’ll notice your metrics improving after you’ve placed your order, but it happens naturally and won’t cause Instagram to wonder why you’re suddenly getting all these views on your channel.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Can I Buy Impressions From A Targeted Country?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>No. Our viewers are real people from across the globe. We have people using Instagram in the UK, USA, Nigeria, Canada, India, and much more.
                        </p>
                        <p>Similar to getting many views organically, people will come from all over to watch your content.  </p>
                        <p>The order process for obtaining these views is easy. Choose your timeframe and indicate how long you’re going live. You can also add extras like comments and likes on your video.
                        </p>
                        <p>If you would prefer to stick with IG Live, simply continue with your order. At minimum, we ask for 30 minutes of live time and a maximum of four hours.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Long Will Viewers Stay On My Instagram Stream?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Our viewers are trained to stay for the majority of your stream from the moment they arrive. Most of our account holders will stay until the video comes to an end. </p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        What Payment Options Does Social Zinger Accepts?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            All major payment methods are accepted at our company. You can use Apple Pay, Google, Amex, Discover, Mastercard, and Visa plus others.
                        </p>
                        <p>Our website is SSL-encrypted, and we keep all of your data protected. It will never fall into the wrong hands when you are working with us.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        Can My Instagram Account Get Banned For Buying Insta Live Views?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            No. Your account will not get banned when you use our service to buy Instagram live views.
                        </p>

                        <p>We use real accounts owned by real viewers around the world. You’ll get engagement from far and near places. It looks as though the content you made is being viewed naturally.
                        </p>
                        <p>We never ask for any personal information or data to make this happen. Just ensure your account is not set to private and supply us with your URL. That way, we can send our accounts over to get the process started when you go live.</p>

                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 9 ? 0 : 9)}
                        aria-expanded={toggle === 9 ? "true" : "false"}
                    >
                        How Can Video Hits Get Me More Followers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            Getting more video hits helps the algorithm for the better. It gets your account out there and noticed more frequently.</p>
                        <p>  There are plenty of benefits associated with <a href="https://www.socialzinger.com/buy-instagram-views">buying Instagram Views</a> . Tell us when you are going live, and we will ensure viewers are waiting to help those numbers improve.</p>
                        <p> The more views you get, the more interest that is generated by others. You can use our service to get your content out there and build you following naturally.</p>
                        <p> As a side note, this is true of your account on the whole: it’s better to have lots of engagement wherever you can get it. Whether it is Live Videos, story posts, or your feed, having people looking at it is a good idea.</p>
                        <p>  Having lots of people engaged with your content helps others gain confidence in your content.</p>
                        <p>  Think about yourself: When you see a song with 1 million streams/likes/views, you’ll probably click on it to see what the fuss is about. You might not do it for a song with only 40 views/likes/streams.
                        </p>

                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 10 ? 0 : 10)}
                        aria-expanded={toggle === 10 ? "true" : "false"}
                    >
                        What Is The Best Time To Share A Video?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            Once your video has ended, save it. Then, share it within 24 hours.
                        </p>
                        <p>Your audience will be your cue about the best time to share that video. Instagram usually has the greatest traffic during daylight hours.</p>
                        <p> You’ll want to look at your analytics to see when your viewers are most active.
                            Instagram activity is highest from 8 am to 8 pm, with the strongest hours beginning at 11 am and ending at 1 PM on Monday.</p>
                        <p>High-activity hours will vary from day to day. For example, 9 AM to 4 PM are the most active hours for Instagram posts on Sundays.</p>
                        <p> Social Zinger offers many more social media services like buy instagram comments, followers, and likes. </p>

                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV19;
