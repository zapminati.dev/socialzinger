import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV16 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can You Buy Instagram Impressions?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Yes, you can buy Instagram impressions, and it’s a good idea to do so. You can enjoy greater exposure when you do so. Being successful with digital marketing starts with keeping impressions high. Such a value indicates to the viewers how many times users viewed your post.
                        </p>

                        <p>To understand and see these metrics, it’s better to switch your account to “business,” even if you aren’t selling a good or service. That way, it’s easier to see stats about your posts, and you can understand the content your viewers want to see. </p>

                        <p>Having plenty of impressions tells the algorithm to increase your posts, giving you more followers and likes. It could help you open the door to Instagram fame and potentially receive brand deals and other lucrative offers. Instagram impressions are a solid way to build your following.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        What Is A Good Impressions Rate On Instagram?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            1 to 3% is a great impressions rate for Instagram. Impressions are the number of instances in which your content was shown to a user/users.
                        </p>

                        <p>People often confuse this metric with Reach, but impressions are the times in which folks have viewed your content. If Instagram impressions surpass reach, the audience is viewing your content repeatedly. </p>
                        <p>Check for postings with high impressions-to-reach ratios to know what’s resonating with followers/viewers. </p>
                        <p>Impressions are measured by the number of times it’s clicked upon. One user could view your content many times.</p>
                        <p>Regardless of how many times somebody reviews your content, the impressions represent the total count of times the content is displayed. Meanwhile, reach counts unique users.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Can I Buy Instagram Impressions For Multiple Posts?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            Yes, we will gladly help you get impressions for each post. You can submit the post URLs you’d like to be used, and we will use those links accordingly. The impressions will be spread evenly among the posts you’ve supplied.   </p>
                        <p>The Instagram impressions our team provides will significantly improve your engagement and help you grow an even bigger reach. </p>
                        <p>Don’t wait. Purchase your Instagram impressions from our team today, and let the numbers speak for themselves. Our prices are competitive, and we never will ask for your password or sensitive info.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Can I Buy Instagram Impressions Service For A Private Account?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Unfortunately, we can only deliver our impressions to people who have public accounts on Instagram.
                        </p>
                        <p>Our team does not want your login credentials or other identifying information for many reasons; firstly, it’s not right and secondly, it violates Instagram Terms of Service to log into someone’s account without permission.</p>
                        <p>Our team prides itself on doing things by the book. So, the more impressions you get are real and from active Instagram users. You'll get real Instagram insights for your account. </p>
                        <p>Once our team has completed the order, you’re free to set your account back to private. We work quickly, so don’t worry about leaving it open- we won’t take long to fill the order.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        Can I Make My Account Private After Buying Instagram Impressions Service?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            Yes. You can make your Instagram account private once you’ve purchased the Instagram impressions, and they have been delivered to you. Your impressions of the post won’t be affected. </p>
                        <p>You have to wait until we’ve delivered the entirety of your impressions because making your account private before we are completely done with your order will pause it.
                        </p>
                        <p>Some of our buyers want to keep their accounts private while buying impressions and that’s OK- we just need it to be open for the duration of your order because we never ask for private account details or sensitive data while fulfilling orders.</p>
                        <p>Social Zingers offers other services also like <a href="https://www.socialzinger.com/buy-instagram-followers">buy Instagram followers</a> , buy Instagram likes, <a href="https://www.socialzinger.com/buy-instagram-comments">buy Instagram Comments</a> , and many more. </p>
                    </div>
                </Accordion.Collapse>
            </div>



        </Accordion>
    );
};

export default AccordionV16;
