import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV2 = () => {
  const [toggle, setToggle] = useState(1);
  return (
    <Accordion
      className="accordion"
      id="generalFAQ"
      defaultActiveKey="collapseOne"
    >
      <div className="accordion-item">
        <div className="accordion-header">
          <Accordion.Toggle
            as="h3"
            eventKey="collapseOne"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 1 ? 0 : 1)}
            aria-expanded={toggle === 1 ? "true" : "false"}
          >
            Why Should I Buy Instagram Followers?
          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapseOne">
          <div className="accordion-body">
            <p>
              You can have the best marketing strategy, but if your accounts get no exposure, then the strategy will go in vain. Whether you are a startup or an established business, you want your account to be ranked as an influencer account. And this isn't possible if you don't have thousands of Instagram followers showing on the main page.
            </p>
            <p>Remember that an account with 100 followers won’t beat the clutter on Instagram. It would help if you had thousands of Instagram followers to stand out from the crowd and make an impression on the audience. Your page views and engagement will increase drastically if you buy Instagram followers. This will help you reach the target audience and bring more customers to the spending fold.
            </p>
            <p>Gone are the days when businesses could flourish without having a prominent social media presence. Now, if you want your business to stay profitable in the long run, then you must tap the market on Instagram. Also, when you see <b>how many followers</b> and likes others have; that's the only way to see how reputable or influential an Insta account is.
            </p>
            <p>This sounds appetizing, but gaining followers on Instagram is no joke. Therefore, the best way to gain followers is to buy them. Your account will get all the limelight it deserves, and you will make bucks.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingTwo">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse2"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 2 ? 0 : 2)}
            aria-expanded={toggle === 2 ? "true" : "false"}
          >
            Is It Safe To Buy Instagram Followers?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse2">
          <div className="accordion-body">
            <p>
              Answering this question is tricky because your account might or might not get banned from buying Instagram followers. Here’s the catch! If you buy low-quality Instagram followers from a questionable company, your account might get flagged or banned. This happens because such companies bring inactive accounts to your page that damage your account’s reputation.

            </p>
            <p>Such companies have little knowledge about Instagram's algorithm, so they try to attract random accounts to get the job done. However, they don't understand the purpose behind buying followers. The actual purpose is not just to show numbers but to get exposure; your account will only get exposure if real and active accounts follow it.
            </p>
            <p>In contrast, if you buy Instagram followers from a reputable company that understands the platform’s algorithm, then you don’t have to worry about your account getting banned. An experienced and renowned company will ensure that it doesn’t violate Instagram's terms while attracting more followers to your accounts.
            </p>
            <p>Make sure you don’t fall for companies offering cheap Instagram followers and claim to get you the most Instagram followers. Such companies aren't trustworthy; all they do is get your account shadowbanned. So, be wise when choosing the package and the company.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingThree">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse3"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 3 ? 0 : 3)}
            aria-expanded={toggle === 3 ? "true" : "false"}
          >
            What’s The Quality Of Social Zinger’s Followers?
          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse3">
          <div className="accordion-body">
            <p>
              Social Zinger is all about quality where you can buy real Instagram followers. The company only delivers real Instagram followers to ensure that your account gets the right exposure and engagement. Social Zinger won’t get you bots or scam accounts, so your account is safe from being banned.
            </p>
            <p>
              You should remember that Instagram deletes any bots or scam accounts. So, there’s no purpose behind gaining fake accounts as followers.
            </p>
            <p>
              Moreover, Instagram gives exposure to accounts that are followed by real people. So, the platform’s algorithm will keep your account afloat after you get real and active followers to follow you.
            </p>
            <p>
              Social Zinger has hundreds of satisfied customers who are making millions because of their flourished active Instagram followers. All you have to do is buy the right package from Social Zinger and wait for your followers to explode in numbers. Make sure you read customer reviews to gain clarity. Many reviews have appreciated the company's commitment to offering real followers, timely services, and charging affordable rates.
            </p>
            <p>
              Social Zinger is one of the best sites to buy Instagram followers because it offers high-quality services, is affordable, and has prompt customer support service. This means you can reach out to the help desk if there's an issue with the quality or quantity of followers.
            </p>
            <p>
              The company is highly transparent, so you can check packages and read terms before paying for anything. You can also contact the customer support team if you are caught on any terms.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingFour">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse4"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 4 ? 0 : 4)}
            aria-expanded={toggle === 4 ? "true" : "false"}
          >
            When Can I Expect The Delivery After I Buy Instagram Followers?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse4">
          <div className="accordion-body">
            <p>
              You will see a massive increase in your Instagram followers in the blink of an eye! Social Zinger offers quick results to ensure that you don’t have to wait. You must remember that more and more people are joining Instagram every day. Along with this, there's increased competition with new businesses entering the platform. Therefore, you can only benefit from it, if you get them at the right time with organic followers.
            </p>
            <p>
              Social Zinger immediately starts working on your account after you select the package. So, it won’t take more than a few minutes before you notice an increase in the number of Instagram followers.
            </p>
            <p>
              The company mentions that it won’t make you wait for days or weeks before you can enjoy followers. Instead, you can have the desired number of accounts following you in less than 10 minutes.
            </p>
            <p>
              Social Zinger ensures that the growth of your account looks organic. Therefore, it doesn’t increase the number of followers all at once. This means your followers will increase gradually, so it doesn’t get caught up by the algorithm and doesn’t look suspicious to people.
            </p>
            <p>
              So, if you buy a package from Social Zinger , you don’t have to worry about waiting too long or any negative consequences because the company has it all covered. You can read customer reviews to learn more about the speed of services at Social Zinger. The company enjoys returning customers because it is affordable and quick in bringing Instagram followers with no fake Instagram followers.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
      <div className="accordion-item">
        <div className="accordion-header" id="headingFive">
          <Accordion.Toggle
            as="h3"
            eventKey="collapse5"
            className="accordion-button"
            type="button"
            onClick={() => setToggle(toggle === 5 ? 0 : 5)}
            aria-expanded={toggle === 5 ? "true" : "false"}
          >
            Which Package Should I Use?

          </Accordion.Toggle>
        </div>
        <Accordion.Collapse eventKey="collapse5">
          <div className="accordion-body">
            <p>
              Social Zinger offers multiple packages to meet your varying requirements like buy followers, buy instagram likes and <a href="https://www.socialzinger.com/buy-instagram-views">buy instagram views</a>. These different packages fit each brand’s unique requirements and allow them to gain the right number of high-quality followers.
            </p>
            <p>
              If you are a beginner, we recommend starting with our small packages. This will help you understand how Social Zinger  works and help you decide if you want to continue with us.
            </p>
            <p>
              Our smallest package is a treat for young businesses because you can enjoy 100 followers, instant delivery, and 24/7 customer support for less than $5. These features and price point ensures that everyone is able to buy the package and give a boost to their Instagram account.
            </p>
            <p>
              In contrast, if you have an established account but want to add more credibility, then you should opt for larger packages. These packages are apt for businesses planning to go big. You can choose from different packages that offer 5,000, 10,000, or 25,000 Instagram followers.
            </p>
            <p>
              We at Social Zinger offer a lot of choices to users. So, the ball is in your court. You can choose the package you want and get instant delivery without worrying about fake followers. You must remember that all these packages come with 24/7 customer support so that you can contact us anytime you need assistance.
            </p>
            <p>
              Lastly, we guarantee instant delivery because we understand and value the importance of time. So, you won't have to wait longer to buy followers and to see wonders for your Instagram account. You can also <a href="https://www.socialzinger.com/buy-instagram-likes">buy instagram likes</a> and views from Social Zinger.
            </p>
          </div>
        </Accordion.Collapse>
      </div>
    </Accordion>
  );
};

export default AccordionV2;
