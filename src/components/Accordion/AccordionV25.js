import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV25 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can You Buy Twitter Retweets?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>Yes, you can buy Twitter retweets! You should consider doing so because more folks can see your content and thus your profile increases its visibility.
                        </p>
                        <p>Buying Twitter retweets is a smart idea. You can get higher numbers of organic retweets from new people and followers. When a user swipes by over your Timeline and notices an interesting tweet, they may stop to read it and then skip it if it has zero retweets. </p>
                        <p>But, if they notice the tweet is packed with retweets, that user may decide it’s worth that effort. Therefore, you gain plenty of followers, likes, and retweets just by purchasing retweets to that single tweet and making it a trend.</p>
                        <p>Purchasing Twitter retweets also offers you more interaction overall. Retweets help your Tweet appear interesting and active. A Tweet with more interaction is viewed as being important by your audience.</p>
                        <p>Twitter will help your Tweets show at the top of a user’s homepage, listing it as a “suggested Tweet.” You can reach more users when you buy Twitter retweets, which then increases revenue.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Do You Get Real Retweets?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>You can purchase real retweets with our service. We can provide retweets in many quantities. Feel free to ask for any amount you want, our goal is to make it easy to buy Twitter retweets.</p>

                        <p> If you’d like to know how to get retweets organically, there are a few ways to do this.</p>
                        <ul>
                            <li>Put links into your tweets</li>
                            <li> Don’t use excessive hashtags</li>
                            <li> Don’t tweet too much in succession, take a break</li>
                            <li> Use GIFs in your tweets</li>
                            <li>  Follow Twitter users who make similar content and users you feel your audience will like.</li>
                            <li>  Post when your target audience is most active- for example, if they’re most active at 6 pm, post at that time.
                            </li>
                        </ul>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Does Buying Retweets Help You?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>Yes. Buy Twitter Retweets to earn higher engagement. Twitter is the world’s most famous microblogging platform. The site serves as a news center, working in real-time to deliver the latest to people around the world, and to do so quickly. </p>
                        <p>You can write up to 140 characters and it’s an efficient communication tool you can use with all your favorite devices. If you wish to gain recognition on this platform, you must buy Twitter retweets. Buying Twitter retweets is easy and can help your content shine. </p>
                        <p>Retweeting allows you to share a tweet you find engaging with others on your Twitter account. It’s a lot like Instagram’s Repost feature.  </p>
                        <p>You can turn off that feature if you wish, but then others cannot share your great content. If you want to be popular on Twitter, you’ve got to leave that feature open. You can find natural ways of getting Retweets, but our service makes it easy and fast to get what you need. Our easy buy Twitter retweets process helps you show off your content to thousands and earn more interaction.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Much Does It Cost to Buy 1000 Twitter Retweets?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>It costs only $26 to get 1000 high-quality retweets with our service. It’s a small price to pay when you think about what those retweets can do for your good or service. </p>
                        <p>We use only real users to carry out our work, so that way you can watch more people view your tweets and increase your popularity. You can buy real Twitter retweets just by using this page.
                        </p>
                        <p>Buying Twitter retweets is easy to do. You simply have to follow our five-step guide:</p>
                        <ul>
                            <li>Choose the amount of Twitter retweets you’d like to order.</li>
                            <li>Paste the link to your Tweet to the provided box. </li>
                            <li>Add the order to your cart, click “buy now, "and then go to the payment screen.</li>
                            <li>Pay for your order. </li>
                            <li>Sit back and watch the retweets come in. </li>
                        </ul>
                        <p>You’ll notice the retweets you purchased being visible on your tweet ASAP. We hope using our services is enjoyable and easy for you, but if it isn’t, please contact us so we can help you anytime. Social Zinger not just provides you with twitter retweets but you can also <a href="https://www.socialzinger.com/buy-instagram-followers">buy twitter followers</a>  as well.
                        </p>
                        <p><a href="https://www.socialzinger.com/buy-twitter-likes">Buy twitter likes</a> , twitter comments, and Instagram followers are just a few of the services that Social Zinger provides for social media platforms on numerous social media networks to boost your social media marketing.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV25;
