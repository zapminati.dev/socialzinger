import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV23 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Is It Safe to Buy Twitter Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            With our team, yes! You can buy Twitter likes without fear that you’ll be overstepping the boundaries of Twitter and its Terms of Service.
                        </p>

                        <p>If you’d like to get more interaction for your posts, you can buy Twitter likes to make it happen. When other users see the engagement on your Tweet, they are far more likely to engage with that content. </p>
                        <p>Our delivery is fast. We begin processing the order as soon as you place it, and we make it easy to buy Twitter likes. </p>
                        <p>The majority of orders are processed in an hour or less. We deliver the likes in a slow but sure format, that way it looks organic. </p>
                        <p>Moreover, our team uses real Twitter users to make it happen. That way, your account won’t be flagged for misuse of the platform- it’s real people and not bots making those likes. There’s never been a better time to buy Twitter likes.</p>
                        <p>Twitter likes make your account greatly accessible via algorithm suggestions. Plus, users will be surprised and interested in your huge like count.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Where Can I Buy Real Twitter Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>Are you ready to buy Twitter likes? Great, we make it easy. Start by going to Social Zinger. Once you’re there, choose our Twitter menu. On that menu, select the number of likes you’d like to purchase. </p>

                        <p>We have pre-made packages available in round numbers. You can buy 100, 500, or 1000 likes. We have smaller and larger counts available, too.</p>
                        <p>We can also create a custom package of likes for you. We strive to make it easy to buy Twitter likes.</p>
                        <p>Once you’ve selected the number of likes you want, pay for your order. Then, provide the link in the box we provide.  </p>
                        <p>Once you’ve done that, sit back and watch those likes roll in. Feel free to return to us and buy Twitter likes whenever you need them again.</p>
                        <p>Remember, we offer 24-hour support. When you have a question about how to buy Twitter likes, we can help you.
                        </p>
                        <p>Quick Summary: </p>
                        <ul>
                            <li>Add the Likes you want to your cart.</li>
                            <li>Paste the URL to the box provided.</li>
                            <li>Pay for your order.</li>
                            <li>Wait for the likes to roll in!</li>
                        </ul>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How To Get Fake Likes on Twitter?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>Getting fake likes is easy. Go to a “buy Twitter likes” website selling fake bot accounts (they’ll sometimes call them ‘Regular’ followers) and order those followers. Those are bots and they might get your account flagged.</p>
                        <p>We don’t recommend this method, as we don’t want to see your account banned. These followers often come with a cheaper price tag, but if you’ve worked hard to create a quality Twitter account, you don’t want to lose it. </p>
                        <p>The best way to get “fake likes” (as in, Likes you paid for) is to use Social Zinger. We use real users only to produce the engagement your Twitter profile needs. Buy Twitter likes with Social Zinger to ensure you get the likes you want of the highest quality. You can also <a href="https://www.socialzinger.com/buy-twitter-views">buy Twitter Views</a>  from Social Zinger.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Do You Buy Followers and Likes on Twitter?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>Buying followers is easy, as is the process to buy Twitter likes. You can begin by going to the Social Zinger website, where we have packages of Twitter likes and followers waiting for you. </p>
                        <p>You can check out our packages available for different social media services and browse the amounts that make sense for you and your budget.</p>
                        <p>Once you’ve selected the packages you want, paste in your account’s URL/post URL, and our team will get to work immediately delivering the followers and likes once the order has been paid.  </p>
                        <p>We make it easy to <a href="https://www.socialzinger.com/buy-twitter-comments">buy Twitter likes</a> and to buy Twitter comments. That's exactly where social proof can be useful, but only if it is wholly genuine.</p>
                        <p>If you have any questions or concerns about your orders, speak with our team anytime you like. We work 24/7 to help our customers and ensure satisfaction with your order.</p>
                        <p>Don’t hesitate. Let your social media accounts enjoy greater visibility when you buy Twitter likes with us.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV23;
