import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV20 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Why Buy Twitter Comments?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Building a brand and getting the word out regarding your good or service begins with obtaining an abundance of targeted followers. Twitter is a crowded, saturated website.
                        </p>

                        <p>You’ve got stiff competition since there are thousands of other small businesses out there also wishing to be unique, stand out, and garner a following.</p>
                        <p>And since comments pique interest and lead to followers, you’ve got to have interesting comments to foster engagement and get more followers.</p>
                        <p>It’s all in how the Twitter algorithm rank signals work. </p>
                        <p>That means Twitter prioritizes tweets based on how users in your network interact with them.</p>
                        <p>If you’re following a specific topic, Twitter will check out how many folks are replying, retweeting, and liking tweets regarding that topic. Engagement with the tweet matters, so leave the heavy lifting to us!
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Is Buying Twitter Comments Safe?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            Yes! Using our service to buy Twitter comments won’t leave you awake at night, wondering if your account will get banned or flagged for violating the Twitter terms of service.
                        </p>

                        <p>We use real people to provide the comments, therefore, there’s nothing to worry about since it's real users commenting on your Tweets.
                        </p>
                        <p>We also do not ask for any sensitive account information. Lesser websites will ask you for identifying information (read: these are scam artists) such as your password to “fulfill your order.” Don’t believe them. Social Zinger only needs your post URL so we can deliver the comments you’ve purchased.</p>
                        <p>To buy Twitter comments is to help your business grow. Twitter is a popular online medium that can help your product or service get the recognition it deserves. You can let everyone know what your content entails, and help it become popular. </p>
                        <p>When you have lots of interaction with a post, users are more apt to click upon it and see what it’s about. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Why Social Zinger?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>One of the primary reasons for using us to buy Twitter comments from Social Zinger is that we start working instantly. Within the hour, you’ll see engagement happening on your tweets in the form of comments.</p>
                        <p>We use real accounts to make these comments happen. We do not use bots or automated programs to do our work when you buy Twitter comments, as that violates Twitter’s Terms of Service. </p>
                        <p>Finally, we provide the best pricing to our customers. The more you buy, the less per comment you pay. Consider buying in bulk when it’s time to buy Twitter comments.</p>
                        <p>We also encourage you to check out our “Reviews” section, where you can see what previous customers had to say about our services. Take it from them; we are the real deal, and we deliver when you ask us to buy Twitter comments.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        How Much Does It Cost to Buy 1000 Twitter Followers?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Ready to buy Twitter comments? You can do exactly that when you work with our service. We charge $80 for 1000 comments.  </p>
                        <p>It’s a fair price for what you get- remember, these are real Twitter comments and not bots or fakes! Moreover, they are all positive and will help foster a positive opinion about your product or service.</p>
                        <p>If you want to buy Twitter followers instead of buying Twitter comments, you can do that also. </p>
                        <p>We have a follower purchase service with the same competitive pricing you’d expect from our buy Twitter comments service. It costs only $26 for 1000 followers from real accounts. </p>
                        <p>Like our buy Twitter comments, the followers begin interacting with your account within the hour after you buy them. </p>
                        <p>We have the followers trickle in slowly so that it appears natural and organic. You’ll get all the followers and perhaps a few more when your account gains further traction thanks to our service. </p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV20;
