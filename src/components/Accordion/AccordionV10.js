import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV10 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Why is YouTube good for businesses and brands?


                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            YouTube for business is meant to assist you in using YouTube to expand your clientele, expand your company, and establish your brand. Since most of the world watches videos on YouTube, so do your clients. For companies of all sizes, YouTube provides a convenient way for customers to find you on the internet when it matters, particularly if they're accessing your page via mobile devices.
                        </p>
                        <p>Users can get entertainment, knowledge, music, and much more on YouTube. Even those who are considering purchasing go there to investigate goods and services. This implies that you might find people looking for movies that are extremely comparable to what your company offers at the exact time they are searching. You can achieve that by watching YouTube video adverts.
                        </p>
                        <p>Because YouTube displays your videos to users interested in similar goods or services to yours, it is an excellent source of targeted traffic that is valuable to your company. This makes it simpler for you to grab their interest and convert them into paying subscribers or clients.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Why do people buy YouTube likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>
                            Advertising your brand through YouTube is essential for increasing your online presence, generating leads, creating product awareness, increasing conversion and traffic, and keeping up in a saturated and competitive market. Hence, people buy YouTube likes for numerous reasons, such as the ones we have listed below:
                        </p>
                        <h6 className="title mt-2">It increases the buyer’s YouTube channel's ranking.</h6>
                        <p>The number of views and engagement may impact your YouTube rating because consumers are often directed to popular material by YouTube's algorithm. The algorithm determining who should see your content considers user engagement, the volume of comments, views, and likes.
                        </p>
                        <p>Your content will rank higher if you boost the amount of YouTube views and channel interaction. </p>

                        <h6 className="title mt-2">It boosts the number of subscribers.  </h6>
                        <p>Buyers concentrate on gaining followers once they’ve established their YouTube channel. But if you're still new to YouTube, gaining subscribers may take some time. However, more people will readily find your channel if you increase the number of views on your YouTube videos.</p>
                        <h6 className="title mt-2">Building credibility on YouTube </h6>
                        <p>YouTube videos with a lot of likes are popular with viewers. Increasing the number of likes on your YouTube channel can help you establish credibility if it is still young. Your channel will increase as a result of this. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How does my account benefit from having YouTube likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>
                            Take any online media site, Instagram, Twitter, or YouTube. The number of followers or subscribers matters greatly. Subscribers or followers give credibility to your brand and online presence, as it results in higher likes which ensures the viewer that the services, products, or content you're offering is worth watching or buying.
                        </p>
                        <p>Even more so, it instills the belief that your brand, product, or content, is good enough to have a large number of following and likes. But getting so many subscribers is a tedious process, let alone getting the likes. Hence, buying YouTube likes is a quick way to get unnecessary matters solved when you deserve it all anyways. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Will YouTube block my account from buying likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>
                            Likes originate from a wide variety of persons and locations. Some likes are better than others. And likes come from various demographics, locations, and people. Hence, you are covered on all fronts if you buy YouTube likes from a credible source that sells real YouTube likes.
                        </p>
                        <p>Real and bot likes fall into two main types. Real likes are made by actual people who engage in a channel's content naturally. Bots are well bots and frequently attempt to impersonate human individuals. However, they are frequently detected by YouTube. Instead of bombarding your YouTube channel with several likes, when you purchase real promotional packages, you are luring actual people to watch your videos and engage with them (by clicking "like" or "subscribing"). The provider is not utilizing an automated system in this instance. They're merely inviting their community of YouTubers to visit and like or watch your content on your behalf.</p>
                        <p>You can be sure that nothing will ever happen to your package or your channel when you purchase a legitimate YouTube promo package. In other words, you won't have a sad tale to tell.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        Why are likes important on YouTube?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>
                            When it comes to exchanging visuals in the form of videos, no site is more popular than YouTube. YouTube is a social media platform, even though it is based on videos. Engagements are vital for this on YouTube, just like other social media platforms. The videos' likes are just as significant to the YouTube algorithm as their comments. Everyone wants to have videos that receive more likes. </p>

                        <p>One factor the algorithm considers when deciding which videos to promote is the number of YouTube likes. A video's likelihood of appearing in pertinent YouTube search results increases with the number of likes it receives. In this instance, the YouTube algorithm's viewpoint on likes is conditional. The YouTube algorithm's perspective on likes is conditional in this case. For instance, the presence of your video on the platform can improve if more likes are received more quickly. You can increase the prominence of your channel on YouTube by buying extra likes for it.  </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        How else can I increase my YouTube likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>
                            Follow these recommendations if you're interested in using YouTube videos to advertise your company to ensure they receive the most views and engagement possible.</p>
                        <h6 className="title mt-2">Make a title that grabs readers' attention.
                        </h6>
                        <p>The most important factor in someone's decision to watch a video is its title, so be sure to come up with one that will entice viewers. Make sure your core keyword is in the title and that it appropriately represents the video's content. Although titles on YouTube can have up to 100 characters, the ideal length is 60–70 letters, with spaces, as anything beyond is likely to be missed in search engine results. </p>

                        <h6 className="title mt-2">Add appropriate tags </h6>
                        <p>YouTube tags are keywords or brief phrases that educate YouTube about your video's subject matter and setting. They also serve as significant ranking elements for YouTube searches. Each video should include 5-8 tags, including the primary keywords and their variations, the overall category, and a mix of general and niche tags.</p>
                        <h6 className="title mt-2">Engage with your audience </h6>
                        <p>You should reciprocate if you ask visitors to interact with your video material. Watch the feedback from your videos' viewers, give rapid answers to queries, and participate in debates. Keeping the lines of communication open with your audience may increase engagement, your channel's reputation, website traffic and revenue. </p>
                        <h6 className="title mt-2">Share links to your YouTube channel on social media </h6>
                        <p>Although you can send a link to a YouTube video on other social networking platforms or use the video's social share buttons, it may be better to submit the video directly to the channel. Try both approaches to see which encourages more interaction.</p>
                        <h6 className="title mt-2">Use Hashtags </h6>
                        <p>Hashtags generally improve your video's searchability and visibility on social media platforms. You can use hashtags in the title and description of your video. Use hashtags carefully since too many can cause the video to appear less relevant in searches. YouTube may punish your video by ignoring all of the hashtags in your video description if you add more than 60, and YouTube may potentially remove the video. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSeven">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse7"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 7 ? 0 : 7)}
                        aria-expanded={toggle === 7 ? "true" : "false"}
                    >
                        What is the benefit of buying youtube likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse7">
                    <div className="accordion-body">
                        <p>
                            Increasing one's popularity on YouTube has practically turned into a business. Because of this, people put a lot of effort into their films to rank highly on search engines. To receive more comments and likes on their videos, people want to share them. Since YouTube features algorithms like most well-known websites like Instagram, it has frequently been required to Buy Youtube Likes.</p>
                        <p>These algorithms determine whether a person can move the rankings if their content is up to par. More people will like and share a video if the material is good. That will let the algorithm understand that a certain video is something people want to watch more, which will help it rank well on search engines. </p>
                        <p>By bringing in more viewers and traffic, which increases engagement and revenue, YouTube likes benefit people in numerous ways. The most popular video appears to have quality content in viewers' eyes. Everyone wants to view high-quality content, and getting plenty of likes will help you build that reputation. If one of your videos receives a lot of likes, you'll see that many other people will do the same because the more traffic a content receives, the more people will be interested in it. </p>
                        <p>This will guarantee the most effective customer traffic if you are a brand. Customers typically purchase things that seem reliable. Massive channels like reassuring clients that the product they purchase is high quality.</p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingEight">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse8"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 8 ? 0 : 8)}
                        aria-expanded={toggle === 8 ? "true" : "false"}
                    >
                        Why should I trust your services to buy youtube likes ?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse8">
                    <div className="accordion-body">
                        <p>
                            A lot of people are confused about whether they should trust YouTube likes. And rightly so. With so many sites and businesses selling YouTube likes, subscribers, and views, choosing one business over the other can be difficult. The important question to ask yourself is whether these businesses are selling real likes, views, and subscribers or selling fake bot users to you.</p>

                        <p>Many businesses are selling YouTube likes through fake bots. YouTube easily detects fake engagement in the form of views or likes and removes the fake bots from your account. When this happens, you lose the increased number of likes from your content, your credibility with your audience, and your ranking, you become flagged in YouTube's algorithm as suspicious, and of course, your money goes useless.  </p>
                        <p>Do not hear it from us; hear it from our real customers. You are welcome to review our highly verified positive reviews to ensure your YouTube channel's safety. And this is why you should trust us. With our verified user purchases, rankings, positive feedback, and authenticity guarantee, your YouTube channel and your reputation and growth are in safe hands. Along with likes you can <a href="https://www.socialzinger.com/buy-youtube-views">buy youtube views</a> and you can also <a href="https://www.socialzinger.com/buy-youtube-subscribers">buy youtube subscribers</a> at cheap rates.  </p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingNine">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse9"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 9 ? 0 : 9)}
                        aria-expanded={toggle === 9 ? "true" : "false"}
                    >
                        How to get paid on youtube for my videos?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse9">
                    <div className="accordion-body">
                        <p>
                            According to several websites, YouTubers can make between $0.01 and $0.03 each with AdSense, or an average of $0.18 per view. However, the money that YouTube.com will pay varies depending on several variables, including the number of views your movie has, how many clicks an advertisement gets, ad caliber, Adblockers, and video duration.  </p>
                        <p>You must first accumulate a balance of $100 or more from views to be paid by YouTube. To collect $5 for every 1,000 views, you must obtain 20,000 views. The YouTube Partner Program, which enables you to get compensated by adverts on your page, was developed by YouTube to make this possible. </p>
                        <p>If you want to join the YouTube Partner Program, you must own a minimum of 1,000 subscribers and have 4,000 hours of legitimate public viewing in the last 12 months. You must also accept the terms and conditions, own a Google AdSense account, and obtain approval and review. You can earn money from commercials as soon as you join the YouTube Partner Program. </p>
                        <p>There are two ways to do this: cost per 1000 views, known as CPM, or cost per click, known as CPC. With CPM, you may get money for every 1,000 views you receive, and with CPC, you can make money for every person who clicks on the advertisement displayed on your page or video.</p>
                        <p>It's vital to remember that when you use AdSense to display advertising, Google will pay you 68% of the earnings. As a result, if a video earns $1,000 in AdSense income, you will receive $680.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV10;
