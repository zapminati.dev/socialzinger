import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV22 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can I Buy Shares on Instagram?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>
                            Yes! You can buy Instagram shares. To buy Instagram shares is as easy as 1-2-3 and will help your account reach more people. You begin by checking out our website where you will select a package that suits you.
                        </p>

                        <p>Next, you will provide the links for your posts so we can send our users out to get the work done.
                        </p>
                        <p>Finally, you may sit back and wait for the shares to come in. Our shares take about an hour to arrive, so please be patient. If you have questions, our team is ready to help 24/7/365.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Where Can I Find Instagram Shares?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>Shares are made by clicking on the paper plane icon. To see who has shared your post on Instagram is easy, but you can only see figures and not the specific users.
                        </p>

                        <p>To check out your Instagram insights, click on your profile and go to where your photos are. Then click on a post and view it. You will see a “View Insights” button at the bottom left of that photo.</p>
                        <p>You can then view the interface where all insights will appear. The paper plane tells you the shares. It’s all the same for every post type- videos, photos, etc.</p>
                        <p>You can also see post-re-shares on Instagram, the pathway is similar. You can see users who’ve shared photos of yours on their respective stories. If a person’s account is private, you cannot see this. If your story is posted on others’ posts, they could see it also. </p>
                        <p>To see who has shared your post on Stories: get to your profile, open a recent post, and click on the three dots at the post’s upper-right corner.</p>
                        <p>“View Story Re-Shares” will be available and can tell you if any user has shared your posts on their story during the past 24 hours.</p>
                        <p>Don’t be filled with regret when there are no shares. <a href="https://www.socialzinger.com/buy-instagram-followers"> Buy Instagram shares</a> today. You can also buy Instagram Followers from Social Zinger.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How Do Shares Work On Instagram?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>The paper plane icon represents shares. You can share posts using direct messaging, or with friends on their Instagram Story.</p>
                        <p>Shareable posts may be anything posted to Instagram, whether it is a cute video or a news story, or a cool pair of jeans somebody wishes to share with their buddies. </p>
                        <p>Why do shares matter, and why should you buy Instagram shares? Shares indicate users approve of your post. Not only does it show that users love your content, but they also send it to friends to show it off. </p>
                        <p>When users share posts in this manner, it helps reach new audiences and helps you, the content creator, garner more of a following. You can also then increase post impressions by <a href="https://www.socialzinger.com/buy-instagram-views">buying Instagram Views</a> as well.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Is Buying Instagram Post Shares Safe?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>With our team, it is completely safe to buy Instagram shares. We never ask for passwords or account information. We never violate the Terms of Service for Instagram users. We are thousands of account holders’ go-to when they want to buy Instagram shares. </p>
                        <p>We do not use low-quality bot programs to make your shares happen. When you buy Instagram shares with us, you can rest assured your account won’t be banned or flagged. Real people provide our shares.</p>
                        <p>Our team has a deep knowledge of the algorithm for Instagram stories users, ergo, we do not use random accounts to fulfill orders when you buy Instagram shares. We have real and active accounts that do the work. </p>
                        <p>It’s best to buy with us instead of shopping around. Not only can you count on the lowest prices to buy Instagram shares, but you can also breathe easy knowing that real users are the ones doing the sharing. You’ll get more followers this way, too.  </p>
                        <p>Don’t accept a low-quality, cheap social media services substitute. When it’s time to buy Instagram shares, our customer service team can help.</p>
                    </div>
                </Accordion.Collapse>
            </div>


        </Accordion>
    );
};

export default AccordionV22;
