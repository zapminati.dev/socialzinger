import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV26 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Can You Buy Twitter Impressions?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>Yes. Social Zinger provides a great deal when you’re ready to buy Twitter impressions. We deliver genuine reactions from real users that come to the platform each day, and who can deliver a positive feeling about your profile’s statistics.  </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        How Much Is a Twitter Impression Worth?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>With us, Twitter impressions are inexpensive. We can provide 100 impressions for $.59, which is a small price to pay when you need to make your content known. </p>

                        <p> For the monetary value of the impression itself, Twitter currently says that $3.50 is the average cost per impression (CPM). </p>
                        <p>So, if you were posting an ad on Twitter to garner impressions, and you wish to reach a thousand impressions, the cost would be $3500. If your tweet had 2000 impressions, it would be worth $7,000.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        Are 100 Impressions Good on Twitter?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>It’s a good question to pose, and the answer isn’t clear-cut. It will depend. There’s no specific figure that indicates a “bad or good” impression count. However, we can choose a range of what makes Tweet impressions good.</p>
                        <p>If you can average over 20 impressions over your followers, that’s a good thing. It means 20% of your followers have seen the Tweet. It doesn’t even have to be your followers.</p>
                        <p>So, buy 100 impressions- it’s a great thing and helps make your content shine. We can do more than that when you are ready, so count on us when you need to buy Twitter impressions.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Do Impressions Really Matter on Twitter?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>Absolutely, yes! Twitter impressions in greater numbers mean greater engagement. Twitter impressions indicate how many times other people have viewed your tweet. 600 impressions mean that 600 people have viewed your Tweet.  </p>
                        <p>This metric doesn’t mean that 600 unique people have seen your Tweet. There may have been 100 people who checked out your Tweet 6 times. While that’s pretty good, you know you can do better, and the solution is to buy Twitter impressions.</p>
                        <p>Why should a person buy Twitter impressions? There are many reasons, and if you want to do well in digital marketing, then Twitter is essential to success. Keeping impressions high and moving to buy Twitter impressions can help. </p>
                        <p>Impressions indicate how many times your post was viewed. Regardless, the info is nothing unless you can understand the measure of your reach. You might change your account to Business so you can view such metrics.
                        </p>
                        <p>Not sure about Twitter reach vs Impressions? We understand. If you want to buy Twitter impressions, you’ll need this information. </p>
                        <p>Impressions indicate to you the number of times your Tweet was shown, regardless of whether it was clicked. </p>
                        <p>Reach, meanwhile, indicates how many times people saw your content.</p>
                        <p>Let’s assume your tweet’s reach measured a value of 600 (that’s how many people viewed the tweet) but your impressions measure 1800. That means your Tweet was so attractive, they saw it three times.
                        </p>
                        <p>Twitter’s algorithm assumes that since the tweet is being viewed more than once, it’s potentially powerful content.</p>
                        <p>It’s all the more reason to buy Twitter impressions because your Tweet will get promoted on Twitter Suggestions and more people can discover your brand.  You can also <a href="https://www.socialzinger.com/buy-twitter-views"> buy Twitter views</a> to enhance your Twitter reach. </p>
                        <p>You can increase organic growth this way, so buy Twitter impressions today.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFive">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse5"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 5 ? 0 : 5)}
                        aria-expanded={toggle === 5 ? "true" : "false"}
                    >
                        From Where Should You Buy Twitter Impressions?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse5">
                    <div className="accordion-body">
                        <p>Social Zinger is the best place to procure your Twitter impressions. We’ve got the best deals when it’s time to buy Twitter impressions cheaply.  </p>
                        <p>We take care to use real Twitter users who will interact with your content like real users. We do not use bot accounts to carry out our work. </p>
                        <p>We also ensure your purchase experience of buying impressions is safe and secure. Social Zinger also offers to <a href="https://www.socialzinger.com/buy-twitter-followers">buy Twitter followers</a>, likes, and views for your Twitter account. Our website is encrypted so your payment info stays safe. We also make the process easy, so if you’ve never done it before, don’t worry.  </p>
                    </div>
                </Accordion.Collapse>
            </div>

        </Accordion>
    );
};

export default AccordionV26;
