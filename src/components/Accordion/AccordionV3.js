import { useState } from "react";
import { Accordion } from "react-bootstrap";

const AccordionV3 = () => {
    const [toggle, setToggle] = useState(1);
    return (
        <Accordion
            className="accordion"
            id="generalFAQ"
            defaultActiveKey="collapseOne"
        >
            <div className="accordion-item">
                <div className="accordion-header">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapseOne"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 1 ? 0 : 1)}
                        aria-expanded={toggle === 1 ? "true" : "false"}
                    >
                        Why Should I Buy Instagram Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapseOne">
                    <div className="accordion-body">
                        <p>Insta likes aren't just a number. You must understand that the number of likes is linked to Instagram's algorithm. Hence, they affect the engagement and exposure your account is getting. </p>
                        <p>You can have an aesthetic social media account and a great business idea, but if your posts don't have enough likes, then they sadly won't reach a wider audience. Therefore, you must buy IG likes from social zinger because it helps accounts get more recognition, Instagram followers, and eventually more conversions. </p>
                        <p>Along with this, likes also act as social proof. An account with more likes is likely to be trusted more by people than an account that has only a bunch of likes. Buying likes is the perfect way to boost organic growth, encourage engagement, and reach the infamous explore page on Instagram. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingTwo">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse2"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 2 ? 0 : 2)}
                        aria-expanded={toggle === 2 ? "true" : "false"}
                    >
                        Why Buy Instagram Likes From Social Zinger?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse2">
                    <div className="accordion-body">
                        <p>We are comparatively new in the market, but the team behind Social Zinger has decades of experience with social media. Our team knows what works best for you because we have been doing this for years now.
                        </p>
                        <p>Many people opt for our Instagram services because we offer instant likes from real people. This allows your account to grow and not get banned from Instagram. Moreover, we charge affordable rates, which makes it easier for new brands and small influencers to afford our services.</p>
                        <p>In addition, we offer different packages to cater to the differing needs of customers. You can choose the package that resonates with the number of likes you want and fits your budget. This way, you won't be forced to pick a package that is way out of your budget.</p>
                        <p>Each of our packages comes with instant delivery and 24/7 customer support. This allows our customers to enjoy the perks of Instagram likes without waiting a second. Moreover, the availability of customer support helps customers get their issues resolved.</p>
                        <p>You can read our customer reviews to get an idea about the quality of service we offer. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingThree">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse3"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 3 ? 0 : 3)}
                        aria-expanded={toggle === 3 ? "true" : "false"}
                    >
                        How Long Do I Have To Wait Until I Get My Instagram Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse3">
                    <div className="accordion-body">
                        <p>We don’t waste your time, which means you won’t have to wait for long to experience the turnaround. Our team goes by the mantra that Instagram moves at the speed of sonic, and so do we!
                        </p>
                        <p>With Social Zinger Instagram likes, you will notice a difference in the likes on your Instagram posts in mere minutes. We deliver your likes from real Instagram accounts quicker than any other site. Moreover, we don't use bots to boost your account, which makes a huge difference. </p>
                        <p>You must understand that Instagram's algorithm is very dense and difficult to take over. Many sites get likes from bots, but they are of no good, and Instagram's algorithm can easily detect bots. Therefore, we guarantee that we only get likes from real and active Instagram real users. </p>
                        <p>If you want to get Instagram likes quickly, you must log in to your Instagram account, select the Instagram photos or videos you want to boost, and enter your credit card information. Once this is done, you will experience a herd of likes within a few minutes. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingFour">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse4"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 4 ? 0 : 4)}
                        aria-expanded={toggle === 4 ? "true" : "false"}
                    >
                        Is It Safe To Buy Instagram Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse4">
                    <div className="accordion-body">
                        <p>It is safe to buy Instagram likes as long as you buy from a reliable, trustworthy, and experienced source. Many sites claim to offer high-end services without even knowing the gist of the Instagram algorithm.
                        </p>
                        <p>Social Zinger offers quick Instagram Likes starting from $2.5. They are safe and reliable. </p>
                    </div>
                </Accordion.Collapse>
            </div>

            <div className="accordion-item">
                <div className="accordion-header" id="headingSix">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse6"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 6 ? 0 : 6)}
                        aria-expanded={toggle === 6 ? "true" : "false"}
                    >
                        How Does Buying Instagram Likes Work?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse6">
                    <div className="accordion-body">
                        <p>We have made it easy to buy Instagram likes. All you have to do is choose the package you want to enjoy. We offer multiple packages to ensure convenience for different kinds of customers. Once you choose the package, you have to go to your Instagram, choose a picture or video that you want to boost, and enter your credit card details.
                        </p>
                        <p>We don’t ask for email or passwords to respect privacy and security. Moreover, we offer instant delivery, so customers can reap the benefits without waiting for more than 24 hours.</p>
                        <p>Once you enter your credit card details, you will see a difference in the post. However, we provide likes gradually, so you must wait before your post gets the chosen number of likes.</p>
                        <p>We prefer to be methodical in how likes are rolled out, so we consider the time zone and activity level on Instagram before rolling out likes on your post. This saves your account from being penalized or looking suspicious. </p>
                        <p>Our team is very big on satisfaction. Therefore, our customer support is live 24/7. You can contact them in case of an emergency. You can also reach out to them to discuss hindrances in the post.</p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingSeven">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse7"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 7 ? 0 : 7)}
                        aria-expanded={toggle === 7 ? "true" : "false"}
                    >
                        What Are The Benefits Of Buying Instagram Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse7">
                    <div className="accordion-body">
                        <p>People are going crazy over Insta likes because they offer huge benefits. Instagram’s algorithm rewards accounts that have more likes with more exposure. This means your account will feature on more screens if it enjoys more likes. Similarly, the chances of your post making it to the explore section are more if you have a solid number of likes.
                        </p>
                        <p>Moreover, Insta likes to act as legitimacy for viewers visiting your posts. If your posts have more likes, then viewers are more likely to trust your brand. You can also <a href="https://www.socialzinger.com/buy-instagram-views">buy Instagram views</a> Therefore, you should buy Instagram likes if you want more viewership, exposure, and conversions.
                        </p>
                        <p>Every day, more and more businesses are entering Instagram. However, they struggle to bag a customer because their account gets buried in the ocean of accounts with more likes and following. So, if you want your account highlighted, you must buy Instagram likes.</p>
                        <p>Remember that purchasing IG likes is the path to success and the key to Instagram's strategy. So, if you want to become an Instagram influencer, then there's no better way to gain ranking for your page than to buy Instagram likes. </p>
                    </div>
                </Accordion.Collapse>
            </div>
            <div className="accordion-item">
                <div className="accordion-header" id="headingEight">
                    <Accordion.Toggle
                        as="h3"
                        eventKey="collapse8"
                        className="accordion-button"
                        type="button"
                        onClick={() => setToggle(toggle === 8 ? 0 : 8)}
                        aria-expanded={toggle === 8 ? "true" : "false"}
                    >
                        Can I Buy Real Instagram Likes?

                    </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="collapse8">
                    <div className="accordion-body">
                        <p>Social Zinger offers real IG likes to our customers with multiple picture options because we want your account to enjoy real activity, engagement, and exposure.</p>
                        <p>Social Zinger only gets likes from real Instagram accounts to ensure value for money. Moreover, we offer 24/7 customer support, so you can report an issue if you think more bots have started following you.</p>
                        <p>We are sure that you won’t face this problem, but even if you do, our team will be on its toes to solve the issue for you. We are a highly skilled site because we offer real Instagram likes, exposure, and engagement to your account. Avoiding fake accounts.</p>
                        <p>In addition, we offer instant delivery to help your account gain a boost at its earliest. You should buy our beginner's package if you are interested in buying Instagram likes. You can also <a href="https://www.socialzinger.com/buy-instagram-followers">buy instagram followers</a> and instagram views to enhance your social media presence, We are sure you won’t be disappointed with our social media services.
                        </p>
                    </div>
                </Accordion.Collapse>
            </div>
        </Accordion>
    );
};

export default AccordionV3;
