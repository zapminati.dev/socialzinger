import mongoose from "mongoose";
import validator from "validator";

const ordersSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Name not mentioned"],
    },
    email:{
        type: String,
        required: [true, "Email not mentioned"],
        validate: [validator.isEmail, "Please enter a vailid/unique email"]
    },
    serviceName:{
        type: String,
        required: [true, "Select the service"],
    },
    url:{
        type: String,
        required: [true, "Enter the link"],
        validate: [validator.isURL, "Please enter an vailid link."]
    },
    quantity:{
        type: Number,
        required: [true, "Select the quantity"]
    },
    amount:{
        type: Number,
        required: [true, "Select the amount"]
    },
    payment: {
        transaction_id: {
            type: String,
            default: "",
        },
        status: {
            type: String,
            default: "",
        },
    },
    billingAddress: {
        state: {
            type: String,
            default: "",
        },
        zipcode: {
            type: Number,
            default: "",
        },
    },
    orderStatus: {
        type: String,
        default: "pending",
    },
    optInEmail: {
        type: Boolean,
        default: true
    },
    
}, {timestamps: true});


module.exports = mongoose.models.Orders || mongoose.model("Orders", ordersSchema, "Orders");