import mongoose from "mongoose";
import validator from "validator";
import crypto from "crypto"

const freemiumSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, "Name not mentioned"],
        maxLength: [30, "Name can not exceed 30 characters"],
        minLength: [4, "Name should have more that 4 characters"]
    },
    email:{
        type: String,
        required: [true, "Email not mentioned"],
        index: true,
        unique: true,
        validate: [validator.isEmail, "Please enter a vailid/unique email"]
    },
    service:{
        type: String,
        required: [true, "Select the service"],
    },
    // serviceType:{
    //     type: String,
    //     required: [true, "Select the service type"]
    // },
    link:{
        type: String,
        required: [true, "Enter the link"],
        validate: [validator.isURL, "Please enter an vailid link."]
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    emailVerificationToken: String,
    emailVerificationExpire: Date,
    
}, {timestamps: true});

freemiumSchema.methods.getEmailVerificationToken = function () {
    //Generating Token
    const verificationToken = crypto.randomBytes(20).toString("hex")

    //Hashing and adding emailVerificationToken to userSchema
    this.emailVerificationToken = crypto
    .createHash("sha256")
    .update(verificationToken)
    .digest("hex")

    this.emailVerificationExpire = Date.now() + 15 * 60 * 1000

    return verificationToken
}

module.exports = mongoose.models.Freemium || mongoose.model("Freemium", freemiumSchema, "Freemium");